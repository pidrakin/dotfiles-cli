package exec

import "os/exec"

type GoExecer struct{}

func (g *GoExecer) Command(path string) (out string, err error) {
	command := exec.Command(path)
	ret, err := command.CombinedOutput()
	if err == nil {
		out = string(ret)
	}
	return
}
