package exec

type Execer interface {
	Command(path string) (string, error)
}

var execer Execer = &GoExecer{}

func SetExecer(e Execer) {
	execer = e
}

func Command(path string) (string, error) {
	return execer.Command(path)
}
