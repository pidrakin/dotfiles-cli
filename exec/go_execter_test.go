package exec

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/pidrakin/go/tests"
	"os"
	"testing"
)

func TestGoExecer_Command(t *testing.T) {
	tests.RunInTmp(t, func(t *testing.T, wd string) {
		err := os.WriteFile("foobar.txt", []byte("foobar"), os.FileMode(0755))
		tests.AssertNoError(t, err)
		result, err := Command("ls")
		tests.AssertNoError(t, err)
		assert.Equal(t, "foobar.txt\n", result)
	})
}
