package exec

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/pidrakin/go/tests"
	"testing"
)

func TestMockExecer(t *testing.T) {
	defer func() {
		execer = &GoExecer{}
	}()
	execer = &MockExecer{}
	out, err := execer.Command("")
	tests.AssertNoError(t, err)
	assert.Empty(t, out)
}
