package exec

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/pidrakin/go/tests"
	"testing"
)

func TestSetExecer(t *testing.T) {
	defer func() {
		execer = &GoExecer{}
	}()
	SetExecer(&MockExecer{})
	result, err := Command("foobar")
	tests.AssertNoError(t, err)
	assert.Equal(t, "", result)
}
