#!/bin/bash

setup_git_directories() {
  git config --global user.email "you@example.com"
  git config --global user.name "Your Name"
  mkdir -p /tmp/x.git/ /tmp/x
  cd /tmp/x.git/
  git init --bare
  cd /tmp/x
  git init
  git remote add origin /tmp/x.git/
}

setup_dotfiles_repo() {
  cd /tmp/x
  echo "blubb" > test
  echo "# testfile" > fakerc
  git add -A && git commit -m "blubb" && git push -u origin master
}

test_install() {
  diff -qr /tmp/x /root/.
}

test_links() {
  test_files=(test fakerc)
  for filename in ${array[*]}; do
    link_dst=$(readlink -f /home/test/${filename})
    if [[ "${link_dst}" != "/home/test/.local/share/dotfiles/123/${filename}" ]]
    then
      echo "${link_dst} does not point to the correct dst"
    fi
  done
}

test_rollout() {
  test_files=(test fakerc)
  for filename in ${array[*]}; do
    link_dst=$(readlink -f /home/test/${filename})
    if [[ "${link_dst}" != "/home/test/.local/share/dotfiles/123/${filename}" ]]
    then
      echo "${link_dst} does not point to the correct dst"
    fi
  done
}

"$@"
