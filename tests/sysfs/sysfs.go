package sysfs

import (
	"gitlab.com/pidrakin/go/slices"
	"gitlab.com/pidrakin/go/sysfs"
	"io/fs"
	"path"
	"testing"
)

func CheckTree(root string, dirs []string, files []string, t *testing.T) {
	dirs = slices.Map(dirs, func(s string) string {
		return path.Join(root, s)
	})
	files = slices.Map(files, func(s string) string {
		return path.Join(root, s)
	})

	var paths = map[string]struct {
		Dir      bool
		Expected bool
	}{}
	if err := sysfs.Walk(root, func(path string, info fs.FileInfo, err error) error {
		var expected bool
		if info.IsDir() {
			expected = slices.Contains(dirs, path)
		} else {
			expected = slices.Contains(files, path)
		}
		paths[path] = struct {
			Dir      bool
			Expected bool
		}{Dir: info.IsDir(), Expected: expected}
		return nil
	}); err != nil {
		t.Errorf(err.Error())
	}

	for k, v := range paths {
		if !v.Expected {
			if v.Dir {
				t.Errorf("found dir [%s] which is not expected", k)
			} else {
				t.Errorf("found file [%s] which is not expected", k)
			}
		}
	}

	for _, dir := range dirs {
		val, ok := paths[dir]
		if !ok {
			t.Errorf("missing dir [%s] in target tree", dir)
		} else if !val.Dir {
			t.Errorf("dir [%s] is not a directory but should be", dir)
		}
	}

	for _, file := range files {
		val, ok := paths[file]
		if !ok {
			t.Errorf("missing file [%s] in target tree", file)
		} else if val.Dir {
			t.Errorf("file [%s] is a directory but should be a file", file)
		}
	}
}
