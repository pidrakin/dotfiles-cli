package rollout

import _ "embed"

//go:embed keys/id_ed25519
var IdentityFile []byte

//go:embed keys/p_id_ed25519
var ProtectedIdentityFile []byte

const ProtectedIdentityPassphrase string = "test"
