package ssh_ultimate

import (
	"bytes"
	"fmt"
	"github.com/pkg/sftp"
	"golang.org/x/crypto/ssh"
	"golang.org/x/crypto/ssh/agent"
	"io"
	"os"
)

type Connection struct {
	client *ssh.Client
}

func NewConnection(configs []*Config) (*Connection, error) {

	c := &Connection{
		client: nil,
	}

	var err error
	c.client, err = dial(configs)
	if err != nil {
		return nil, err
	}

	return c, nil
}

func (connection *Connection) SftpUploadFile(sourcePath string, destinationPath string, permissions os.FileMode) error {
	source, err := os.Open(sourcePath)
	if err != nil {
		return fmt.Errorf("[dotfiles rollout] sftpUpload sourcefile error: %v", err)
	}
	defer source.Close()
	return connection.SftpUpload(source, destinationPath, permissions)
}

func (connection *Connection) SftpWriteUpload(source []byte, destinationPath string, permissions os.FileMode) error {
	sourceReader := bytes.NewReader(source)
	return connection.SftpUpload(sourceReader, destinationPath, permissions)
}

func (connection *Connection) SftpUpload(source io.Reader, destinationPath string, permissions os.FileMode) error {
	// Create remote file for writing.
	client, err := sftp.NewClient(connection.client)
	if err != nil {
		return err
	}
	destination, err := client.Create(destinationPath)
	if err != nil {
		return fmt.Errorf("[dotfiles rollout] sftpUpload remoteFile error: %v", err)
	}
	defer destination.Close()
	err = client.Chmod(destinationPath, permissions)
	if err != nil {
		return fmt.Errorf("[dotfiles rollout] sftpUpload remoteFile error: %v", err)
	}

	chunk := make([]byte, 1000000) // 1MB chunks TODO: check if there is a better default

	for {
		num, err := source.Read(chunk)
		if err == io.EOF {
			tot, err := destination.Write(chunk[:num])
			if err != nil {
				return err
			}

			if tot != len(chunk[:num]) {
				return fmt.Errorf("[dotfiles rollout] sftp upload failed to write stream")
			}

			return nil
		}

		if err != nil {
			return err
		}

		tot, err := destination.Write(chunk[:num])
		if err != nil {
			return err
		}

		if tot != len(chunk[:num]) {
			return fmt.Errorf("[dotfiles rollout] sftp upload to write stream")
		}
	}
}

func run(connection *Connection, reader io.Reader, interactive bool, stdout io.Writer, stderr io.Writer, command string) error {

	session, err := connection.client.NewSession()
	if err != nil {
		return err
	}
	defer session.Close()

	if requestErr := agent.RequestAgentForwarding(session); requestErr != nil {
		if requestErr.Error() != "forwarding request denied" {
			return err
		}
	}

	if err = session.RequestPty("xterm", 80, 40, ssh.TerminalModes{}); err != nil {
		_, _ = fmt.Fprintf(stderr, "Unable to request Pseudo Terminal")
		return err
	}

	in, err := session.StdinPipe()
	if err != nil {
		return err
	}
	out, err := session.StdoutPipe()
	if err != nil {
		return err
	}
	errOut, err := session.StderrPipe()
	if err != nil {
		return err
	}

	consumer := NewConsumer()
	consumer.AddLocal(reader, stdout, stderr)
	consumer.AddRemote(STDOUT, interactive, out, in)
	consumer.AddRemote(STDERR, interactive, errOut, in)
	consumer.Consume()

	_, err = session.Output(command)
	if err != nil {
		return err
	}

	consumer.Wait()

	return err
}

func (connection *Connection) Run(interactive bool, stdout io.Writer, stderr io.Writer, command string) error {
	return run(connection, os.Stdin, interactive, stdout, stderr, command)
}
