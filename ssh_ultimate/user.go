package ssh_ultimate

import (
	"github.com/kevinburke/ssh_config"
	osuser "os/user"
)

func parseUser(sshConfig *ssh_config.Config, host string, user string) (string, error) {
	if user != "" {
		return user, nil
	}

	var err error
	if sshConfig != nil {
		user, err = sshConfig.Get(host, "User")
		if err != nil {
			return "", err
		}
		if user != "" {
			return user, nil
		}
	}

	userObject, err := osuser.Current()
	if err != nil {
		return "", err
	}
	return userObject.Username, nil
}
