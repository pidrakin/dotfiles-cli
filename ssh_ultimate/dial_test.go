//go:build keep

package ssh_ultimate

import (
	"bytes"
	_ "embed"
	"fmt"
	"github.com/kevinburke/ssh_config"
	"github.com/stretchr/testify/assert"
	"gitlab.com/pidrakin/dotfiles-cli/tests/rollout"
	"gitlab.com/pidrakin/go/tests"
	"golang.org/x/crypto/ssh"
	"golang.org/x/crypto/ssh/agent"
	"strings"
	"testing"
)

func dialSSHConfig() string {
	return fmt.Sprintf(`
Host test
  HostName %s
  Port %d
  User test
  UserKnownHostsFile /dev/null
  StrictHostKeyChecking no

Host testproxy
  HostName 127.0.0.1
  ProxyJump test
  Port 23
  User test2
`, sshUltimateHost, sshUltimatePort)
}

func TestDial(t *testing.T) {
	Prepare(t, dialSSHConfig, func(t *testing.T, sshConfig *ssh_config.Config) {
		t.Run("with password", func(t *testing.T) {
			reader := strings.NewReader("test\n")
			var buf bytes.Buffer

			configs, err := NewConfigs(
				"test",
				ReaderOption(reader),
				WriterOption(&buf),
			)
			tests.AssertNoError(t, err)

			client, err := dial(configs)
			tests.AssertNoError(t, err)
			assert.NotNil(t, client)

			session, err := client.NewSession()
			tests.AssertNoError(t, err)
			assert.NotNil(t, session)
			defer session.Close()

			var stdout bytes.Buffer
			session.Stdout = &stdout

			err = session.Run("pwd")
			tests.AssertNoError(t, err)

			assert.Equal(t, "/home/test\n", stdout.String())
		})
		t.Run("without agent", func(t *testing.T) {
			reader := strings.NewReader("test\ntest\n")
			var buf bytes.Buffer

			configs, err := NewConfigs(
				"testproxy",
				ReaderOption(reader),
				WriterOption(&buf),
			)
			tests.AssertNoError(t, err)

			client, err := dial(configs)
			tests.AssertNoError(t, err)
			assert.NotNil(t, client)

			session, err := client.NewSession()
			tests.AssertNoError(t, err)
			assert.NotNil(t, session)
			defer session.Close()

			var stdout bytes.Buffer
			session.Stdout = &stdout

			err = session.Run("pwd")
			tests.AssertNoError(t, err)

			assert.Equal(t, "/home/test2\n", stdout.String())
		})
		t.Run("with agent", func(t *testing.T) {
			privateKey, err := ssh.ParseRawPrivateKey(rollout.IdentityFile)
			tests.AssertNoError(t, err)
			key := agent.AddedKey{
				PrivateKey: privateKey,
			}
			agentClient = agent.NewKeyring()
			defer func() {
				agentClient = nil
			}()
			err = agentClient.Add(key)
			tests.AssertNoError(t, err)

			reader := strings.NewReader("")
			var buf bytes.Buffer

			configs, err := NewConfigs(
				"testproxy",
				ReaderOption(reader),
				WriterOption(&buf),
			)
			tests.AssertNoError(t, err)

			client, err := dial(configs)
			tests.AssertNoError(t, err)
			assert.NotNil(t, client)

			session, err := client.NewSession()
			tests.AssertNoError(t, err)
			assert.NotNil(t, session)
			defer session.Close()

			var stdout bytes.Buffer
			session.Stdout = &stdout

			err = session.Run("pwd")
			tests.AssertNoError(t, err)

			assert.Equal(t, "/home/test2\n", stdout.String())
		})
	})
}
