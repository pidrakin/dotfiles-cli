package ssh_ultimate

import (
	"github.com/kevinburke/ssh_config"
	"github.com/stretchr/testify/assert"
	"gitlab.com/pidrakin/go/tests"
	"testing"
)

func portSSHConfig() string {
	return `
Host *
  Port 24
`
}

func TestParsePort(t *testing.T) {
	Prepare(t, portSSHConfig, func(t *testing.T, sshConfig *ssh_config.Config) {
		t.Run("without ssh config: override", func(t *testing.T) {
			port, err := parsePort(nil, "foobar", "23")
			tests.AssertNoError(t, err)
			assert.Equal(t, "23", port)
		})

		t.Run("without ssh config: default", func(t *testing.T) {
			port, err := parsePort(nil, "foobar", "")
			tests.AssertNoError(t, err)
			assert.Equal(t, "22", port)
		})

		t.Run("with config", func(t *testing.T) {
			port, err := parsePort(sshConfig, "foobar", "")
			tests.AssertNoError(t, err)
			assert.Equal(t, "24", port)
		})
	})
}
