package ssh_ultimate

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestNewProxyJump(t *testing.T) {
	t.Run("Empty string", func(t *testing.T) {
		proxy := newProxyJump("")
		assert.NotNil(t, proxy)
		assert.Empty(t, proxy.Host)
		assert.Empty(t, proxy.Port)
		assert.Empty(t, proxy.User)
	})

	t.Run("Only host", func(t *testing.T) {
		proxy := newProxyJump("example.com")
		assert.NotNil(t, proxy)
		assert.Equal(t, "example.com", proxy.Host)
		assert.Empty(t, proxy.Port)
		assert.Empty(t, proxy.User)
	})

	t.Run("With user", func(t *testing.T) {
		proxy := newProxyJump("user@example.com")
		assert.NotNil(t, proxy)
		assert.Equal(t, "example.com", proxy.Host)
		assert.Empty(t, proxy.Port)
		assert.Equal(t, "user", proxy.User)
	})

	t.Run("With port", func(t *testing.T) {
		proxy := newProxyJump("example.com:23")
		assert.NotNil(t, proxy)
		assert.Equal(t, "example.com", proxy.Host)
		assert.Equal(t, "23", proxy.Port)
		assert.Empty(t, proxy.User)
	})

	t.Run("With all", func(t *testing.T) {
		proxy := newProxyJump("user@example.com:23")
		assert.NotNil(t, proxy)
		assert.Equal(t, "example.com", proxy.Host)
		assert.Equal(t, "23", proxy.Port)
		assert.Equal(t, "user", proxy.User)
	})
}
