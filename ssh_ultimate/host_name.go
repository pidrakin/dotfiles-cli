package ssh_ultimate

import (
	"github.com/kevinburke/ssh_config"
	"strings"
)

func parseHostName(sshConfig *ssh_config.Config, host string) (string, error) {
	var hostName string
	var err error
	if sshConfig != nil {
		hostName, err = sshConfig.Get(host, "HostName")
		if err != nil {
			return "", err
		}

		if hostName == "" {
			for _, hostConfig := range sshConfig.Hosts {
				if hostConfig.Matches(host) {
					for _, pattern := range hostConfig.Patterns {
						hostName = pattern.String()
					}
				}
			}
		}
	}

	if hostName == "" || strings.Contains(hostName, "*") {
		hostName = host
	}
	return hostName, nil
}
