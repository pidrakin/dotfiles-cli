//go:build keep

package ssh_ultimate

import (
	"bytes"
	"fmt"
	"github.com/kevinburke/ssh_config"
	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"gitlab.com/pidrakin/dotfiles-cli/common/logging"
	rolloutTest "gitlab.com/pidrakin/dotfiles-cli/tests/rollout"
	"gitlab.com/pidrakin/go/tests"
	"strings"
	"testing"
)

//func Test_read(t *testing.T) {
//	out, outWriter := io.Pipe()
//	inPipe := make(chan []byte)
//	outPipe := make(chan []byte)
//	promptPipe := make(chan []byte)
//	cleanupPipe := make(chan string)
//
//	wg := &sync.WaitGroup{}
//	wg.Add(1)
//	go handleRemoteCommunication(wg, true, outWriter, out, inPipe, outPipe, promptPipe, cleanupPipe, "")
//
//	_, err := io.WriteString(outWriter, "line 1\n")
//	tests.AssertNoError(t, err)
//
//	line1 := <-outPipe
//
//	assert.Equal(t, 0, len(outPipe))
//	assert.Equal(t, "line 1\n", string(line1))
//
//	_, err = io.WriteString(outWriter, "promptCh: ")
//	tests.AssertNoError(t, err)
//
//	linePrompt := <-outPipe
//
//	assert.Equal(t, 0, len(outPipe))
//	assert.Equal(t, "promptCh: ", string(linePrompt))
//
//	promptResult := <-promptPipe
//
//	assert.Equal(t, 0, len(promptPipe))
//	assert.Equal(t, "promptCh: ", string(promptResult))
//
//	inPipe <- []byte("answer\n")
//
//	answer := <-outPipe
//
//	assert.Equal(t, 0, len(outPipe))
//	assert.Equal(t, "answer\n", string(answer))
//
//	err = outWriter.Close()
//	tests.AssertNoError(t, err)
//	wg.Wait()
//}
//
//func Test_handleLocalWrite(t *testing.T) {
//	var writer bytes.Buffer
//	outPipe := make(chan []byte)
//	var output string
//
//	wg := &sync.WaitGroup{}
//	wg.Add(1)
//	go handleLocalWrite(wg, &writer, outPipe, &output)
//
//	outPipe <- []byte("foobar\n")
//	outPipe <- []byte("bazbar")
//
//	close(outPipe)
//
//	wg.Wait()
//
//	assert.Equal(t, "foobar\nbazbar", writer.String())
//	assert.Equal(t, "foobar\nbazbar", output)
//}
//
//func Test_handleLocalRead(t *testing.T) {
//	reader := strings.NewReader("answer\nanswer2\n")
//	inPipe := make(chan []byte)
//	promptPipe := make(chan []byte)
//
//	wg := &sync.WaitGroup{}
//	wg.Add(1)
//	go handleLocalRead(wg, reader, inPipe, promptPipe)
//
//	promptPipe <- []byte("promptCh: ")
//	response := <-inPipe
//
//	assert.Equal(t, "answer\n", string(response))
//
//	promptPipe <- []byte("[sudo] password for")
//	response = <-inPipe
//
//	assert.Equal(t, "answer2\n", string(response))
//
//	wg.Wait()
//}

func connectionSSHConfig() string {
	return fmt.Sprintf(`
Host FooAlias
  HostName foodns.host.name
  User foouser
  ProxyJump bastion1,bastion2

Host FooAlias2
  HostName foodns2.host.name
  Port 6666
  User foouser2

Host Test
  HostName %s
  Port %d
  User test
`, sshUltimateHost, sshUltimatePort)
}

func TestNewConnection(t *testing.T) {
	Prepare(t, connectionSSHConfig, func(t *testing.T, sshConfig *ssh_config.Config) {
		logging.SetLogLevel(log.FatalLevel)
		t.Run("failed to dial", func(t *testing.T) {

			var buf bytes.Buffer
			var errBuf bytes.Buffer
			var exit *int

			configs, err := NewConfigs(
				"FooAlias2",
				WriterOption(&buf),
				ErrWriterOption(&errBuf),
				ExitFuncOption(func(i int) {
					exit = &i
				}),
				PortOption("6667"),
				UserOption("foobar"),
				IdentityFilePathOption("~/.ssh/p_id_ed25519"),
				IdentityFilePassphraseOption(rolloutTest.ProtectedIdentityPassphrase),
			)
			tests.AssertNoError(t, err)
			assert.Empty(t, buf.String())
			assert.Empty(t, errBuf.Bytes())
			assert.Nil(t, exit)

			_, err = NewConnection(configs)
			assert.NotNil(t, err)
			assert.Contains(t, err.Error(), "dial tcp: lookup foodns2.host.name")
		})

		var buf bytes.Buffer
		var errBuf bytes.Buffer
		var exit *int

		configs, err := NewConfigs(
			"Test",
			ReaderOption(strings.NewReader("")),
			WriterOption(&buf),
			ErrWriterOption(&errBuf),
			ExitFuncOption(func(i int) {
				exit = &i
			}),
			IdentityFilePathOption("~/.ssh/id_ed25519"),
		)
		tests.AssertNoError(t, err)
		assert.Empty(t, buf.String())
		assert.Empty(t, errBuf.Bytes())
		assert.Nil(t, exit)

		t.Run("success", func(t *testing.T) {
			c, err := NewConnection(configs)
			tests.AssertNoError(t, err)
			assert.Nil(t, errBuf.Bytes())
			if !assert.Nil(t, exit) {
				t.FailNow()
			}

			var stdout, stderr bytes.Buffer
			cmdErr := c.Run(false, &stdout, &stderr, "pwd")
			assert.Equal(t, "/home/test\n", stdout.String())
			assert.Empty(t, stderr.String())
			assert.Nil(t, cmdErr)
		})

		t.Run("success: interactive", func(t *testing.T) {
			c, err := NewConnection(configs)
			tests.AssertNoError(t, err)
			assert.Nil(t, errBuf.Bytes())
			if !assert.Nil(t, exit) {
				t.FailNow()
			}

			command := `
echo "Start of program"
echo "Choose"
echo "0 first"
echo "1 second"
echo -n "what will you choose: "
read result
echo "result was $result"
echo "End of program"
`

			var stdout, stderr bytes.Buffer
			cmdErr := run(
				c,
				strings.NewReader("0\n"),
				true,
				&stdout,
				&stderr,
				command,
			)
			assert.Equal(t, "Start of program\nChoose\n0 first\n1 second\nwhat will you choose: result was 0\nEnd of program\n", stdout.String())
			assert.Empty(t, stderr.String())
			assert.Nil(t, cmdErr)
		})
	})
}
