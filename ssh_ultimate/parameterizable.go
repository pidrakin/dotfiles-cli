package ssh_ultimate

import (
	"io"
	"os"
)

type Parameterizable struct {
	Reader                 io.Reader
	Writer                 io.Writer
	ErrWriter              io.Writer
	ExitFunc               func(int)
	Port                   string
	User                   string
	IdentityFilePath       string
	IdentityFilePassphrase string
}

func ReaderOption(reader io.Reader) func(*Parameterizable) error {
	return func(p *Parameterizable) error {
		p.Reader = reader
		return nil
	}
}

func WriterOption(writer io.Writer) func(*Parameterizable) error {
	return func(p *Parameterizable) error {
		p.Writer = writer
		return nil
	}
}

func ErrWriterOption(errWriter io.Writer) func(*Parameterizable) error {
	return func(p *Parameterizable) error {
		p.ErrWriter = errWriter
		return nil
	}
}

func ExitFuncOption(exitFunc func(int)) func(*Parameterizable) error {
	return func(p *Parameterizable) error {
		p.ExitFunc = exitFunc
		return nil
	}
}

func PortOption(port string) func(*Parameterizable) error {
	return func(p *Parameterizable) error {
		p.Port = port
		return nil
	}
}

func UserOption(user string) func(*Parameterizable) error {
	return func(p *Parameterizable) error {
		p.User = user
		return nil
	}
}

func IdentityFilePathOption(identityFilePath string) func(*Parameterizable) error {
	return func(p *Parameterizable) error {
		p.IdentityFilePath = identityFilePath
		return nil
	}
}

func IdentityFilePassphraseOption(identityFilePassphrase string) func(*Parameterizable) error {
	return func(p *Parameterizable) error {
		p.IdentityFilePassphrase = identityFilePassphrase
		return nil
	}
}

func NewParameterizable(options ...func(*Parameterizable) error) (*Parameterizable, error) {
	p := &Parameterizable{
		Reader:                 os.Stdin,
		Writer:                 os.Stdout,
		ErrWriter:              os.Stderr,
		ExitFunc:               os.Exit,
		Port:                   "",
		User:                   "",
		IdentityFilePath:       "",
		IdentityFilePassphrase: "",
	}

	for _, op := range options {
		if err := op(p); err != nil {
			return nil, err
		}
	}
	return p, nil
}
