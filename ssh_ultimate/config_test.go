package ssh_ultimate

import (
	"bytes"
	"strings"
	"testing"

	"github.com/kevinburke/ssh_config"
	"github.com/stretchr/testify/assert"
	"gitlab.com/pidrakin/go/tests"
)

func configSSHConfig() string {
	return `
Host bastion3
  HostName bastion3.example.com

Host bastion2
  HostName bastion2.example.com
  proxyJump bastion3

Host bastion1.example.com
  User root

Host remote
  HostName remote.example.com
  Port 6666
  proxyJump user@bastion1.example.com:23, bastion2

Host *
  ControlPath none
`
}

func configSSHConfigWithError() string {
	return `
THIS IS NOT A VALID SSH CONFIG
`
}

func TestNewSSHConfig(t *testing.T) {
	Prepare(t, configSSHConfig, func(t *testing.T, sshConfig *ssh_config.Config) {
		t.Run("test remote host", func(t *testing.T) {
			var buf bytes.Buffer
			var errBuf bytes.Buffer
			reader := strings.NewReader("\n")
			cfg, err := newConfig(reader, &buf, &errBuf, sshConfig, "remote", "", "", "", "")
			tests.AssertNoError(t, err)
			assert.NotNil(t, cfg)
			assert.Equal(t, "remote", cfg.Name)
			assert.Equal(t, "remote.example.com", cfg.HostName)
			assert.Equal(t, "6666", cfg.Port)
			assert.NotEmpty(t, cfg.ClientConfig.User)
			assert.Nil(t, cfg.ProxyJumps)
		})

		t.Run("test bastion1 host", func(t *testing.T) {
			var buf bytes.Buffer
			var errBuf bytes.Buffer
			reader := strings.NewReader("\n")
			cfg, err := newConfig(reader, &buf, &errBuf, sshConfig, "bastion1.example.com", "", "", "", "")
			tests.AssertNoError(t, err)
			assert.NotNil(t, cfg)
			assert.Equal(t, "bastion1.example.com", cfg.Name)
			assert.Equal(t, "bastion1.example.com", cfg.HostName)
			assert.Equal(t, "22", cfg.Port)
			assert.Equal(t, "root", cfg.ClientConfig.User)
			assert.Nil(t, cfg.ProxyJumps)
		})

		t.Run("test unknown foobar host", func(t *testing.T) {
			var buf bytes.Buffer
			var errBuf bytes.Buffer
			reader := strings.NewReader("\n")
			cfg, err := newConfig(reader, &buf, &errBuf, sshConfig, "foobar", "", "", "", "")
			tests.AssertNoError(t, err)
			assert.NotNil(t, cfg)
			assert.Equal(t, "foobar", cfg.Name)
			assert.Equal(t, "foobar", cfg.HostName)
			assert.Equal(t, "22", cfg.Port)
			assert.NotEmpty(t, cfg.ClientConfig.User)
			assert.Nil(t, cfg.ProxyJumps)
		})
	})
}

func TestNewSSHConfigs(t *testing.T) {
	Prepare(t, configSSHConfig, func(t *testing.T, sshConfig *ssh_config.Config) {
		var buf bytes.Buffer
		reader := strings.NewReader("\n")
		var cfgs, err = NewConfigs(
			"remote",
			ReaderOption(reader),
			WriterOption(&buf),
			PortOption("5555"),
			UserOption("user"),
		)
		tests.AssertNoError(t, err)
		assert.NotNil(t, cfgs)

		cfg := cfgs[0]
		assert.Equal(t, "bastion3", cfg.Name)

		cfg = cfgs[1]
		assert.Equal(t, "bastion2", cfg.Name)

		cfg = cfgs[2]
		assert.Equal(t, "bastion1.example.com", cfg.Name)

		cfg = cfgs[3]
		assert.Equal(t, "remote", cfg.Name)
		assert.Equal(t, "remote.example.com", cfg.HostName)
		assert.Equal(t, "5555", cfg.Port)
		assert.Equal(t, "user", cfg.ClientConfig.User)
		assert.Equal(t, 2, len(cfg.ProxyJumps))
	})
}
