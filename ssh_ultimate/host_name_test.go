package ssh_ultimate

import (
	"github.com/kevinburke/ssh_config"
	"github.com/stretchr/testify/assert"
	"gitlab.com/pidrakin/go/tests"
	"testing"
)

func hostnameSSHConfig() string {
	return `
Host foobar

Host bazbar
  HostName bazbar.example.com

Host foo*

Host *
`
}

func TestParseHostName(t *testing.T) {
	Prepare(t, hostnameSSHConfig, func(t *testing.T, sshConfig *ssh_config.Config) {
		t.Run("without ssh config", func(t *testing.T) {
			host, err := parseHostName(nil, "foobar")
			tests.AssertNoError(t, err)
			assert.Equal(t, "foobar", host)
		})

		t.Run("with ssh config: no HostName", func(t *testing.T) {
			host, err := parseHostName(sshConfig, "foobar")
			tests.AssertNoError(t, err)
			assert.Equal(t, "foobar", host)
		})

		t.Run("with ssh config: with HostName", func(t *testing.T) {
			host, err := parseHostName(sshConfig, "bazbar")
			tests.AssertNoError(t, err)
			assert.Equal(t, "bazbar.example.com", host)
		})

		t.Run("with ssh config: with Wildcard", func(t *testing.T) {
			host, err := parseHostName(sshConfig, "fooa")
			tests.AssertNoError(t, err)
			assert.Equal(t, "fooa", host)
		})
	})
}
