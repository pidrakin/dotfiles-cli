package ssh_ultimate

import (
	"errors"
	"fmt"
	"github.com/kevinburke/ssh_config"
	"github.com/mitchellh/go-homedir"
	log "github.com/sirupsen/logrus"
	"gitlab.com/pidrakin/dotfiles-cli/common/logging"
	"gitlab.com/pidrakin/go/interactive"
	"golang.org/x/crypto/ssh"
	"golang.org/x/crypto/ssh/agent"
	"io"
	"net"
	"os"
	"strings"
)

func parseIdentityFile(sshConfig *ssh_config.Config, host string, identityFile string) (string, error) {
	if identityFile != "" {
		return identityFile, nil
	}

	if sshConfig != nil {
		identityFile, err := sshConfig.Get(host, "IdentityFile")
		if err != nil {
			return "", err
		}
		if identityFile != "" {
			return identityFile, nil
		}
	}

	return "", nil
}

func parseIdentityAgent(sshConfig *ssh_config.Config, host string, identityAgent string) (string, error) {
	var err error
	if sshConfig != nil {
		if identityAgent, err = sshConfig.Get(host, "IdentityAgent"); err != nil {
			return "", err
		}
	}

	return identityAgent, nil
}

func passwordAuthMethod(reader io.Reader, writer io.Writer, id string) ssh.AuthMethod {
	return ssh.PasswordCallback(func() (string, error) {
		response := os.Getenv("SSHPASS")
		if response == "" {
			_, err := fmt.Fprintf(writer, "[%s] Password required: ", id)
			if err != nil {
				return "", err
			}
			response, err = interactive.PromptLine(reader, true)
			if err != nil {
				return "", err
			}
			response = strings.TrimSuffix(response, "\n")
			_, err = fmt.Fprintf(writer, "\n")
			if err != nil {
				return "", err
			}
		}
		return response, nil
	})
}

var agentClient agent.Agent

func sshAgentAuthMethod(socket string) (ssh.AuthMethod, error) {
	if agentClient == nil {
		socket, err := homedir.Expand(socket)
		if err != nil {
			return nil, fmt.Errorf("[dotfiles rollout] failed to open SSH_AUTH_SOCK: %v", err)
		}
		conn, err := net.Dial("unix", socket)
		if err != nil {
			return nil, fmt.Errorf("[dotfiles rollout] failed to open SSH_AUTH_SOCK: %v", err)
		}

		agentClient = agent.NewClient(conn)
	}
	return ssh.PublicKeysCallback(agentClient.Signers), nil
}

func privateKeyAuthMethod(errWriter io.Writer, file string, keyPass string) (ssh.AuthMethod, error) {
	defer logging.SetLogOutput(errWriter)()
	var key ssh.Signer
	var b []byte
	normalizedPath, err := homedir.Expand(file)
	if err != nil {
		return nil, err
	}
	b, err = os.ReadFile(normalizedPath)
	if err != nil {
		return nil, err
	}
	if keyPass == "" {
		key, err = ssh.ParsePrivateKey(b)
	} else {
		key, err = ssh.ParsePrivateKeyWithPassphrase(b, []byte(keyPass))
	}

	if err != nil {
		var passphraseMissingError *ssh.PassphraseMissingError
		if errors.As(err, &passphraseMissingError) {
			log.Errorf("[dotfiles rollout] ssh identity file needs passphrase")
		}
		return nil, err
	}
	return ssh.PublicKeys(key), nil
}

func parseAuthMethods(reader io.Reader, writer io.Writer, errWriter io.Writer, sshConfig *ssh_config.Config, host string, identityFilePath string, identityKeyPass string, id string) ([]ssh.AuthMethod, error) {
	var authMethods []ssh.AuthMethod

	var err error

	identityFilePath, err = parseIdentityFile(sshConfig, host, identityFilePath)
	if err != nil {
		return nil, err
	}

	if identityFilePath != "" {
		authMethod, privErr := privateKeyAuthMethod(errWriter, identityFilePath, identityKeyPass)
		if privErr != nil {
			return nil, privErr
		}
		authMethods = append(authMethods, authMethod)
	}

	// TODO: not yet used; don't know how
	//var identitiesOnly string
	//if sshConfig != nil {
	//	identitiesOnly, err = sshConfig.Get(host, "IdentitiesOnly")
	//	if err != nil {
	//		return nil, err
	//	}
	//}

	identityAgent, err := parseIdentityAgent(sshConfig, host, os.Getenv("SSH_AUTH_SOCK"))
	if err != nil {
		return nil, err
	}

	if agentClient != nil || identityAgent != "" {
		authMethod, agentErr := sshAgentAuthMethod(identityAgent)
		if agentErr == nil {
			authMethods = append(authMethods, authMethod)
		}
	}

	var passwordAuthentication string
	if sshConfig != nil {
		passwordAuthentication, err = sshConfig.Get(host, "PasswordAuthentication")
		if err != nil {
			return nil, err
		}
	}

	if passwordAuthentication != "no" {
		authMethods = append(authMethods, passwordAuthMethod(reader, writer, id))
	}

	return authMethods, nil
}
