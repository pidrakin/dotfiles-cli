package ssh_ultimate

import (
	"github.com/kevinburke/ssh_config"
	"strings"
)

func parseProxyJumps(sshConfig *ssh_config.Config, host string) ([]*proxyJump, error) {
	var proxies []*proxyJump

	if sshConfig == nil {
		return proxies, nil
	}

	proxyString, err := sshConfig.Get(host, "proxyJump")
	if err != nil {
		return proxies, err
	}

	if proxyString == "" {
		return proxies, nil
	}

	for _, proxy := range strings.Split(proxyString, ",") {
		proxies = append(proxies, newProxyJump(strings.TrimSpace(proxy)))
	}
	return proxies, nil
}
