//go:build testing

package ssh_ultimate

import (
	"flag"
	"os"
	"path/filepath"
	"testing"

	"github.com/kevinburke/ssh_config"
	"github.com/mitchellh/go-homedir"
	"github.com/stretchr/testify/assert"
	"gitlab.com/pidrakin/dotfiles-cli/tests/rollout"
	"gitlab.com/pidrakin/go/tests"
	"golang.org/x/crypto/ssh"
	"golang.org/x/crypto/ssh/agent"
)

var sshUltimateHost string
var sshUltimatePort int

func init() {
	homedir.DisableCache = true
	flag.StringVar(&sshUltimateHost, "ssh-ultimate-host", "localhost", "SSH Ultimate host")
	flag.IntVar(&sshUltimatePort, "ssh-ultimate-port", 2222, "SSH Ultimate port")
}

type sshConfigCreator func() string

func Prepare(t *testing.T, sshConfigCreator sshConfigCreator, f func(t *testing.T, sshConfig *ssh_config.Config)) {
	tests.RunInTmp(t, func(t *testing.T, wd string) {
		homeDir := filepath.Join(wd, "home/user")
		err := os.MkdirAll(homeDir, os.FileMode(0755))
		tests.AssertNoError(t, err)

		err = os.Setenv("HOME", homeDir)
		tests.AssertNoError(t, err)

		sshDir := filepath.Join(homeDir, ".ssh")
		err = os.Mkdir(sshDir, os.FileMode(0700))
		tests.AssertNoError(t, err)

		err = os.WriteFile(filepath.Join(sshDir, "id_ed25519"), rollout.IdentityFile, os.FileMode(0600))
		tests.AssertNoError(t, err)
		err = os.WriteFile(filepath.Join(sshDir, "p_id_ed25519"), rollout.ProtectedIdentityFile, os.FileMode(0600))
		tests.AssertNoError(t, err)

		if sshConfigCreator != nil {
			sshConfigString := sshConfigCreator()

			if sshConfigString != "" {
				err = os.WriteFile(filepath.Join(sshDir, "config"), []byte(sshConfigString), os.FileMode(0600))
				tests.AssertNoError(t, err)

				sshConfig, err := LoadConfig()
				tests.AssertNoError(t, err)
				assert.NotNil(t, sshConfig)

				f(t, sshConfig)
				return
			}
		}

		f(t, nil)
	})
}

func PrepareAgent(t *testing.T, f func(t *testing.T)) {
	privateKey, err := ssh.ParseRawPrivateKey(rollout.IdentityFile)
	tests.AssertNoError(t, err)
	key := agent.AddedKey{
		PrivateKey: privateKey,
	}
	agentClient = agent.NewKeyring()
	defer func() {
		agentClient = nil
	}()
	err = agentClient.Add(key)
	tests.AssertNoError(t, err)

	f(t)
}
