package ssh_ultimate

import (
	"github.com/kevinburke/ssh_config"
	"github.com/stretchr/testify/assert"
	"gitlab.com/pidrakin/go/tests"
	"testing"
)

func userSSHConfig() string {
	return `
Host *
  User root
`
}

func TestParseUser(t *testing.T) {
	Prepare(t, userSSHConfig, func(t *testing.T, sshConfig *ssh_config.Config) {
		t.Run("without ssh config: override", func(t *testing.T) {
			user, err := parseUser(nil, "foobar", "alice")
			tests.AssertNoError(t, err)
			assert.Equal(t, "alice", user)
		})

		t.Run("without ssh config: default", func(t *testing.T) {
			user, err := parseUser(nil, "foobar", "")
			tests.AssertNoError(t, err)
			assert.NotEmpty(t, user)
		})

		t.Run("with config", func(t *testing.T) {
			user, err := parseUser(sshConfig, "foobar", "")
			tests.AssertNoError(t, err)
			assert.Equal(t, "root", user)
		})
	})
}
