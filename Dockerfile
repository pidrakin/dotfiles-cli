############################
# STEP 1 build executable binary
############################
FROM golang:alpine@sha256:64c82390220281455e221d3c75a0a2405e34a5be2d373b411afed363555b6857 AS builder

ARG VERSION=0.0.0

RUN apk --update --no-cache add musl-dev gcc ca-certificates

# Create appuser.
ENV USER=dotfilesuser
ENV UID=10001
# See https://stackoverflow.com/a/55757473/12429735RUN
RUN adduser \
    --disabled-password \
    --gecos "" \
    --home "/home/${USER}" \
    --shell "/sbin/nologin" \
    --no-create-home \
    --uid "${UID}" \
    "${USER}"

WORKDIR /go/src/dotfiles-cli
COPY . .

# Using go mod with go 1.11
RUN go mod download
RUN go mod verify

RUN namespace="gitlab.com/pidrakin/dotfiles-cli" && \
    buildTime=$(date -u +"%Y-%m-%dT%H:%M:%SZ") && \
    GOOS=linux GOARCH=amd64 go build \
    -a \
    -ldflags="-linkmode external -extldflags '-static' -w -s -X '${namespace}/cmd.version=${VERSION}' -X '${namespace}/cmd.buildTime=${buildTime}'" \
    -o /go/bin/dotfiles

############################
# STEP 2 build a small image
############################
FROM scratch
# FROM alpine:latest

# Import from builder.
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/ca-certificates.crt
COPY --from=builder /etc/passwd /etc/passwd
COPY --from=builder /etc/group /etc/group

# Copy our static executable.
COPY --from=builder /go/bin/dotfiles /go/bin/dotfiles

# Use an unprivileged user.
USER dotfilesuser:dotfilesuser

VOLUME ["/home/dotfilesuser"]

# Run the hello binary.
ENTRYPOINT ["/go/bin/dotfiles"]
