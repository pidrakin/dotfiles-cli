package main

import (
	"os"

	log "github.com/sirupsen/logrus"
	"gitlab.com/pidrakin/dotfiles-cli/cli"
	"gitlab.com/pidrakin/dotfiles-cli/cli/completion"
	"gitlab.com/pidrakin/dotfiles-cli/cli/config"
	"gitlab.com/pidrakin/dotfiles-cli/cli/diff"
	"gitlab.com/pidrakin/dotfiles-cli/cli/eject"
	"gitlab.com/pidrakin/dotfiles-cli/cli/initialize"
	"gitlab.com/pidrakin/dotfiles-cli/cli/install"
	"gitlab.com/pidrakin/dotfiles-cli/cli/link"
	"gitlab.com/pidrakin/dotfiles-cli/cli/rollout"
	"gitlab.com/pidrakin/dotfiles-cli/cli/status"
	"gitlab.com/pidrakin/dotfiles-cli/cli/unlink"
	"gitlab.com/pidrakin/dotfiles-cli/text"
)

func main() {
	rootHandler := cli.NewRootHandler("dotfiles", "", "")

	completionHandler := completion.NewHandler(
		text.CompletionUse,
		text.CompletionShort,
		"",
	)
	rootHandler.AddCommand(completionHandler.Command())

	initHandler, err := initialize.NewHandler(
		text.InitUse,
		text.InitShort,
		text.InitLong,
	)
	if err != nil {
		log.Fatal(err)
	}
	rootHandler.AddCommand(initHandler.Command())

	installHandler, err := install.NewHandler(
		text.InstallUse,
		text.InstallShort,
		text.InstallLong,
	)
	if err != nil {
		log.Fatal(err)
	}
	rootHandler.AddCommand(installHandler.Command())

	linkHandler, err := link.NewHandler(
		text.LinkUse,
		text.LinkShort,
		text.LinkLong,
	)
	if err != nil {
		log.Fatal(err)
	}
	rootHandler.AddCommand(linkHandler.Command())

	unlinkHandler := unlink.NewHandler(
		text.UnlinkUse,
		text.UnlinkShort,
		text.UnlinkLong,
	)
	rootHandler.AddCommand(unlinkHandler.Command())

	diffHandler, err := diff.NewHandler(
		text.DiffUse,
		text.DiffShort,
		text.DiffLong,
	)
	if err != nil {
		log.Fatal(err)
	}
	rootHandler.AddCommand(diffHandler.Command())

	rolloutHandler, err := rollout.NewHandler(
		text.RolloutUse,
		text.RolloutShort,
		text.RolloutLong,
	)
	if err != nil {
		log.Fatal(err)
	}
	rootHandler.AddCommand(rolloutHandler.Command())

	statusHandler, err := status.NewHandler(
		text.StatusUse,
		text.StatusShort,
		text.StatusLong,
	)
	if err != nil {
		log.Fatal(err)
	}
	rootHandler.AddCommand(statusHandler.Command())

	configHandler, err := config.NewHandler(
		"config",
		"view or set configs",
		"view or set configs for the state or repo config",
	)
	if err != nil {
		log.Fatal(err)
	}
	rootHandler.AddCommand(configHandler.Command())

	ejectHandler, err := eject.NewHandler(
		"eject",
		"eject internal config files",
		"allows ejecting internal config files in the current working directory",
	)
	if err != nil {
		log.Fatal(err)
	}
	rootHandler.AddCommand(ejectHandler.Command())

	if err := rootHandler.Execute(); err != nil {
		//cobra prints errors self
		os.Exit(1)
	}
	return
}
