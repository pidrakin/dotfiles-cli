package text

import "github.com/fatih/color"

var (
	Diff         = "Diff %s\n\n"
	Package      = "  Package %s\n"
	Add          = color.New(color.FgGreen, color.Bold).Sprintf("    add:\t%%s\n")
	Delete       = color.New(color.FgRed, color.Bold).Sprintf("    delete:\t%%s\n")
	Linked       = color.New(color.FgBlue, color.Bold).Sprintf("    linked:\t%%s\n")
	Modify       = color.New(color.FgYellow, color.Bold).Sprintf("    modify:\t%%s\n")
	InstallFirst = "please run [dotfiles install] first"
)
