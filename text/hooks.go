package text

var (
	HookNotExecutable = "hook does not have executable bit set"
	ExecuteHook       = "executing"
)
