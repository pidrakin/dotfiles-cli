package text

var (
	NoConfig                    = "no config available; create with [init]"
	ConfigRead                  = "parsing config"
	StateConfigDir              = "looking for state configs"
	StateConfigFound            = "found potential state config"
	StateConfigNoInstall        = "state config has no candidate installed; trying to delete it"
	StateConfigDelete           = "successfully deleted state config without installed candidate"
	StateConfigUnmarshal        = "trying to unmarshal state config"
	StateConfigUnmarshalled     = "successfully unmarshalled state config"
	StateConfigValid            = "found valid state config"
	StateConfigCurrent          = "found current state config"
	StateConfigPrevious         = "found previous state config"
	StateConfigValidationFailed = "state config validation failed"
)
