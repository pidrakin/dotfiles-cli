package text

var (
	VerboseOverrulesQuite = "verbose overrules quiet"
	DryRunActive          = "dry run is active and no changes will be made"
)
