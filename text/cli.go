package text

var (
	InitUse         = "init"
	InitShort       = "Scaffold a new dotfiles repository"
	InitLong        = `Scaffold a new dotfiles repository`
	CompletionUse   = "completion [bash]"
	CompletionShort = "Generate completion script"
	InstallUse      = "install [uri]"
	InstallShort    = "Installs dotfiles"
	InstallLong     = `Clones <uri> into a newly created directory, either under .local/share/dotfiles/ or if --global is set under /usr/share/dotfiles.

<uri> may also be in the format gl:<username> or gh:<username> to find and install a dotfiles repository from a Gitlab.com or Github.com user's repository respectively.`
	LinkUse      = "link [collection]"
	LinkShort    = "Link dotfiles from installed repository to the home directory"
	LinkLong     = "Link dotfiles from installed repository to the home directory"
	DiffUse      = "diff [collection]"
	DiffShort    = "Show changes between linked files and repository"
	DiffLong     = "Show changes between linked files and repository"
	UnlinkUse    = "unlink"
	UnlinkShort  = "Unlinks previously linked files in home directory"
	UnlinkLong   = "Unlinks previously linked files in home directory"
	RolloutUse   = "rollout [host]"
	RolloutShort = "Rollout current dotfiles"
	RolloutLong  = "Connects to a remote host through SSH and installs and links the same dotfiles repository installed locally"
	StatusUse    = "status"
	StatusShort  = "Status of dotfile environments"
	StatusLong   = "Can show the status of the local or the remotely managed dotfile environments"
)
