package text

var (
	TrySwitchContext = "trying to switch context"
	Install          = "install dotfiles"
	TryClone         = "trying to clone repository"
	Cloned           = "successfully cloned repository"
	AlreadySymlinked = "already symlinked"
	Symlinked        = "successfully symlinked"
	NothingToInstall = "Nothing to install"
	InvalidUri       = "invalid uri [%s] specified"
	GoGitInvalidUri  = "SCP-like URIs are not supported by go-git; see: https://github.com/go-git/go-git/pull/324"
)
