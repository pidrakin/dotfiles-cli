# Install

## Easy

```shell
curl -fsSL https://dotfiles.pidrak.in | sudo sh -
```

Usage:

```shell
curl -fsSL https://dotfiles.pidrak.in | sudo sh -s -- --help

install.sh

Installer for dotfiles CLI

Usage:
install.sh [-h|--help]
install.sh [-s|--silent] [-n|--no-package] [-v|--version VERSION]

Options:
-s, --silent           Do not print any messages
-n, --no-package       Do not install via package manager
-v, --version          Install specific version
```

## Set Architecture

If `uname` is unvailable

```shell
# for x86_64
export ARCH=amd64
# for i386
export ARCH=i386
# for ARM
export ARCH=arm64
```

## Compile

```shell
git clone https://gitlab.com/pidrakin/dotfiles-cli.git
cd dotfiles-cli
go build -o /usr/local/bin/dotfiles
```

## Linux

```shell
curl -fsSL https://dotfiles.pidrak.in/$(uname -m).tar.gz | sudo tar -xz -C /usr/local/bin/
```

## Alpine

```shell
curl -fsSL -o dotfiles.apk https://dotfiles.pidrak.in/$(uname -m).apk
apk add --no-cache --allow-untrusted dotfiles.apk
```

```shell
echo "https://alpine.fury.io/satanik/" >> /etc/apk/repositories
curl -fsSL https://alpine.fury.io/satanik/satanik@fury.io-e8dc17e0.rsa.pub > /etc/apk/keys/satanik@fury.io-e8dc17e0.rsa.pub
apk update >/dev/null
apk add --no-cache dotfiles
```

## Debian/Ubuntu

```shell
curl -fsSL -o dotfiles.deb https://dotfiles.pidrak.in/$(uname -m).deb
dpkg -i dotfiles.deb
```

```shell
apt update
apt install -y ca-certificates gpg
curl -fsSL https://fury.pidrak.in/apt/gpg.key | gpg --dearmor > /usr/share/keyrings/pidrakin-keyring.gpg
echo "deb [signed-by=/usr/share/keyrings/pidrakin-keyring.gpg] https://fury.pidrak.in/apt/ /" > /etc/apt/sources.list.d/pidrakin.list
apt update
apt install -y dotfiles
```

## Fedora/CentOS/RHEL

```shell
dnf install https://dotfiles.pidrak.in/$(uname -m).rpm
```

```shell
curl -fsSL -o /etc/pki/rpm-gpg/RPM-GPG-KEY-pidrakin https://fury.pidrak.in/rpm/gpg.key
cat > /etc/yum.repos.d/pidrakin.repo <<EOL
[pidrakin]
name=Pidrakin Repo
baseurl=https://fury.pidrak.in/yum/
enabled=1
gpgcheck=0
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-pidrakin
EOL
dnf makecache
dnf install -y dotfiles
```

## Mac

### cURL

```shell
curl -fsSL https://dotfiles.pidrak.in/mac-$(uname -m).tar.gz | sudo tar -xz -C /usr/local/bin/
```

### Homebrew

```shell
brew tap pidrakin/dotfiles
brew install dotfiles
```

## Container

```shell
podman pull registry.gitlab.com/pidrakin/dotfiles-cli:latest
```
