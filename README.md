# dotfiles <img src="icon.png" height="30px" alt="icon">

This tool encourages and supports creating, maintaining and distributing a set of **[dotfiles](https://wiki.archlinux.org/title/Dotfiles)**.
Dotfiles encompasses the relevant configuration files accompanying most of the binaries, tools on *nix-themed* systems.

## Installation

### Linux Binary

```shell
curl -fsSL https://dotfiles.pidrak.in | sudo sh -
```

For further details see [INSTALL.md](INSTALL.md)

## Usage

<img src="preview.png" alt="usage-preview"/>

See `man 1 dotfiles`

## Contributing

Contributing is not possible at the moment.

## License

[HOOKAH-WARE](LICENSE)

## TODO

[TODO LIST](TODO.md)
