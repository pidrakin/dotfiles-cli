package git

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	giturls "github.com/whilp/git-urls"
	"gitlab.com/pidrakin/go/tests"
	"testing"
)

func TestGitlabUserArray_Get(t *testing.T) {
	t.Run("fail: array empty", func(t *testing.T) {
		var array GitlabUserArray
		result := array.Get("")
		assert.Nil(t, result)
	})
	t.Run("fail: username does not exist", func(t *testing.T) {
		users := GitlabUserArray{GitlabUser{Id: 1, Username: "foobar"}}
		result := users.Get("bazbar")
		assert.Nil(t, result)
	})
	t.Run("success", func(t *testing.T) {
		users := GitlabUserArray{GitlabUser{Id: 1, Username: "foobar"}}
		result := users.Get("foobar")
		assert.NotNil(t, result)
		assert.Equal(t, 1, result.Id)
		assert.Equal(t, "foobar", result.Username)
	})
}

func TestGitlabProjectArray_Filter(t *testing.T) {
	t.Run("fail: array empty", func(t *testing.T) {
		var array GitlabProjectArray
		result := array.Filter(func(project GitlabProject) bool {
			return true
		})
		assert.Empty(t, result)
	})
	t.Run("fail: filter does not yield anything", func(t *testing.T) {
		projects := GitlabProjectArray{GitlabProject{Path: "dotfiles", Url: "https://gitlab.com/user/dotfiles.git"}}
		result := projects.Filter(func(project GitlabProject) bool {
			return project.Path == ""
		})
		assert.Empty(t, result)
	})
	t.Run("success", func(t *testing.T) {
		projects := GitlabProjectArray{GitlabProject{Path: "dotfiles", Url: "https://gitlab.com/user/dotfiles.git"}}
		result := projects.Filter(func(project GitlabProject) bool {
			return project.Path == "dotfiles"
		})
		assert.NotEmpty(t, result)
		assert.Equal(t, "dotfiles", result[0].Path)
	})
}

func TestGitlabProjectArray_Sorted(t *testing.T) {
	projects := GitlabProjectArray{
		GitlabProject{Path: "dot-files-cli"},
		GitlabProject{Path: "dot-files"},
		GitlabProject{Path: "dotfile"},
		GitlabProject{Path: "dotfiles"},
		GitlabProject{Path: "dotfile-"},
		GitlabProject{Path: "dot-file"},
	}
	sorted := projects.Sorted()
	assert.Equal(t, "dotfiles", sorted[0].Path)
	assert.Equal(t, "dot-files", sorted[1].Path)
	assert.Equal(t, "dotfile", sorted[2].Path)
	assert.Equal(t, "dot-file", sorted[3].Path)
	assert.Equal(t, "dotfile-", sorted[4].Path)
	assert.Equal(t, "dot-files-cli", sorted[5].Path)
}

func TestGitlabUriToUsername(t *testing.T) {
	t.Run("fail: empty", func(t *testing.T) {
		username := gitlabUriToUsername("")
		assert.Empty(t, username)
	})
	protocols := giturls.Transports.Transports
	for protocol := range protocols {
		t.Run(fmt.Sprintf("fail: protocol %s", protocol), func(t *testing.T) {
			username := gitlabUriToUsername(fmt.Sprintf("%sfoobar", protocol))
			assert.Empty(t, username)
		})
	}
	t.Run("success: gitlab://", func(t *testing.T) {
		username := gitlabUriToUsername("gitlab://foobar")
		assert.Equal(t, "foobar", username)
	})
	t.Run("success: gl:", func(t *testing.T) {
		username := gitlabUriToUsername("gl:foobar")
		assert.Equal(t, "foobar", username)
	})
}

func TestParseGitlabUri(t *testing.T) {
	t.Run("pidrakin", func(t *testing.T) {
		uri, err := _gitlabUsernameToUri("pidrakin", true)
		tests.AssertNoError(t, err)
		assert.Equal(t, "https://gitlab.com/pidrakin/dotfiles-cli.git", uri)
	})
	t.Run("satanik", func(t *testing.T) {
		uri, err := _gitlabUsernameToUri("satanik", true)
		tests.AssertNoError(t, err)
		assert.Equal(t, "https://gitlab.com/satanik/dotfiles.git", uri)
	})
	t.Run("b3n4ak", func(t *testing.T) {
		uri, err := _gitlabUsernameToUri("b3n4ak", true)
		tests.AssertEqualError(t, err, "user [b3n4ak] does not exist on Gitlab")
		assert.Equal(t, "", uri)
	})
	t.Run("+43", func(t *testing.T) {
		uri, err := _gitlabUsernameToUri("+43", true)
		tests.AssertEqualError(t, err, "user [+43] does not exist on Gitlab")
		assert.Equal(t, "", uri)
	})
}
