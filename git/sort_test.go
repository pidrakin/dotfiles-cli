package git

import (
	"github.com/stretchr/testify/assert"
	"sort"
	"testing"
)

type Foo struct {
	Name string
}

func TestByRank_Len(t *testing.T) {
	t.Run("length is 0", func(t *testing.T) {
		var foos []RankedItem
		assert.Equal(t, 0, ByRank(foos).Len())
	})
	t.Run("ranks are sorted", func(t *testing.T) {
		foos := []RankedItem{
			{
				Rank: 1,
				Item: Foo{Name: "1"},
			},
			{
				Rank: 2,
				Item: Foo{Name: "2"},
			},
			{
				Rank: 3,
				Item: Foo{Name: "3"},
			},
		}
		sort.Sort(ByRank(foos))
		assert.Equal(t, 1, foos[0].Rank)
		assert.Equal(t, 2, foos[1].Rank)
		assert.Equal(t, 3, foos[2].Rank)
	})
	t.Run("ranks are unsorted", func(t *testing.T) {
		foos := []RankedItem{
			{
				Rank: 2,
				Item: Foo{Name: "2"},
			},
			{
				Rank: 1,
				Item: Foo{Name: "1"},
			},
			{
				Rank: 3,
				Item: Foo{Name: "3"},
			},
		}
		sort.Sort(ByRank(foos))
		assert.Equal(t, 1, foos[0].Rank)
		assert.Equal(t, 2, foos[1].Rank)
		assert.Equal(t, 3, foos[2].Rank)
	})
	t.Run("ranks are doubled", func(t *testing.T) {
		foos := []RankedItem{
			{
				Rank: 1,
				Item: Foo{Name: "1"},
			},
			{
				Rank: 1,
				Item: Foo{Name: "11"},
			},
			{
				Rank: 2,
				Item: Foo{Name: "2"},
			},
		}
		sort.Sort(ByRank(foos))
		assert.Equal(t, 1, foos[0].Rank)
		assert.Equal(t, "1", foos[0].Item.(Foo).Name)
		assert.Equal(t, 1, foos[1].Rank)
		assert.Equal(t, "11", foos[1].Item.(Foo).Name)
		assert.Equal(t, 2, foos[2].Rank)
	})
	t.Run("ranks are doubled and preserved", func(t *testing.T) {
		foos := []RankedItem{
			{
				Rank: 1,
				Item: Foo{Name: "11"},
			},
			{
				Rank: 1,
				Item: Foo{Name: "1"},
			},
			{
				Rank: 2,
				Item: Foo{Name: "2"},
			},
		}
		sort.Sort(ByRank(foos))
		assert.Equal(t, 1, foos[0].Rank)
		assert.Equal(t, "11", foos[0].Item.(Foo).Name)
		assert.Equal(t, 1, foos[1].Rank)
		assert.Equal(t, "1", foos[1].Item.(Foo).Name)
		assert.Equal(t, 2, foos[2].Rank)
	})
}

func TestByRank_Less(t *testing.T) {
	t.Run("i smaller j", func(t *testing.T) {
		foos := []RankedItem{
			{
				Rank: 1,
				Item: Foo{Name: "1"},
			},
			{
				Rank: 2,
				Item: Foo{Name: "2"},
			},
		}
		assert.Equal(t, true, ByRank(foos).Less(0, 1))
	})
	t.Run("i greater j", func(t *testing.T) {
		foos := []RankedItem{
			{
				Rank: 2,
				Item: Foo{Name: "2"},
			},
			{
				Rank: 1,
				Item: Foo{Name: "1"},
			},
		}
		assert.Equal(t, false, ByRank(foos).Less(0, 1))
	})
	t.Run("i equals j", func(t *testing.T) {
		foos := []RankedItem{
			{
				Rank: 1,
				Item: Foo{Name: "1"},
			},
			{
				Rank: 1,
				Item: Foo{Name: "1"},
			},
		}
		assert.Equal(t, false, ByRank(foos).Less(0, 1))
	})
}

func TestByRank_Swap(t *testing.T) {
	foos := []RankedItem{
		{
			Rank: 1,
			Item: Foo{Name: "1"},
		},
		{
			Rank: 2,
			Item: Foo{Name: "2"},
		},
	}
	ByRank(foos).Swap(0, 1)
	assert.Equal(t, 2, foos[0].Rank)
	assert.Equal(t, 1, foos[1].Rank)
}
