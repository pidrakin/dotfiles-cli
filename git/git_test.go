package git

import (
	"fmt"
	"testing"

	"gitlab.com/pidrakin/go/tests"

	"github.com/stretchr/testify/assert"
)

var goGitWorkaroundSuccessUris = []string{
	"foobar.com",
	"git@foobar.com",
	"git:pw@foobar.com",
	"foobar.com:22",
	"git@foobar.com:22",
	"git:pw@foobar.com:22",
	"foobar.com:path",
	"foobar.com:/path/deep",
	"git@foobar.com:/path",
	"git:pw@foobar.com:/path",
	"git:pw@foobar.com:22:/path",
	"git@github.com:james/bond",
	"git@github.com:007/bond",
	"git@github.com:22:james/bond",
	"git@github.com:22:007/bond",
	"git@website.com:22:22/",
}

var goGitWorkaroundFailUris = []string{
	"https://git@foobar.com",
	"git://foo.bar:22/xxx",
}

var testSuccessUris = []string{
	"git@github.com:james/bond",
	"git@github.com:007/bond",
	"git@github.com:22:james/bond",
	"git@github.com:22:007/bond",
}

var testProtocols = []string{
	"ssh",
	"git",
	"git+ssh",
	"http",
	"https",
	"ftp",
	"ftps",
	"rsync",
}

var testFailedUris = []string{
	"git@git.example.com:22:repo",
	"git@git.example.com:repo.git",
	"git.example.com:repo.git",
}

func TestGoGitWorkaround(t *testing.T) {
	for _, uri := range goGitWorkaroundSuccessUris {
		t.Run(fmt.Sprintf("fail: %s", uri), func(t *testing.T) {
			err := goGitWorkaround(uri)
			tests.AssertEqualError(t, err, "SCP-like URIs are not supported by go-git; see: https://github.com/go-git/go-git/pull/324")
		})
	}
	for _, uri := range goGitWorkaroundFailUris {
		t.Run(fmt.Sprintf("fail: %s", uri), func(t *testing.T) {
			err := goGitWorkaround(uri)
			tests.AssertNoError(t, err)
		})
	}
}

func TestParseGitUri(t *testing.T) {
	t.Run("fail: empty", func(t *testing.T) {
		uri, err := parseGitUri("")
		assert.Empty(t, uri)
		tests.AssertEqualError(t, err, "[] not a git uri")
	})
	t.Run("parse protocol", func(t *testing.T) {
		for _, protocol := range testProtocols {
			protocol = protocol + "://"
			t.Run(fmt.Sprintf("success: %s", protocol), func(t *testing.T) {
				combined := fmt.Sprintf("%sfoobar", protocol)
				uri, err := parseGitUri(combined)
				assert.Equal(t, combined, uri)
				tests.AssertNoError(t, err)
			})
		}
		t.Run("fail: file://", func(t *testing.T) {
			fileUri := "file://foobar"
			uri, err := parseGitUri(fileUri)
			assert.Equal(t, fileUri, uri)
			tests.AssertEqualError(t, err, fmt.Sprintf("[%s] got parsed as a file-uri but path couldn't be found", fileUri))
		})

		t.Run("fail: foobar://", func(t *testing.T) {
			uri, err := parseGitUri("foobar://foobar")
			assert.Equal(t, "foobar://foobar", uri)
			tests.AssertEqualError(t, err, "[foobar] protocol not supported")
		})

		for _, testUri := range testFailedUris {
			output := fmt.Sprintf("fail: %s", testUri)
			t.Run(output, func(t *testing.T) {
				uri, err := parseGitUri(testUri)
				assert.Equal(t, testUri, uri)
				assert.Nil(t, err)
			})
		}

		for _, testUri := range testSuccessUris {
			output := fmt.Sprintf("success: %s", testUri)
			t.Run(output, func(t *testing.T) {
				uri, err := parseGitUri(testUri)
				assert.Equal(t, testUri, uri)
				tests.AssertNoError(t, err)
			})
		}
	})
}
