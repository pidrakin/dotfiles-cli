package git

type RankedItem struct {
	Rank int
	Item interface{}
}
type ByRank []RankedItem

func (a ByRank) Len() int           { return len(a) }
func (a ByRank) Less(i, j int) bool { return a[i].Rank < a[j].Rank }
func (a ByRank) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
