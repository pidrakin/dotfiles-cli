package git

import (
	"bytes"
	"fmt"
	"gitlab.com/pidrakin/go/tests"
	"os"
	"path/filepath"
	"testing"

	"github.com/go-git/go-git/v5"
	"github.com/go-git/go-git/v5/config"
	"github.com/go-git/go-git/v5/plumbing/object"
	"github.com/stretchr/testify/assert"
)

func TestIsRepo(t *testing.T) {
	repo := "repo.git"
	t.Run("fail: dir does not exist", func(t *testing.T) {
		defer tests.ChTmpDir(t)()

		check, err := goGitterIsRepo(repo)
		assert.False(t, check)
		tests.AssertEqualError(t, err, "[repo.git] does not exist")
	})
	t.Run("fail: not a dir", func(t *testing.T) {
		defer tests.ChTmpDir(t)()

		_, err := os.Create(repo)
		tests.AssertNoError(t, err)

		check, err := goGitterIsRepo(repo)
		assert.False(t, check)
		tests.AssertEqualError(t, err, "[repo.git] is not a directory")
	})
	t.Run("fail: is empty", func(t *testing.T) {
		defer tests.ChTmpDir(t)()

		err := os.Mkdir(repo, os.FileMode(0755))
		tests.AssertNoError(t, err)

		check, err := goGitterIsRepo(repo)
		assert.False(t, check)
		tests.AssertEqualError(t, err, "[repo.git] is empty")
	})
	t.Run("fail: not a git repository", func(t *testing.T) {
		defer tests.ChTmpDir(t)()

		err := os.Mkdir(repo, os.FileMode(0755))
		tests.AssertNoError(t, err)

		_, err = os.Create(filepath.Join(repo, "foobar.txt"))
		tests.AssertNoError(t, err)

		check, err := goGitterIsRepo(repo)
		assert.False(t, check)
		tests.AssertEqualError(t, err, "[repo.git] not a git repository")
	})
	t.Run("success", func(t *testing.T) {
		defer tests.ChTmpDir(t)()

		err := os.Mkdir(repo, os.FileMode(0755))
		tests.AssertNoError(t, err)

		_, err = git.PlainInit(repo, true)
		tests.AssertNoError(t, err)

		check, err := goGitterIsRepo(repo)
		assert.True(t, check)
		tests.AssertNoError(t, err)
	})
}

func setupRepo(t *testing.T, repo *string, localRepo *string, paths ...*string) func() {
	toDefer := tests.ChTmpDir(t)

	var err error
	*repo, err = filepath.Abs(*repo)
	tests.AssertNoError(t, err)

	*localRepo, err = filepath.Abs(*localRepo)
	tests.AssertNoError(t, err)

	for _, path := range paths {
		*path, err = filepath.Abs(*path)
		tests.AssertNoError(t, err)
	}

	err = os.Mkdir(*repo, os.FileMode(0755))
	tests.AssertNoError(t, err)

	_, err = git.PlainInit(*repo, true)
	tests.AssertNoError(t, err)

	err = os.MkdirAll(*localRepo, os.FileMode(0755))
	tests.AssertNoError(t, err)

	r, err := git.PlainInit(*localRepo, false)
	tests.AssertNoError(t, err)

	err = os.WriteFile(filepath.Join(*localRepo, "foobar.txt"), []byte("foobar"), os.FileMode(0644))
	tests.AssertNoError(t, err)

	w, err := r.Worktree()
	tests.AssertNoError(t, err)

	_, err = w.Add("foobar.txt")
	tests.AssertNoError(t, err)

	_, err = w.Commit("Initial commit", &git.CommitOptions{
		Author: &object.Signature{
			Name:  "Foo Bar",
			Email: "foo@bar.baz",
		},
	})
	tests.AssertNoError(t, err)

	remote, err := r.CreateRemote(&config.RemoteConfig{
		Name: "origin",
		URLs: []string{*repo},
	})
	tests.AssertNoError(t, err)

	var buf bytes.Buffer
	err = remote.Push(&git.PushOptions{
		Progress: &buf,
	})
	tests.AssertNoError(t, err)

	return toDefer
}

type repoTestFunc func(t *testing.T, repo string, localRepo string, target string)

func repoTest(t *testing.T, testFunc repoTestFunc) func(*testing.T) {
	repo := "repo.git"
	localRepo := "home/user/cloned"
	target := "home/user/repo"
	toDefer := setupRepo(t, &repo, &localRepo, &target)
	return func(t *testing.T) {
		defer toDefer()
		testFunc(t, repo, localRepo, target)
	}
}

func TestClone(t *testing.T) {
	t.Run("success: dry run", repoTest(t, func(t *testing.T, repo string, localRepo string, target string) {
		var buf bytes.Buffer
		_, err := goGitterClone(true, repo, target, &buf)
		tests.AssertNoError(t, err)

		check, err := goGitterIsRepo(target)
		assert.False(t, check)
		tests.AssertEqualError(t, err, fmt.Sprintf("[%s] does not exist", target))
	}))
	t.Run("success: from uri", repoTest(t, func(t *testing.T, repo string, localRepo string, target string) {
		var buf bytes.Buffer
		_, err := goGitterClone(false, repo, target, &buf)
		tests.AssertNoError(t, err)

		check, err := goGitterIsRepo(target)
		assert.True(t, check)
		tests.AssertNoError(t, err)

		tests.AssertNoError(t, err)
		tests.AssertFileExists(t, filepath.Join(target, "foobar.txt"))
	}))
	t.Run("success: from local", repoTest(t, func(t *testing.T, repo string, localRepo string, target string) {
		var buf bytes.Buffer
		_, err := goGitterClone(false, localRepo, target, &buf)
		tests.AssertNoError(t, err)

		check, err := goGitterIsRepo(target)
		assert.True(t, check)
		tests.AssertNoError(t, err)

		path, err := filepath.EvalSymlinks(filepath.Join(target, "foobar.txt"))
		tests.AssertFileExists(t, path)
	}))
}

func TestPull(t *testing.T) {
	t.Run("success: dry run", repoTest(t, func(t *testing.T, repo string, localRepo string, target string) {
		var buf bytes.Buffer
		_, err := goGitterPull(true, localRepo, &buf)
		tests.AssertNoError(t, err)
	}))
	t.Run("success: target", repoTest(t, func(t *testing.T, repo string, localRepo string, target string) {
		var buf bytes.Buffer
		_, err := goGitterPull(false, localRepo, &buf)
		tests.AssertNoError(t, err)
	}))
}

func updateRepo(t *testing.T, repo string) {
	tempRepo := filepath.Join(filepath.Dir(repo), "temp")

	var buf bytes.Buffer
	r, err := git.PlainClone(tempRepo, false, &git.CloneOptions{
		URL:      repo,
		Progress: &buf,
	})

	err = os.WriteFile(filepath.Join(tempRepo, "foobar2.txt"), []byte("foobar"), os.FileMode(0644))
	tests.AssertNoError(t, err)

	w, err := r.Worktree()
	tests.AssertNoError(t, err)

	_, err = w.Add("foobar2.txt")
	tests.AssertNoError(t, err)

	_, err = w.Commit("Update commit", &git.CommitOptions{
		Author: &object.Signature{
			Name:  "Foo Bar",
			Email: "foo@bar.baz",
		},
	})
	tests.AssertNoError(t, err)

	remote, err := r.Remote("origin")
	tests.AssertNoError(t, err)

	buf.Reset()
	err = remote.Push(&git.PushOptions{
		Progress: &buf,
	})
	tests.AssertNoError(t, err)
}

func TestBehind(t *testing.T) {
	t.Run("success: not behind", repoTest(t, func(t *testing.T, repo string, localRepo string, target string) {
		var buf bytes.Buffer
		behind, err := goGitterBehind(false, localRepo, &buf)
		tests.AssertNoError(t, err)
		assert.False(t, behind)
	}))
	t.Run("success: behind", repoTest(t, func(t *testing.T, repo string, localRepo string, target string) {
		updateRepo(t, repo)

		var buf bytes.Buffer
		behind, err := goGitterBehind(false, localRepo, &buf)
		tests.AssertNoError(t, err)
		assert.True(t, behind)
	}))
}
