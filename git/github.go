package git

import (
	"fmt"
	"regexp"
	"sort"
	"strings"

	"gitlab.com/pidrakin/dotfiles-cli/text"
	"gitlab.com/pidrakin/go/interactive"
)

type GithubProject struct {
	Name string `json:"name"`
	Url  string `json:"clone_url"`
}
type GithubProjectArray []GithubProject
type GithubProjectPredicate func(name GithubProject) bool

func (arr GithubProjectArray) Filter(f GithubProjectPredicate) GithubProjectArray {
	var projects GithubProjectArray
	for _, project := range arr {
		if f(project) {
			projects = append(projects, project)
		}
	}
	return projects
}

func (arr GithubProjectArray) Sorted() GithubProjectArray {
	var projects []RankedItem
	for _, project := range arr {
		if project.Name == "dotfiles" {
			projects = append(projects, RankedItem{
				Rank: 0,
				Item: project,
			})
		} else if project.Name == "dot-files" {
			projects = append(projects, RankedItem{
				Rank: 1,
				Item: project,
			})
		} else if project.Name == "dotfile" {
			projects = append(projects, RankedItem{
				Rank: 2,
				Item: project,
			})
		} else if project.Name == "dot-file" {
			projects = append(projects, RankedItem{
				Rank: 3,
				Item: project,
			})
		} else {
			projects = append(projects, RankedItem{
				Rank: 4 + len(project.Name) - 8,
				Item: project,
			})
		}
	}
	sort.Sort(ByRank(projects))
	var result GithubProjectArray
	for _, item := range projects {
		if project, ok := item.Item.(GithubProject); ok {
			result = append(result, project)
		}
	}
	return result
}

func githubUriToUsername(uri string) string {
	re := regexp.MustCompile("^(?:github://|gh:)")
	if re.MatchString(uri) {
		return re.ReplaceAllString(uri, "")
	}
	return ""
}

func githubUsernameToUri(username string) (string, error) {
	return _githubUsernameToUri(username, false)
}

func _githubUsernameToUri(username string, first bool) (string, error) {

	projectUrl := fmt.Sprintf("https://api.github.com/users/%s/repos", username)
	var projects GithubProjectArray
	if err := httpGetJSON(projectUrl, &projects); err != nil {
		return "", fmt.Errorf(text.NotAUser, username, "Github")
	}
	filtered := projects.Filter(func(project GithubProject) bool {
		return strings.Contains(strings.ToLower(strings.ReplaceAll(project.Name, "-", "")), "dotfile")
	})

	if len(filtered) == 0 {
		return "", fmt.Errorf(text.UserHasNoRepos, username, "Github")
	}

	sorted := filtered.Sorted()

	if first || len(sorted) == 1 {
		return sorted[0].Url, nil
	}

	var options []string
	for _, project := range sorted {
		options = append(options, project.Url)
	}

	prompter, err := interactive.NewPrompter(interactive.Interactive)
	if err != nil {
		return "", err
	}

	i, _, err := prompter.SingleChoice(
		options,
		text.RepoPromptQuestion,
	)
	if err != nil || i == -1 {
		return "", err
	}
	return options[i], nil
}
