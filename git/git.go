package git

import (
	"fmt"
	"gitlab.com/pidrakin/go/sysfs"
	"regexp"
	"strings"

	"github.com/go-git/go-git/v5/plumbing/transport"
	"gitlab.com/pidrakin/dotfiles-cli/text"
)

var Protocols = map[string]struct{}{
	"ssh":     {},
	"git":     {},
	"git+ssh": {},
	"http":    {},
	"https":   {},
	"ftp":     {},
	"ftps":    {},
	"rsync":   {},
	"file":    {},
}

type Gitter interface {
	SetDryRun(dryRun bool)
	IsRepo(target string) (bool, error)
	Clone(uri string, target string) (string, error)
	Pull(target string) (string, error)
	Behind(target string) (bool, error)
}

var gitter Gitter = &GoGitter{}

func SetGitter(g Gitter) {
	gitter = g
}

func SetDryRun(dryRun bool) {
	gitter.SetDryRun(dryRun)
}

func ParseUri(uri string) (string, error) {
	return parseGitUri(uri)
}

func IsRepo(target string) (bool, error) {
	return gitter.IsRepo(target)
}

func Clone(uri string, target string) (string, error) {
	return gitter.Clone(uri, target)
}

func Pull(target string) (string, error) {
	return gitter.Pull(target)
}

func Behind(target string) (bool, error) {
	return gitter.Behind(target)
}

func goGitWorkaround(uri string) error {
	//scpLikeUri := regexp.MustCompile(`^(?:(?P<user>[^@]+)@)?(?P<host>[^:\s]+):(?:(?P<port>[0-9]{1,5})(?::))?(?P<path>[^?#]*)$`)
	schemaLike := regexp.MustCompile(`^[^:]+:\/\/`)
	if schemaLike.MatchString(uri) {
		return nil
	}
	scpLikeUri := regexp.MustCompile(`^(?:(?P<user>[^@:\s\/]+)(?::(?P<pass>[^@\s\/]+))?@)?(?P<host>[^:\s\/]+)(?::(?P<port>[0-9]+))?(?::(?P<path>(?:[^?#\s\/]|\/[^\s\/])*(?:[^\s\/]\/)?))?$`)
	groupList := scpLikeUri.FindStringSubmatch(uri)
	if groupList == nil {
		return nil
	}
	names := scpLikeUri.SubexpNames()
	groups := map[string]string{}
	for idx, name := range names {
		groups[name] = groupList[idx]
	}
	if scpLikeUri.MatchString(uri) {
		return fmt.Errorf(text.GoGitInvalidUri)
	}
	return nil
}

func fileUriCheck(uri string) error {
	if strings.HasPrefix(uri, "file://") {
		uriPath := strings.Split(uri, "file://")
		if sysfs.DirExists(uriPath[1]) {
			return nil
		}
		return fmt.Errorf(text.FileUriPathNotFound, uri)
	}
	return goGitWorkaround(uri)
}

func parseGitUri(uri string) (string, error) {
	if uri == "" {
		return uri, fmt.Errorf(text.NotAGitUri, uri)
	}
	if username := gitlabUriToUsername(uri); username != "" {
		gitlabUri, err := gitlabUsernameToUri(username)
		if err != nil {
			return uri, err
		}
		uri = gitlabUri
	}
	if username := githubUriToUsername(uri); username != "" {
		githubUri, err := githubUsernameToUri(username)
		if err != nil {
			return uri, err
		}
		uri = githubUri
	}

	parsedUri, err := transport.NewEndpoint(uri)
	if err != nil {
		return uri, fmt.Errorf(text.NotAGitUri, uri)
	}

	if parsedUri.Protocol == "file" {
		if sysfs.DirExists(uri) {
			return uri, nil
		}
		if err = fileUriCheck(uri); err != nil {
			return uri, err
		}
		return uri, fmt.Errorf(text.FileUriPathNotFound, uri)
	}

	if _, ok := Protocols[parsedUri.Protocol]; !ok {
		return uri, fmt.Errorf(text.ProtocolNotSupported, parsedUri.Protocol)
	}

	return uri, err
}
