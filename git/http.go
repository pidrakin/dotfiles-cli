package git

import (
	"encoding/json"
	"net/http"
)

// TODO: if we can remove net/http in the future, this is how you do the manual reqs
// func httpGetJSON(url string, result interface{}) error {
// 	host, port, path, useTLS := parseURL(url)
// 	if host == "" || path == "" {
// 		return fmt.Errorf("invalid URL")
// 	}

// 	address := host + ":" + port

// 	var conn net.Conn
// 	var err error

// 	if useTLS {
// 		conn, err = tls.Dial("tcp", address, &tls.Config{InsecureSkipVerify: true})
// 	} else {
// 		conn, err = net.Dial("tcp", address)
// 	}
// 	if err != nil {
// 		return err
// 	}
// 	defer conn.Close()

// 	fmt.Fprintf(conn, "GET %s HTTP/1.1\r\n", path)
// 	fmt.Fprintf(conn, "Host: %s\r\n", host)
// 	fmt.Fprintf(conn, "Accept: application/json\r\n")
// 	fmt.Fprintf(conn, "User-Agent: dotfiles/0.0\r\n")
// 	fmt.Fprintf(conn, "Connection: close\r\n\r\n")

// 	// Read the complete response
// 	respBytes, err := io.ReadAll(conn)
// 	if err != nil {
// 		return err
// 	}

// 	idx := bytes.Index(respBytes, []byte("\r\n\r\n"))
// 	if idx == -1 {
// 		return fmt.Errorf("malformed HTTP response")
// 	}
// 	bodyStart := idx + 4 // Skip past the "\r\n\r\n"

// 	// Handle chunked response
// 	var responseBody []byte
// 	if isChunked(respBytes[:idx]) {
// 		responseBody, err = unchunkBody(respBytes[bodyStart:])
// 		if err != nil {
// 			return err
// 		}
// 	} else {
// 		responseBody = respBytes[bodyStart:]
// 	}

// 	// Decode JSON from the body
// 	if err := json.Unmarshal(responseBody, result); err != nil {
// 		return err
// 	}

// 	return nil
// }

// // Check if the response is using chunked transfer encoding
// func isChunked(header []byte) bool {
// 	return bytes.Contains(header, []byte("Transfer-Encoding: chunked"))
// }

// // Process a chunked body to remove chunk sizes and get the actual body
// func unchunkBody(body []byte) ([]byte, error) {
// 	reader := bytes.NewReader(body)
// 	var result []byte

// 	for {
// 		// Read the size of the chunk
// 		sizeStr, err := readLine(reader)
// 		if err != nil {
// 			return nil, err
// 		}

// 		// Convert hex size to decimal
// 		size, err := strconv.ParseInt(sizeStr, 16, 64)
// 		if err != nil {
// 			return nil, fmt.Errorf("invalid chunk size: %v", err)
// 		}
// 		if size == 0 { // End of message
// 			break
// 		}

// 		chunk := make([]byte, size)
// 		_, err = io.ReadFull(reader, chunk)
// 		if err != nil {
// 			return nil, err
// 		}
// 		result = append(result, chunk...)

// 		// Consume the trailing \r\n after the chunk
// 		_, err = readLine(reader)
// 		if err != nil {
// 			return nil, err
// 		}
// 	}

// 	return result, nil
// }

// // readLine reads bytes until \n or EOF and returns the string without \r\n or \n
// func readLine(r *bytes.Reader) (string, error) {
// 	var line []byte
// 	for {
// 		b, err := r.ReadByte()
// 		if err != nil {
// 			if err == io.EOF {
// 				return string(line), nil // EOF is ok if we're reading the size (last chunk can be just "0")
// 			}
// 			return "", err
// 		}
// 		if b == '\n' { // End of line
// 			break
// 		}
// 		line = append(line, b)
// 	}
// 	return strings.TrimRight(string(line), "\r"), nil // Remove potential \r
// }

// // Enhanced parseURL function for extracting host, port, path, and a flag indicating HTTPS.
// func parseURL(url string) (host, port, path string, useTLS bool) {
// 	if strings.HasPrefix(url, "https://") {
// 		useTLS = true
// 		port = "443" // Default port for HTTPS
// 		url = strings.TrimPrefix(url, "https://")
// 	} else if strings.HasPrefix(url, "http://") {
// 		port = "80" // Default port for HTTP
// 		url = strings.TrimPrefix(url, "http://")
// 	} else {
// 		return "", "", "", false
// 	}

// 	// Extracting host and optional port
// 	hostPortPath := strings.SplitN(url, "/", 2)
// 	hostPort := hostPortPath[0]
// 	var pathPart string
// 	if len(hostPortPath) > 1 {
// 		pathPart = "/" + hostPortPath[1]
// 	} else {
// 		pathPart = "/"
// 	}

// 	hostParts := strings.SplitN(hostPort, ":", 2)
// 	if len(hostParts) == 2 {
// 		host = hostParts[0]
// 		port = hostParts[1] // Override port if specified
// 	} else {
// 		host = hostParts[0]
// 	}

// 	return host, port, pathPart, useTLS
// }

func httpGetJSON(url string, result interface{}) error {
	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		return err
	}
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Accept", `application/json`)
	if err != nil {
		return err
	}

	client := &http.Client{}
	res, err := client.Do(req)
	if err != nil {
		return err
	}

	defer res.Body.Close()
	if err = json.NewDecoder(res.Body).Decode(result); err != nil {
		return err
	}
	return nil
}
