package git

import (
	"fmt"
	"regexp"
	"sort"
	"strings"

	"gitlab.com/pidrakin/dotfiles-cli/text"
	"gitlab.com/pidrakin/go/interactive"
)

type GitlabUser struct {
	Id       int    `json:"id"`
	Username string `json:"username"`
}
type GitlabUserArray []GitlabUser

func (arr GitlabUserArray) Get(username string) *GitlabUser {
	for _, u := range arr {
		if u.Username == username {
			return &u
		}
	}
	return nil
}

type Release struct {
	Name string `json:"name"`
}
type GitlabGroup struct {
	Path string `json:"path"`
}

type GitlabProject struct {
	Path string `json:"path"`
	Url  string `json:"http_url_to_repo"`
}
type GitlabProjectArray []GitlabProject
type GitlabProjectPredicate func(GitlabProject) bool

func (arr GitlabProjectArray) Filter(f GitlabProjectPredicate) GitlabProjectArray {
	var projects GitlabProjectArray
	for _, project := range arr {
		if f(project) {
			projects = append(projects, project)
		}
	}
	return projects
}

func (arr GitlabProjectArray) Sorted() GitlabProjectArray {
	var projects []RankedItem
	for _, project := range arr {
		if project.Path == "dotfiles" {
			projects = append(projects, RankedItem{
				Rank: 0,
				Item: project,
			})
		} else if project.Path == "dot-files" {
			projects = append(projects, RankedItem{
				Rank: 1,
				Item: project,
			})
		} else if project.Path == "dotfile" {
			projects = append(projects, RankedItem{
				Rank: 2,
				Item: project,
			})
		} else if project.Path == "dot-file" {
			projects = append(projects, RankedItem{
				Rank: 3,
				Item: project,
			})
		} else {
			projects = append(projects, RankedItem{
				Rank: 4 + len(project.Path) - 8,
				Item: project,
			})
		}
	}
	sort.Sort(ByRank(projects))
	var result GitlabProjectArray
	for _, item := range projects {
		if project, ok := item.Item.(GitlabProject); ok {
			result = append(result, project)
		}
	}
	return result
}

func gitlabUriToUsername(uri string) string {
	re := regexp.MustCompile("^(?:gitlab://|gl:)")
	if re.MatchString(uri) {
		return re.ReplaceAllString(uri, "")
	}
	return ""
}

func GetLatestVersion() (string, error) {
	var release Release
	err := httpGetJSON("https://gitlab.com/api/v4/projects/24946569/releases/permalink/latest", &release)
	if err != nil {
		return "", err
	}
	latestVersion := release.Name
	return latestVersion, nil
}

func gitlabUsernameToUri(username string) (string, error) {
	return _gitlabUsernameToUri(username, false)
}

func _gitlabUsernameToUri(username string, first bool) (string, error) {

	userUrl := fmt.Sprintf("https://gitlab.com/api/v4/users?username=%s", username)
	var users GitlabUserArray
	if err := httpGetJSON(userUrl, &users); err != nil {
		return "", err
	}
	var projectUrl string
	if len(users) > 0 {
		user := users.Get(username)
		if user == nil {
			return "", fmt.Errorf(text.NotAUser, username, "Gitlab")
		}
		projectUrl = fmt.Sprintf("https://gitlab.com/api/v4/users/%d/projects", user.Id)
	} else {
		groupUrl := fmt.Sprintf("https://gitlab.com/api/v4/groups/%s", username)
		var group GitlabGroup
		if err := httpGetJSON(groupUrl, &group); err != nil || group.Path == "" {
			return "", fmt.Errorf(text.NotAUser, username, "Gitlab")
		}
		projectUrl = fmt.Sprintf("https://gitlab.com/api/v4/groups/%s/projects", username)
	}
	var projects GitlabProjectArray
	if err := httpGetJSON(projectUrl, &projects); err != nil {
		return "", err
	}
	filtered := projects.Filter(func(project GitlabProject) bool {
		return strings.Contains(strings.ToLower(strings.ReplaceAll(project.Path, "-", "")), "dotfile")
	})

	if len(filtered) == 0 {
		return "", fmt.Errorf(text.UserHasNoRepos, username, "Gitlab")
	}

	sorted := filtered.Sorted()

	if first || len(sorted) == 1 {
		return sorted[0].Url, nil
	}

	var options []string
	for _, project := range sorted {
		options = append(options, project.Url)
	}

	prompter, err := interactive.NewPrompter(interactive.Interactive)
	if err != nil {
		return "", err
	}

	i, _, err := prompter.SingleChoice(
		options,
		text.RepoPromptQuestion,
	)
	if err != nil || i == -1 {
		return "", err
	}
	return options[i], nil
}
