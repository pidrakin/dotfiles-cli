package git

import (
	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"gitlab.com/pidrakin/go/tests"
	"os"
	"testing"
)

func TestMockGitter(t *testing.T) {
	defer tests.ChTmpDir(t)()
	log.SetLevel(log.FatalLevel)

	uri := "repository"
	target := "target"

	err := os.Mkdir(uri, os.FileMode(0755))
	tests.AssertNoError(t, err)

	gitter = &MockGitter{}

	t.Run("success: isRepo should be false", func(t *testing.T) {
		isRepo, err := gitter.IsRepo(target)
		tests.AssertNoError(t, err)
		assert.False(t, isRepo)
	})

	t.Run("success: clone", func(t *testing.T) {
		_, err := gitter.Clone(uri, target)
		tests.AssertNoError(t, err)
	})

	t.Run("success: isRepo should be true", func(t *testing.T) {
		isRepo, err := gitter.IsRepo(target)
		tests.AssertNoError(t, err)
		assert.True(t, isRepo)
	})

	t.Run("fail: clone", func(t *testing.T) {
		_, err := gitter.Clone(uri, target)
		tests.AssertEqualError(t, err, "target [target] is already a repository")
	})

	t.Run("success: pull", func(t *testing.T) {
		_, err := gitter.Pull(target)
		tests.AssertNoError(t, err)
	})
}
