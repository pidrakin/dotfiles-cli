package git

import (
	"errors"
	"fmt"
	"github.com/go-git/go-git/v5/plumbing"
	"github.com/go-git/go-git/v5/plumbing/transport"
	"github.com/go-git/go-git/v5/plumbing/transport/ssh"
	"github.com/mitchellh/go-homedir"
	"gitlab.com/pidrakin/dotfiles-cli/common/logging"
	"gitlab.com/pidrakin/dotfiles-cli/ssh_ultimate"
	sysfs2 "gitlab.com/pidrakin/go/sysfs"
	"io"
	"os"
	"path/filepath"
	"strings"
	"syscall"

	"github.com/go-git/go-git/v5"
	"github.com/go-git/go-git/v5/config"
	log "github.com/sirupsen/logrus"
	"gitlab.com/pidrakin/dotfiles-cli/text"
)

type GoGitter struct {
	dryRun bool
}

func goGitterIsRepo(target string) (check bool, err error) {
	if !sysfs2.Exists(target) {
		return false, fmt.Errorf(text.RepoDoesNotExist, target)
	}
	if !sysfs2.DirExists(target) {
		return false, fmt.Errorf(text.RepoNotADir, target)
	}
	empty, err := sysfs2.IsEmpty(target)
	if err != nil {
		return false, err
	}
	if empty {
		return false, fmt.Errorf(text.RepoEmpty, target)
	}

	log.WithField("target", target).Debug(text.RepoNotEmpty)

	_, err = git.PlainOpen(target)
	if errors.Is(err, git.ErrRepositoryNotExists) {
		return false, fmt.Errorf(text.RepoNotGit, target)
	} else if err != nil {
		return false, err
	}
	return true, nil
}

func (g *GoGitter) IsRepo(target string) (bool, error) {
	return goGitterIsRepo(target)
}

func (g *GoGitter) SetDryRun(dryRun bool) {
	g.dryRun = dryRun
}

func goGitterAuth(uri string) (ssh.AuthMethod, error) {
	// go-git workaround
	// we use almost the same code as inside go-git to determine the Auth method
	// we also override the host key callback to the one used in the ssh_ultimate package
	var auth ssh.AuthMethod
	ep, err := transport.NewEndpoint(uri)
	if err == nil && ep.Protocol == "ssh" {

		sshConfig, loadConfigErr := ssh_ultimate.LoadConfig()
		if loadConfigErr != nil {
			var pathError *os.PathError
			ok := errors.As(loadConfigErr, &pathError)
			if !ok {
				return nil, loadConfigErr
			}
		}
		if sshConfig != nil {
			if identityAgent, getConfigErr := sshConfig.Get(ep.Host, "IdentityAgent"); getConfigErr == nil {
				if identityAgent, getConfigErr = homedir.Expand(identityAgent); getConfigErr != nil {
					return nil, getConfigErr
				}
				if setEnvErr := os.Setenv("SSH_AUTH_SOCK", identityAgent); setEnvErr != nil {
					return nil, setEnvErr
				}
			}
		}

		auth, err = ssh.DefaultAuthBuilder(ep.User)
		if err != nil {
			return auth, err
		}
		auth.(*ssh.PublicKeysCallback).HostKeyCallback = ssh_ultimate.TrustedHostKeyCallback("")
	}
	return auth, nil
}

func goGitterIsBare(uri string) (bool, error) {
	repo, err := git.PlainOpen(uri)
	if err != nil {
		return false, err
	}
	var cfg *config.Config
	cfg, err = repo.Config()
	if err != nil {
		return false, err
	}
	return cfg.Core.IsBare, nil
}

func goGitterRemote(target string) (*git.Repository, string, error) {
	repo, err := git.PlainOpen(target)
	if errors.Is(err, git.ErrRepositoryNotExists) {
		return nil, "", fmt.Errorf(text.RepoNotGit, target)
	} else if err != nil {
		return nil, "", err
	}

	remotes, err := repo.Remotes()
	if err != nil {
		return nil, "", err
	}
	var remote string
	if len(remotes) > 0 {
		remote = strings.Fields(remotes[0].String())[1]
	}
	return repo, remote, nil
}

func goGitterClone(dryRun bool, uri string, target string, writer io.Writer) (string, error) {
	defer logging.SetLogOutput(writer)()
	parent := filepath.Dir(target)
	log.WithField("parent", parent).Debug(text.EnsureFileExists)
	if !dryRun {
		if err := os.MkdirAll(parent, os.FileMode(0755)); err != nil {
			var pathError *os.PathError
			ok := errors.As(err, &pathError)
			if ok && pathError.Err == syscall.Errno(30) {
				return uri, fmt.Errorf(text.NoPermissionsToClone, target)
			}
			return uri, err
		}
	}
	uri, err := parseGitUri(uri)
	if err != nil {
		return uri, err
	}
	log.WithFields(log.Fields{
		"repository": uri,
		"target":     target,
	}).Info(text.TryClone)
	if !dryRun {
		linked := false
		if sysfs2.DirExists(uri) {
			var isBare bool
			isBare, err = goGitterIsBare(uri)
			if err != nil {
				return uri, err
			}
			if !isBare {
				absolutePath, err := filepath.Abs(uri)
				if err != nil {
					return uri, err
				}
				if err = sysfs2.Symlink(absolutePath, target, writer); err != nil {
					return uri, err
				}
				linked = true
				_, uri, err = goGitterRemote(uri)
				if err != nil {
					return uri, err
				}
			}
		}
		if !linked {
			auth, err := goGitterAuth(uri)
			if err != nil {
				return uri, err
			}
			if _, err = git.PlainClone(target, false, &git.CloneOptions{
				URL:      uri,
				Auth:     auth,
				Progress: writer,
			}); err != nil {
				return uri, err
			}
		}
	}
	log.WithFields(log.Fields{
		"repository": uri,
		"target":     target,
	}).Info(text.Cloned)
	return uri, nil
}

func (g *GoGitter) Clone(uri string, target string) (string, error) {
	return goGitterClone(g.dryRun, uri, target, nil)
}

func goGitterPull(dryRun bool, target string, writer io.Writer) (string, error) {
	defer logging.SetLogOutput(writer)()
	entry := log.WithFields(log.Fields{
		"target": target,
	})

	repo, remote, err := goGitterRemote(target)
	if err != nil {
		return "", err
	}

	worktree, err := repo.Worktree()
	if err != nil {
		return remote, fmt.Errorf(text.CannotRetrieveWorkingTree)
	}

	entry.Info(text.TryPull)

	if !dryRun {
		auth, authErr := goGitterAuth(remote)
		if authErr != nil {
			return remote, authErr
		}
		pullErr := worktree.Pull(&git.PullOptions{
			Progress: writer,
			Auth:     auth,
		})

		if errors.Is(pullErr, git.NoErrAlreadyUpToDate) {
			entry.Info(text.AlreadyUpToDate)
			return remote, nil
		} else if pullErr != nil {
			return remote, fmt.Errorf(text.CannotPull, pullErr)
		}
	}
	entry.Info(text.SuccessfulPull)

	return remote, nil
}

func (g *GoGitter) Pull(target string) (string, error) {
	return goGitterPull(g.dryRun, target, nil)
}

func goGitterBehind(dryRun bool, target string, writer io.Writer) (bool, error) {
	defer logging.SetLogOutput(writer)()

	repo, err := git.PlainOpen(target)
	if errors.Is(err, git.ErrRepositoryNotExists) {
		return false, fmt.Errorf(text.RepoNotGit, target)
	} else if err != nil {
		return false, err
	}

	remotes, err := repo.Remotes()
	if err != nil {
		return false, err
	}
	//var remote string
	var remoteName string
	if len(remotes) > 0 {
		//remote = strings.Fields(remotes[0].String())[1]
		remoteName = remotes[0].Config().Name
	}

	if !dryRun {
		err = repo.Fetch(&git.FetchOptions{
			RemoteName: remoteName,
			Progress:   writer,
		})
		if err != nil {
			if err.Error() != "already up-to-date" {
				return false, err
			}
		}
	}

	headRef, err := repo.Head()
	if err != nil {
		return false, err
	}
	headCommit, err := repo.CommitObject(headRef.Hash())
	if err != nil {
		return false, err
	}

	revision := fmt.Sprintf("%s/%s", remoteName, headRef.Name().Short())
	revHash, err := repo.ResolveRevision(plumbing.Revision(revision))
	if err != nil {
		return false, err
	}
	revCommit, err := repo.CommitObject(*revHash)
	if err != nil {
		return false, err
	}

	isAncestor, err := headCommit.IsAncestor(revCommit)
	if err != nil {
		return false, err
	}

	return isAncestor && headCommit.Hash != revCommit.Hash, nil
}

func (g *GoGitter) Behind(target string) (bool, error) {
	return goGitterBehind(g.dryRun, target, nil)
}
