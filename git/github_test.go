package git

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	giturls "github.com/whilp/git-urls"
	"gitlab.com/pidrakin/go/tests"
	"testing"
)

func TestGithubProjectArray_Filter(t *testing.T) {
	t.Run("fail: array empty", func(t *testing.T) {
		var array GithubProjectArray
		result := array.Filter(func(project GithubProject) bool {
			return true
		})
		assert.Empty(t, result)
	})
	t.Run("fail: filter does not yield anything", func(t *testing.T) {
		projects := GithubProjectArray{GithubProject{Name: "dotfiles", Url: "https://github.com/user/dotfiles.git"}}
		result := projects.Filter(func(project GithubProject) bool {
			return project.Name == ""
		})
		assert.Empty(t, result)
	})
	t.Run("success", func(t *testing.T) {
		projects := GithubProjectArray{GithubProject{Name: "dotfiles", Url: "https://github.com/user/dotfiles.git"}}
		result := projects.Filter(func(project GithubProject) bool {
			return project.Name == "dotfiles"
		})
		assert.NotEmpty(t, result)
		assert.Equal(t, "dotfiles", result[0].Name)
	})
}

func TestGithubProjectArray_Sorted(t *testing.T) {
	projects := GithubProjectArray{
		GithubProject{Name: "dot-files-cli"},
		GithubProject{Name: "dot-files"},
		GithubProject{Name: "dotfile"},
		GithubProject{Name: "dotfiles"},
		GithubProject{Name: "dotfile-"},
		GithubProject{Name: "dot-file"},
	}
	sorted := projects.Sorted()
	assert.Equal(t, "dotfiles", sorted[0].Name)
	assert.Equal(t, "dot-files", sorted[1].Name)
	assert.Equal(t, "dotfile", sorted[2].Name)
	assert.Equal(t, "dot-file", sorted[3].Name)
	assert.Equal(t, "dotfile-", sorted[4].Name)
	assert.Equal(t, "dot-files-cli", sorted[5].Name)
}

func TestGithubUriToUsername(t *testing.T) {
	t.Run("fail: empty", func(t *testing.T) {
		username := githubUriToUsername("")
		assert.Empty(t, username)
	})
	protocols := giturls.Transports.Transports
	for protocol := range protocols {
		t.Run(fmt.Sprintf("fail: protocol %s", protocol), func(t *testing.T) {
			username := githubUriToUsername(fmt.Sprintf("%sfoobar", protocol))
			assert.Empty(t, username)
		})
	}
	t.Run("success: github://", func(t *testing.T) {
		username := githubUriToUsername("github://foobar")
		assert.Equal(t, "foobar", username)
	})
	t.Run("success: gh:", func(t *testing.T) {
		username := githubUriToUsername("gh:foobar")
		assert.Equal(t, "foobar", username)
	})
}

func TestParseGithubUri(t *testing.T) {
	t.Run("b3n4kh", func(t *testing.T) {
		uri, err := _githubUsernameToUri("b3n4kh", true)
		tests.AssertNoError(t, err)
		assert.Equal(t, "https://github.com/b3n4kh/dotfiles.git", uri)
	})
	t.Run("satanik", func(t *testing.T) {
		uri, err := _githubUsernameToUri("satanik", true)
		tests.AssertEqualError(t, err, "user [satanik] has no dotfile, dotfiles, dot-file or dot-files repositories on Github")
		assert.Equal(t, "", uri)
	})
	t.Run("+43", func(t *testing.T) {
		uri, err := _githubUsernameToUri("+43", true)
		tests.AssertEqualError(t, err, "user [+43] does not exist on Github")
		assert.Equal(t, "", uri)
	})
}
