package git

import (
	"fmt"
	sysfs2 "gitlab.com/pidrakin/go/sysfs"
	"io"
)

type MockGitter struct {
	dryRun bool
	isRepo map[string]bool
	uri    string
	writer io.Writer
}

func (m *MockGitter) SetDryRun(dryRun bool) {
	m.dryRun = dryRun
}

func mockGitterIsRepo(isRepo map[string]bool, target string) (bool, error) {
	if isRepo == nil {
		return false, nil
	}
	v, ok := isRepo[target]
	return ok && v, nil
}

func (m *MockGitter) IsRepo(target string) (bool, error) {
	return mockGitterIsRepo(m.isRepo, target)
}

func mockGitterClone(isRepo map[string]bool, uri string, target string, writer io.Writer) (string, error) {
	isR, err := mockGitterIsRepo(isRepo, target)
	if err != nil {
		return uri, err
	}
	if isR {
		return uri, fmt.Errorf("target [%s] is already a repository", target)
	}
	if sysfs2.DirExists(target) {
		isEmpty, err := sysfs2.IsEmpty(target)
		if err != nil {
			return uri, err
		}
		if !isEmpty {
			return uri, fmt.Errorf("target %s is not empty", target)
		}
	}
	isRepo[target] = true

	return uri, sysfs2.Copy(uri, target, writer)
}

func (m *MockGitter) Clone(uri string, target string) (string, error) {
	if m.isRepo == nil {
		m.isRepo = map[string]bool{}
	}
	m.uri = uri
	return mockGitterClone(m.isRepo, uri, target, m.writer)
}

func (m *MockGitter) Pull(target string) (string, error) {
	return m.uri, nil
}

func (m *MockGitter) Behind(target string) (bool, error) {
	return false, nil
}
