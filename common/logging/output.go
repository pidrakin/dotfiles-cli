package logging

import (
	"fmt"
	"github.com/fatih/color"
	log "github.com/sirupsen/logrus"
	"io"
	"os"
)

var (
	Writer   io.Writer = os.Stdout
	ExitFunc           = os.Exit
)

func SetOutputWriter(writer io.Writer) func() {
	if writer == nil {
		return func() {}
	}
	temp := Writer
	Writer = writer
	return func() {
		Writer = temp
	}
}

func SetOutputExitFunc(exitFunc func(int)) func() {
	if exitFunc == nil {
		return func() {}
	}
	temp := ExitFunc
	ExitFunc = exitFunc
	return func() {
		ExitFunc = temp
	}
}

func Successfully(text string) error {
	tick := color.New(color.FgGreen, color.Bold).Sprintf("✔")
	_, err := fmt.Fprintf(Writer, "[ %s ] successfully %s 🎉\n", tick, text)
	return err
}

func Failed(text string, args ...interface{}) error {
	log.Errorf(text, args...)
	tick := color.New(color.FgRed, color.Bold).Sprintf("✘")
	if _, err := fmt.Fprintf(Writer, "[ %s ] failed %s 😭\n", tick, fmt.Sprintf(text, args...)); err != nil {
		return err
	}
	ExitFunc(1)
	return nil
}
