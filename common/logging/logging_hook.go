package logging

import (
	"fmt"
	"github.com/mitchellh/go-homedir"
	log "github.com/sirupsen/logrus"
	"io"
	"os"
	"path/filepath"
)

type LogTarget uint

const (
	STDERR LogTarget = 0
	STDOUT           = 1
	FILE             = 2
)

type LoggingHook struct {
	writer io.Writer
	target LogTarget
}

func NewLoggingHook(target LogTarget) (*LoggingHook, error) {
	hook := &LoggingHook{
		target: target,
	}
	if target == STDERR {
		hook.writer = os.Stderr
		log.SetLevel(log.WarnLevel)
		log.SetOutput(hook.writer)
	} else if target == STDOUT {
		hook.writer = os.Stdout
		log.SetLevel(log.WarnLevel)
		log.SetOutput(hook.writer)
	} else if target == FILE {
		home, err := homedir.Dir()
		if err != nil {
			return nil, err
		}
		logFile := filepath.Join(home, ".local/share/dotfiles", "dotfiles.log")
		file, err := os.OpenFile(logFile, os.O_CREATE|os.O_WRONLY|os.O_APPEND, os.FileMode(0644))
		if err != nil {
			return nil, err
		}
		hook.writer = file
		log.SetLevel(log.TraceLevel)
	}
	return hook, nil
}

func (hook *LoggingHook) Fire(entry *log.Entry) error {
	line, err := entry.String()
	if err != nil {
		_, _ = fmt.Fprintf(os.Stderr, "Unable to read entry, %v", err)
		return err
	}

	if hook.target == FILE {
		_, err = hook.writer.Write([]byte(line))
	}
	return err
}

func (hook *LoggingHook) Levels() []log.Level {
	return log.AllLevels
}
