package logging

import (
	log "github.com/sirupsen/logrus"
	"io"
)

func SetLogOutput(writer io.Writer) func() {
	if writer == nil {
		return func() {}
	}
	output := log.StandardLogger().Out
	log.SetOutput(writer)
	return func() {
		log.SetOutput(output)
	}
}

func SetLogExitFunc(exitFunc func(int)) func() {
	if exitFunc == nil {
		return func() {}
	}
	f := log.StandardLogger().ExitFunc
	log.StandardLogger().ExitFunc = exitFunc
	return func() {
		log.StandardLogger().ExitFunc = f
	}
}

func SetLogLevel(level log.Level) func() {
	l := log.GetLevel()
	log.SetLevel(level)
	return func() {
		log.SetLevel(l)
	}
}
