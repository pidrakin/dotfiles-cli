package logging

import (
	"bytes"
	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"strings"
	"testing"
)

func TestSetLogOutput(t *testing.T) {
	t.Run("writer is nil", func(t *testing.T) {
		oldWriter := log.StandardLogger().Out
		toDefer := SetLogOutput(nil)
		writer := log.StandardLogger().Out
		assert.Equal(t, oldWriter, writer)
		toDefer()
		writer = log.StandardLogger().Out
		assert.Equal(t, oldWriter, writer)
	})
	t.Run("writer is buffer", func(t *testing.T) {
		oldWriter := log.StandardLogger().Out
		var buf bytes.Buffer
		toDefer := SetLogOutput(&buf)
		writer := log.StandardLogger().Out
		cast, ok := writer.(*bytes.Buffer)
		assert.True(t, ok)
		assert.Equal(t, buf, *(cast))
		log.Info("foobar")
		message := strings.Split(buf.String(), " ")[2]
		assert.Equal(t, "msg=foobar\n", message)
		toDefer()
		writer = log.StandardLogger().Out
		assert.Equal(t, oldWriter, writer)
	})
}

func TestSetLogExitFunc(t *testing.T) {
	t.Run("exitFunc is nil", func(t *testing.T) {
		oldExitFunc := log.StandardLogger().ExitFunc
		toDefer := SetLogExitFunc(nil)
		exitFunc := log.StandardLogger().ExitFunc
		assert.NotNil(t, oldExitFunc)
		assert.NotNil(t, exitFunc)
		toDefer()
		exitFunc = log.StandardLogger().ExitFunc
		assert.NotNil(t, exitFunc)
	})
	t.Run("exitFunc is overriding int", func(t *testing.T) {
		var exitValue *int
		eF := func(value int) {
			exitValue = &value
		}
		toDefer := SetLogExitFunc(eF)
		log.Fatal("second fatal exit")
		toDefer()
		assert.Equal(t, 1, *exitValue)
	})
}
