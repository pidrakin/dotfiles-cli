package regex

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/pidrakin/go/regex"
	"testing"
)

func Test_UserHostPortRegex(t *testing.T) {
	test0 := ""
	test1 := "foobar"
	test2 := "foo@bar"
	test3 := "foobar:6666"
	test4 := "foo@bar:6666"
	test5 := "ssh://foo@bar:6666"
	test6 := "f@@oo::bar:6666"

	assert.False(t, UserHostPortRegex.MatchString(test0))
	assert.True(t, UserHostPortRegex.MatchString(test1))
	assert.True(t, UserHostPortRegex.MatchString(test2))
	assert.True(t, UserHostPortRegex.MatchString(test3))
	assert.True(t, UserHostPortRegex.MatchString(test4))
	assert.True(t, UserHostPortRegex.MatchString(test5))
	assert.False(t, UserHostPortRegex.MatchString(test6))
}

func Test_MatchGroups_UserHostPortRegex(t *testing.T) {
	test0 := ""
	test1 := "foobar"
	test2 := "foo@bar"
	test3 := "foobar:6666"
	test4 := "foo@bar:6666"
	test5 := "ssh://foo@bar:6666"
	test6 := "f@@oo::bar:6666"

	groups0 := regex.MatchGroups(UserHostPortRegex, test0)
	groups1 := regex.MatchGroups(UserHostPortRegex, test1)
	groups2 := regex.MatchGroups(UserHostPortRegex, test2)
	groups3 := regex.MatchGroups(UserHostPortRegex, test3)
	groups4 := regex.MatchGroups(UserHostPortRegex, test4)
	groups5 := regex.MatchGroups(UserHostPortRegex, test5)
	groups6 := regex.MatchGroups(UserHostPortRegex, test6)

	assert.Nil(t, groups0)

	assert.Equal(t, 1, len(groups1))
	value, ok := groups1["host"]
	assert.True(t, ok)
	assert.Equal(t, "foobar", value)
	value, ok = groups1["user"]
	assert.False(t, ok)
	value, ok = groups1["port"]
	assert.False(t, ok)

	assert.Equal(t, 2, len(groups2))
	value, ok = groups2["host"]
	assert.True(t, ok)
	assert.Equal(t, "bar", value)
	value, ok = groups2["user"]
	assert.True(t, ok)
	assert.Equal(t, "foo", value)
	value, ok = groups2["port"]
	assert.False(t, ok)

	assert.Equal(t, 2, len(groups3))
	value, ok = groups3["host"]
	assert.True(t, ok)
	assert.Equal(t, "foobar", value)
	value, ok = groups3["user"]
	assert.False(t, ok)
	value, ok = groups3["port"]
	assert.True(t, ok)
	assert.Equal(t, "6666", value)

	assert.Equal(t, 3, len(groups4))
	value, ok = groups4["host"]
	assert.True(t, ok)
	assert.Equal(t, "bar", value)
	value, ok = groups4["user"]
	assert.True(t, ok)
	assert.Equal(t, "foo", value)
	value, ok = groups4["port"]
	assert.True(t, ok)
	assert.Equal(t, "6666", value)

	assert.Equal(t, 3, len(groups5))
	value, ok = groups5["host"]
	assert.True(t, ok)
	assert.Equal(t, "bar", value)
	value, ok = groups5["user"]
	assert.True(t, ok)
	assert.Equal(t, "foo", value)
	value, ok = groups5["port"]
	assert.True(t, ok)
	assert.Equal(t, "6666", value)

	assert.Nil(t, groups6)
}
