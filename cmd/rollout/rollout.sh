#!/usr/bin/env sh

# set -o xtrace

usage () {
  cat << EOT

rollout.sh

Usage:
  rollout.sh root version dotfilesRemoteRepo [collection] [flags]

Arguments:
  root                  run as root
  version               version of dotfiles CLI to install
  dotfilesRemoteRepo    remote URL of dotfiles repo
  collection            collection of dotfiles to link (optional)

Flags:
  -f, --force           force overwrite of existing files (while linking)
  -b, --backup          backup existing files (while linking)
  -k, --hooks           execute hooks (while linking)

EOT
}

if [ "$#" -lt 3 ]; then
  usage
  exit 2
fi

RUN_AS_ROOT=$1
VERSION=$2
DOTFILES_REMOTE_REPO=$3

# Check if the fourth argument is a flag or a COLLECTION
if [ -n "$4" ]; then
  case "$4" in
    -*)
      unset COLLECTION
      shift 3  # Shift arguments to process flags
      ;;
    *)
      COLLECTION=$4
      shift 4  # Shift arguments to process flags and COLLECTION
      ;;
  esac
else
  unset COLLECTION
  shift 3  # No fourth argument, shift to process any remaining arguments
fi

# Concatenate all flags into one variable
LINKER_FLAGS=""
while [ -n "$1" ]; do
  LINKER_FLAGS="${LINKER_FLAGS} $1"
  shift
done

# Trim leading space from LINKER_FLAGS
LINKER_FLAGS=$(echo $LINKER_FLAGS | xargs)

download_dotfiles () {
  VERSION=${1}
  if [ "${RUN_AS_ROOT}" = "true" ]; then
    sudo sh ${HOME}/.local/bin/install.sh --version "v${VERSION}"
  else
    sh ${HOME}/.local/bin/install.sh --version "v${VERSION}"
  fi
  if [ $? -ne 0 ]; then
    echo "[rollout.sh] installation of dotfiles CLI failed"
    exit 3
  fi
}

dotfiles_version () {
  check_version=$1
  installed_version=$(dotfiles --version | cut -d' ' -f3)
  installed_version=${installed_version#v}

  if [ "${check_version}" = "${installed_version}" ]; then
    return 0
  fi
  return 1
}

mkdir -p ${HOME}/.local/bin/
export PATH=${HOME}/.local/bin/:${PATH}

command -v dotfiles >/dev/null
DOTFILES_INSTALLED=$?

if [ ${DOTFILES_INSTALLED} -eq 0 ]; then
  dotfiles_version ${VERSION} || download_dotfiles ${VERSION}
else
  download_dotfiles ${VERSION}
fi

dotfiles install ${DOTFILES_REMOTE_REPO}
if [ $? -ne 0 ]; then
  echo "[rollout.sh] dotfiles install failed"
  exit 4
fi

dotfiles link ${COLLECTION}  ${LINKER_FLAGS}
if [ $? -ne 0 ]; then
  echo "[rollout.sh] dotfiles link failed"
  exit 5
fi
