package rollout

import (
	_ "embed"
	"errors"
	"fmt"
	"os"
	"strings"

	log "github.com/sirupsen/logrus"
	"gitlab.com/pidrakin/dotfiles-cli/cli"
	"gitlab.com/pidrakin/dotfiles-cli/cmd/common"
	"gitlab.com/pidrakin/dotfiles-cli/cmd/eject"
	"gitlab.com/pidrakin/dotfiles-cli/common/logging"
	regex2 "gitlab.com/pidrakin/dotfiles-cli/common/regex"
	"gitlab.com/pidrakin/dotfiles-cli/config"
	"gitlab.com/pidrakin/dotfiles-cli/git"
	"gitlab.com/pidrakin/dotfiles-cli/ssh_ultimate"
	"gitlab.com/pidrakin/go/interactive"
	"gitlab.com/pidrakin/go/regex"
	"gitlab.com/pidrakin/go/slices"
	"golang.org/x/crypto/ssh"
)

//go:embed rollout.sh
var rolloutScript []byte

func Run(rootInstall *bool, track *bool, once *bool, remote string, uri string, collection string, linkerFlags []string, remove *string, options []func(*ssh_ultimate.Parameterizable) error) error {
	if remove != nil {
		return removeRolloutConfig(*remove)
	}
	return run(os.Exit, rootInstall, track, once, remote, uri, collection, linkerFlags, options...)
}

func removeRolloutConfig(remove string) error {
	if err := common.RemoveRollout(remove); err != nil {
		return err
	}
	return logging.Successfully("removed rollout config")
}

func run(exitFunc func(int), rootInstallFlag *bool, track *bool, once *bool, remote string, uri string, collection string, linkerFlags []string, options ...func(*ssh_ultimate.Parameterizable) error) error {
	cfgMgr, err := config.NewManager()
	if err != nil {
		return err
	}

	var rootInstall bool
	var host string
	if err = handleParams(remote, rootInstallFlag, &rootInstall, &host, &uri, &collection, &options); err != nil {
		return err
	}

	if uri == "" && cfgMgr.StateConfig == nil {
		return fmt.Errorf("[dotfiles] no state config found")
	}

	remoteConnection, err := common.NewRemote(host, options...)
	if err != nil {
		return err
	}
	var actualUri string
	var actualCollection string
	if err = remoteConnection.Perform(func(remoteConnection *common.Remote) error {
		prompter, err := interactive.NewPrompter(
			interactive.SetReader(remoteConnection.Reader),
			interactive.SetWriter(remoteConnection.Writer),
		)
		if err != nil {
			return err
		}

		actualUri, actualCollection, err = runRolloutScript(exitFunc, rootInstall, uri, collection, linkerFlags, prompter, cfgMgr, remoteConnection.Connection, remoteConnection.LocalBinPath)
		return err
	}); err != nil {
		return err
	}

	if once == nil {
		if cfgMgr.StateConfig.Once != nil {
			once = cfgMgr.StateConfig.Once
		}
		if cfgMgr.Config.Once != nil {
			once = cfgMgr.Config.Once
		}
		if once == nil {
			onceBase := false
			once = &onceBase
		}
	}
	if track == nil {
		if cfgMgr.StateConfig.Track != nil {
			track = cfgMgr.StateConfig.Track
		}
		if cfgMgr.Config.Track != nil {
			track = cfgMgr.Config.Track
		}
		if track == nil {
			trackBase := true
			track = &trackBase
		}
	}
	if !*once {
		if err = createLocalConnectionEntry(rootInstall, remote, actualUri, actualCollection, *track, options...); err != nil {
			return err
		}
	}
	return logging.Successfully("rolled out")
}

func handleParams(remote string, rootInstallFlag *bool, rootInstall *bool, host *string, uri *string, collection *string, options *[]func(*ssh_ultimate.Parameterizable) error) error {
	if rootInstallFlag != nil {
		*rootInstall = *rootInstallFlag
	}

	rolloutParams, _ := common.LoadRollout(remote)
	if rolloutParams != nil {
		if rootInstallFlag == nil {
			*rootInstall = rolloutParams.RootInstall
		}
		*host = rolloutParams.Host
		if *uri == "" {
			*uri = rolloutParams.Uri
		}
		if *collection == "" {
			*collection = rolloutParams.Collection
		}
		if len(*options) == 0 {
			*options = rolloutParams.Options
		}
	} else {
		*host = remote
		groups := regex.MatchGroups(regex2.UserHostPortRegex, remote)
		if h, ok := groups["host"]; ok {
			*host = h
		}
		if p, ok := groups["port"]; ok {
			*options = slices.Prepend(*options, ssh_ultimate.PortOption(p))
		}
		if u, ok := groups["user"]; ok {
			*options = slices.Prepend(*options, ssh_ultimate.UserOption(u))
		}
	}

	return nil
}

func runRolloutScript(exitFunc func(int), rootInstall bool, remote string, collection string, linkerFlags []string, prompter *interactive.Prompter, cfgMgr *config.Manager, connection *ssh_ultimate.Connection, localBinPath string) (string, string, error) {
	defer logging.SetOutputExitFunc(exitFunc)()
	var err error

	if err = connection.SftpWriteUpload(eject.InstallScript(), fmt.Sprintf("%s/install.sh", localBinPath), os.FileMode(0755)); err != nil {
		return "", "", err
	}

	if err = connection.SftpWriteUpload(rolloutScript, fmt.Sprintf("%s/rollout.sh", localBinPath), os.FileMode(0755)); err != nil {
		return "", "", err
	}

	version := cli.Version
	if version == "" {
		if version, err = git.GetLatestVersion(); err != nil {
			return "", "", logging.Failed("rollout: no version set see .goreleaser.yml on how to set")
		}
	}
	version = strings.TrimPrefix(version, "v")

	if remote == "" {
		if collection == "" && cfgMgr.CollectionsConfig != nil {
			collection = config.InteractiveCollectionPrompt(prompter, cfgMgr.CollectionsConfig.Collections)
		}
		remote = cfgMgr.StateConfig.Remote
	}

	rolloutCmd := fmt.Sprintf("%s/rollout.sh %t %s %s %s %s", localBinPath, rootInstall, version, remote, collection, strings.Join(linkerFlags, " "))
	cmdErr := connection.Run(true, os.Stdout, os.Stderr, rolloutCmd)
	if cmdErr != nil {
		var exitErr *ssh.ExitError
		if errors.As(cmdErr, &exitErr) {
			errorCode := exitErr.ExitStatus()
			log.Errorf("[dotfiles rollout] rollout failed\nExited: %v", errorCode)

			switch errorCode {
			case 2:
				log.Errorf("[dotfiles rollout] no root flag, version or remote in stateconfig. See .goreleaser.yml on how to set version")
			case 3:
				log.Errorf("[dotfiles rollout] installation of dotfiles CLI failed")
			case 4:
				log.Errorf("[dotfiles rollout] dotfiles install failed")
			case 5:
				log.Errorf("[dotfiles rollout] dotfiles link failed")
			}
		} else {
			log.WithField("error", cmdErr.Error()).Errorf("[dotfiles rollout] unexpected error encountered")
		}
		return "", "", logging.Failed("rollout")
	}
	log.Debugf("[dotfiles rollout] rollout successful")
	return remote, collection, nil
}

func createLocalConnectionEntry(rootInstall bool, remote string, uri string, collection string, track bool, options ...func(*ssh_ultimate.Parameterizable) error) error {
	return common.SaveRollout(rootInstall, remote, uri, collection, track, options...)
}
