//go:build keep

package rollout

import (
	"bytes"
	"flag"
	"fmt"
	"strings"
	"testing"

	"github.com/kevinburke/ssh_config"
	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"gitlab.com/pidrakin/dotfiles-cli/cli"
	"gitlab.com/pidrakin/dotfiles-cli/cmd/common"
	common2 "gitlab.com/pidrakin/dotfiles-cli/common/logging"
	"gitlab.com/pidrakin/dotfiles-cli/config"
	"gitlab.com/pidrakin/dotfiles-cli/git"
	"gitlab.com/pidrakin/dotfiles-cli/ssh_ultimate"
	"gitlab.com/pidrakin/go/tests"
)

var rolloutHost string
var rolloutPort int

func init() {
	flag.StringVar(&rolloutHost, "rollout-host", "localhost", "rollout host")
	flag.IntVar(&rolloutPort, "rollout-port", 2222, "rollout port")
}

func rolloutSSHConfig() string {
	return fmt.Sprintf(`
Host Test
  HostName %s
  Port %d
  User test
`, rolloutHost, rolloutPort)
}

func Test_rollout(t *testing.T) {
	ssh_ultimate.Prepare(t, rolloutSSHConfig, func(t *testing.T, sshConfig *ssh_config.Config) {
		common2.SetLogLevel(log.FatalLevel)
		common.SftpUploadBinary = true
		defer func() {
			common.SftpUploadBinary = false
		}()

		stateConfig := config.StateConfig{
			Version:         config.Version(),
			Name:            "b3f2035551f6e48c67489ace9f588f94",
			Source:          "repository",
			InstallLocation: ".local/share/dotfiles",
			Remote:          "https://gitlab.com/pidrakin/dotfiles-example.git",
		}
		err := stateConfig.Save()
		tests.AssertNoError(t, err)

		_, err = git.Clone(stateConfig.Remote, stateConfig.GetContextPath())
		tests.AssertNoError(t, err)

		cli.Version = "test"

		rootInstallFlag := false
		trackFlag := true
		onceFlag := false

		reader := strings.NewReader("test\n")
		var buf bytes.Buffer
		var errBuf bytes.Buffer
		var exit *int
		err = run(
			func(i int) {
				exit = &i
			},
			&rootInstallFlag,
			&trackFlag,
			&onceFlag,
			"Test",
			"",
			"",
			[]string{},
			ssh_ultimate.ReaderOption(reader),
			ssh_ultimate.WriterOption(&buf),
			ssh_ultimate.ErrWriterOption(&errBuf),
			ssh_ultimate.ExitFuncOption(func(i int) {
				exit = &i
			}),
		)
		tests.AssertNoError(t, err)
		assert.Nil(t, errBuf.Bytes())
		assert.Nil(t, exit)
	})
}

func Test_rolloutAlternativeRemote(t *testing.T) {
	ssh_ultimate.Prepare(t, rolloutSSHConfig, func(t *testing.T, sshConfig *ssh_config.Config) {
		common2.SetLogLevel(log.FatalLevel)
		common.SftpUploadBinary = true
		defer func() {
			common.SftpUploadBinary = false
		}()

		cli.Version = "test"

		rootInstallFlag := false
		trackFlag := true
		onceFlag := false

		reader := strings.NewReader("test\n")
		var buf bytes.Buffer
		var errBuf bytes.Buffer
		var exit *int
		err := run(
			func(i int) {
				exit = &i
			},
			&rootInstallFlag,
			&trackFlag,
			&onceFlag,
			"Test",
			"https://gitlab.com/pidrakin/dotfiles-expert-example.git",
			"linux",
			[]string{},
			ssh_ultimate.ReaderOption(reader),
			ssh_ultimate.WriterOption(&buf),
			ssh_ultimate.ErrWriterOption(&errBuf),
			ssh_ultimate.ExitFuncOption(func(i int) {
				exit = &i
			}),
		)
		tests.AssertNoError(t, err)
		assert.Nil(t, errBuf.Bytes())
		assert.Nil(t, exit)
	})
}
