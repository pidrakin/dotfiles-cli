package diff

import (
	"fmt"
	"strings"

	"gitlab.com/pidrakin/dotfiles-cli/cmd/common"
	"gitlab.com/pidrakin/dotfiles-cli/text"
)

func Run(collection string) (err error) {
	differ2 := common.Differ{}
	diffs, err := differ2.Diff(collection)
	if err != nil {
		return
	}

	if len(diffs) == 0 {
		return
	}

	root := strings.TrimSuffix(diffs[0].Root, "/"+diffs[0].Collection)
	message := fmt.Sprintf(text.Diff, root)

	for _, diff2 := range diffs {
		if len(diff2.ToAdd) == 0 && len(diff2.ToDelete) == 0 && len(diff2.ToIgnore) == 0 {
			continue
		}
		message += fmt.Sprintf(text.Package, diff2.Collection)
		for _, file := range diff2.ToAdd {
			message += fmt.Sprintf(text.Add, file)
		}
		for _, file := range diff2.ToDelete {
			message += fmt.Sprintf(text.Delete, file)
		}
		for _, file := range diff2.ToIgnore {
			message += fmt.Sprintf(text.Linked, file)
		}
		for _, file := range diff2.ToModify {
			message += fmt.Sprintf(text.Modify, file)
		}
	}
	fmt.Println(message)
	return
}
