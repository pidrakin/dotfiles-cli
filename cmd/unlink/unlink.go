package unlink

import (
	"fmt"
	"github.com/fatih/color"
	common2 "gitlab.com/pidrakin/dotfiles-cli/cmd/common"
	"gitlab.com/pidrakin/dotfiles-cli/common/logging"
	"gitlab.com/pidrakin/dotfiles-cli/config"
	"gitlab.com/pidrakin/go/slices"
	"os"
)

type unlinker struct {
	stateConfig  *config.StateConfig
	deletedFiles []string
	deletedDirs  []string
}

func Run() error {
	u := unlinker{}
	return u.unlink(os.Exit)
}

func (unlinker *unlinker) unlink(exitFunc func(int)) (err error) {
	logging.SetOutputExitFunc(exitFunc)
	cfgMgr, err := config.NewManager()
	if err != nil {
		return
	}
	stateConfig := cfgMgr.StateConfig
	unlinker.stateConfig = stateConfig

	if stateConfig == nil {
		if err = logging.Failed("nothing linked"); err != nil {
			return
		}
		return nil
	}

	if unlinker.deletedFiles, unlinker.deletedDirs, err = common2.Unlink(stateConfig, exitFunc); err != nil {
		return
	}
	if err = cfgMgr.StateConfig.Save(); err != nil {
		return err
	}

	if err = logging.Successfully("unlinked"); err != nil {
		return
	}
	if err = unlinker.outputStats(); err != nil {
		return
	}
	return
}

func (unlinker *unlinker) outputStats() error {
	if unlinker.stateConfig.Collection != "" {
		if _, err := fmt.Fprintf(os.Stdout, "\ncollection [%s]\n", unlinker.stateConfig.Collection); err != nil {
			return err
		}
	}

	sums := []int{
		len(unlinker.deletedFiles),
		len(unlinker.deletedDirs),
	}

	if slices.Sum(sums) > 0 {
		if _, err := fmt.Fprintf(os.Stdout, "\n"); err != nil {
			return err
		}
	}

	if err := outputStat(sums[0], "deleted file links", color.FgRed, color.Bold); err != nil {
		return err
	}
	if err := outputStat(sums[1], "deleted dirs", color.FgRed, color.Bold); err != nil {
		return err
	}

	return nil
}

func outputStat(stat int, mod string, attrs ...color.Attribute) error {
	if stat == 0 {
		return nil
	}
	colored := color.New(attrs...).Sprintf("%d", stat)
	_, err := fmt.Fprintf(os.Stdout, "%s %s\n", colored, mod)
	return err
}
