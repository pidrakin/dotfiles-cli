package unlink

import (
	"bytes"
	"os"
	"path"
	"path/filepath"
	"testing"
	"time"

	"github.com/go-git/go-git/v5"
	"github.com/go-git/go-git/v5/plumbing/object"
	"github.com/mitchellh/go-homedir"
	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"gitlab.com/pidrakin/dotfiles-cli/cmd/initialize"
	"gitlab.com/pidrakin/dotfiles-cli/cmd/install"
	"gitlab.com/pidrakin/dotfiles-cli/cmd/link"
	"gitlab.com/pidrakin/go/tests"
)

var (
	repoPath   = "repository"
	clonedRepo = "clone"
	homeDir    = "home/user"
)

func init() {
	homedir.DisableCache = true
}

func setupRepo(t *testing.T, wd string, path string, cloned string, expert bool) {
	log.SetLevel(log.FatalLevel)

	err := os.MkdirAll(filepath.Join(wd, homeDir), os.FileMode(0755))
	tests.AssertNoError(t, err)
	err = os.Setenv("HOME", filepath.Join(wd, homeDir))
	tests.AssertNoError(t, err)

	repo := filepath.Join(wd, path)
	err = os.MkdirAll(repo, os.FileMode(0755))
	tests.AssertNoError(t, err)

	err = os.Chdir(filepath.Join(wd, path))
	tests.AssertNoError(t, err)

	err = initialize.Run(expert, true)
	tests.AssertNoError(t, err)

	_, err = git.PlainInit(repo, false)
	tests.AssertNoError(t, err)

	r, err := git.PlainOpen(repo)
	tests.AssertNoError(t, err)

	w, err := r.Worktree()
	tests.AssertNoError(t, err)

	_, err = w.Add(".")
	tests.AssertNoError(t, err)

	status, err := w.Status()
	tests.AssertNoError(t, err)
	assert.NotEmpty(t, status)

	commit, err := w.Commit("example go-git commit", &git.CommitOptions{
		Author: &object.Signature{
			Name:  "John Doe",
			Email: "john@doe.org",
			When:  time.Now(),
		},
	})
	tests.AssertNoError(t, err)

	obj, err := r.CommitObject(commit)
	tests.AssertNoError(t, err)
	assert.NotEmpty(t, obj)

	err = os.Chdir(wd)
	tests.AssertNoError(t, err)

	var buf bytes.Buffer
	_, err = git.PlainClone(cloned, false, &git.CloneOptions{
		URL:      path,
		Progress: &buf,
	})
	tests.AssertNoError(t, err)
	assert.NotEmpty(t, buf.String())
}

func installRepo(t *testing.T, wd string, path string) {
	err := install.Run(false, false, filepath.Join(wd, path))
	tests.AssertNoError(t, err)
}

func linkRepo(t *testing.T, collection string) {
	runHooks := true
	err := link.Run(false, false, false, &runHooks, collection)
	tests.AssertNoError(t, err)
}

func unlinkRepo(t *testing.T) {
	u := unlinker{}
	var exitCode int
	err := u.unlink(func(i int) {
		exitCode = i
	})
	tests.AssertNoError(t, err)
	assert.Equal(t, 0, exitCode)
}

func TestLinker_UnlinkSimple(t *testing.T) {
	tests.RunInTmp(t, func(t *testing.T, wd string) {
		setupRepo(t, wd, repoPath, clonedRepo, false)
		installRepo(t, wd, clonedRepo)
		linkRepo(t, "")
		unlinkRepo(t)

		files := []string{
			".bashrc",
			".profile",
		}

		for _, entry := range files {
			target := path.Join(wd, homeDir, entry)
			tests.AssertFileAbsent(t, target)
		}
	})
}

func TestLinker_UnlinkSimpleFailed(t *testing.T) {
	tests.RunInTmp(t, func(t *testing.T, wd string) {
		setupRepo(t, wd, repoPath, clonedRepo, false)
		unlinker := unlinker{}
		var exitCode int
		err := unlinker.unlink(func(i int) {
			exitCode = i
		})
		tests.AssertNoError(t, err)
		assert.Equal(t, 1, exitCode)
	})
}
