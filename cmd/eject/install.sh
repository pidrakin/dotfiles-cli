#!/usr/bin/env sh

# script exits when command fails; use || true to allow failure
set -o errexit
# script exits when trying to access undefined variables
set -o nounset
# enable for debugging
# set -o xtrace

LOG_LEVEL=2

SOURCE="${0##*/}"
VERSION="latest"
NO_PACKAGE=1

OS=""
ARCH=""

usage() {
  cat << EOT

install.sh

Installer for dotfiles CLI

Usage:
  ${SOURCE} [-h|--help]
  ${SOURCE} [-s|--silent] [-n|--no-package] [-v|--version VERSION]

Options:
  -s, --silent           Do not print any messages
  -n, --no-package       Do not install via package manager
  -v, --version          Install specific version
EOT
  exit 1
}

parse_args() {
  while test $# -gt 0; do
    opt="$1"
    opts=""

    case "$opt" in
      -[^-][^-]* )
        opt="${opt#-}"
        i=0
        while [ $i -lt ${#opt} ]; do
          char=$(expr substr "$opt" $((i + 1)) 1)
          opts="$opts -${char}"
          i=$((i+1))
        done;;
      *)
        opts="$opts $opt";;
    esac

    for o in "$opts"; do
      case "$(echo "$o" | tr -d ' ')" in
        -s|--silent )
          LOG_LEVEL=0;;
        -n|--no-package )
          NO_PACKAGE=0;;
        -v|--version )
          shift
          VERSION="$1";;
        -h|--help )
          usage
          exit 0;;
        *)
          usage
          exit 1;;
      esac
    done
    shift
  done
}

validate_root() {
  [ $(id -u) -eq 0 ]
}

download() {
  if ! is_command wget && ! is_command curl; then
    log_err "please install either wget or curl"
    exit 1
  fi
  if is_command wget; then
    wget -q -O - $1
  else
    curl -fsSL $1
  fi
}

determine_version() {
  if [ "${VERSION}" = "latest" ]; then
    download_result=$(download https://gitlab.com/api/v4/projects/24946569/releases/ | grep -o -E '"name":"v[^"]+')
    VERSION=$(echo "${download_result}" | head -1 | cut -d'"' -f4)
  fi
}

install_from_binary() {
  local=${1:-false}
  if [ "${local}" = "false" ]; then
    install_path=/usr/local/bin/
  else
    mkdir -p ${HOME}/.local/bin
    install_path=${HOME}/.local/bin
  fi
  determine_version
  log_info "installing release version ${VERSION}"
  download https://gitlab.com/pidrakin/dotfiles-cli/-/releases/${VERSION}/downloads/dotfiles-${VERSION#v}-${OS}-${ARCH}.tar.gz | tar -xz -C "${install_path}"
  log_info "installed dotfiles"
}

linux() {
  if ! validate_root; then
    install_from_binary true
    return
  fi

  if [ ${NO_PACKAGE} -ne 0 ] && is_command apt; then
    log_info "found apt: installing via package manager apt"
    apt-get update >/dev/null
    apt-get install -y ca-certificates gpg >/dev/null
    download https://fury.pidrak.in/apt/gpg.key | gpg --dearmor > /usr/share/keyrings/pidrakin-keyring.gpg
    echo "deb [signed-by=/usr/share/keyrings/pidrakin-keyring.gpg] https://fury.pidrak.in/apt/ /" > /etc/apt/sources.list.d/pidrakin.list
    apt-get update >/dev/null
    if [ "${VERSION}" = "latest" ]; then
      apt-get install -y dotfiles >/dev/null
    else
      log_info "installing specific version ${VERSION}"
      apt-get install -y dotfiles=${VERSION#v} >/dev/null
    fi
    log_info "installed dotfiles"
  elif [ ${NO_PACKAGE} -ne 0 ] &&  is_command dnf; then
    log_info "found dnf: installing via package manager dnf"
    download https://fury.pidrak.in/rpm/gpg.key > /etc/pki/rpm-gpg/RPM-GPG-KEY-pidrakin
    cat > /etc/yum.repos.d/pidrakin.repo <<EOL
[pidrakin]
name=Pidrakin Repo
baseurl=https://fury.pidrak.in/yum/
enabled=1
gpgcheck=0
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-pidrakin
EOL
    dnf makecache >/dev/null
    if [ "${VERSION}" = "latest" ]; then
      dnf install -y dotfiles >/dev/null
    else
      log_info "installing specific version ${VERSION}"
      dnf install -y dotfiles-${VERSION#v}-1 >/dev/null
    fi
    log_info "installed dotfiles"
  elif [ ${NO_PACKAGE} -ne 0 ] && is_command apk; then
    log_info "found apk: installing via package manager apk"
    echo "https://alpine.fury.io/satanik/" >> /etc/apk/repositories
    download https://alpine.fury.io/satanik/satanik@fury.io-e8dc17e0.rsa.pub > /etc/apk/keys/satanik@fury.io-e8dc17e0.rsa.pub
    apk update >/dev/null
    if [ "${VERSION}" = "latest" ]; then
      apk add --no-cache dotfiles >/dev/null
    else
      log_info "installing specific version ${VERSION}"
      apk add --no-cache dotfiles=${VERSION#v} >/dev/null
    fi
    log_info "installed dotfiles"
  else
    install_from_binary
  fi
}

mac() {
  if [ ${NO_PACKAGE} -ne 0 ] && is_command brew; then
    log_info "found brew: installing via package manager brew"
    brew tap pidrakin/dotfiles >/dev/null
    brew install dotfiles >/dev/null
    log_info "installed dotfiles"
  else
    if ! validate_root; then
      install_from_binary true
      return
    fi
    install_from_binary
  fi
}

run_os_specific() {
  machine=$(uname -m)
  case "${machine}" in
    x86_64|amd64)
      ARCH=amd64;;
    i?86)
      ARCH=i386;;
    aarch64)
      ARCH=arm64;;
    *)
      log_err "platform ${ARCH} not supported; only amd64, i386 and amd64"
      exit 1;;
  esac
  if [ "$(getconf LONG_BIT)" = 32 ]; then
    if [ "${ARCH}" == arm64 ]; then
      log_err "platform arm32 not supported; only amd64, i386 and amd64"
      exit 1
    fi
    ARCH=i386
  fi
  OS=$(uname -s | tr '[:upper:]' '[:lower:]')
  case "${OS}" in
    linux*)
      log_info "OS is linux"
      linux
      ;;
    darwin*)
      log_info "OS is darwin"
      mac
      ;;
    *)
      log_err "OS not supported: ${OS}"
      exit 1;;
    esac
}


main() {
  parse_args "${@}"
  run_os_specific
}

is_command() {
  type "${1}" >/dev/null 2>&1
}

log_info() {
  [ 2 -le "${LOG_LEVEL}" ] || return 0
  if [ -t 0 ]; then
    color="\033[1;34m"
    reset="\033[m"
  fi
  printf ${color:-}'[DEBUG]'${reset:-}' %s\n' "${*}" 1>&2
}

log_err() {
  [ 1 -le "${LOG_LEVEL}" ] || return 0
  if [ -t 0 ]; then
    color="\033[1;31m"
    reset="\033[m"
  fi
  printf ${color:-}'[ERROR]'${reset:-}' %s\n' "${*}" 1>&2
}

main "${@}"
