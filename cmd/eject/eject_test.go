package eject

import (
	"bytes"
	"github.com/stretchr/testify/assert"
	"gitlab.com/pidrakin/go/tests"
	"path/filepath"
	"testing"
)

func Test_listEjectable(t *testing.T) {
	var buf bytes.Buffer
	err := listEjectable(&buf)
	tests.AssertNoError(t, err)
	assert.Equal(t, "files available for ejection:\n\ninstall.sh\n", buf.String())
}

func Test_eject(t *testing.T) {
	t.Run("eject config: failed", func(t *testing.T) {
		var buf bytes.Buffer
		err := run(&buf, "config", false)
		tests.AssertEqualError(t, err, "file [config] does not exist")
		assert.Empty(t, buf.String())
	})
	t.Run("eject install.sh: success", func(t *testing.T) {
		tests.RunInTmp(t, func(t *testing.T, wd string) {
			var buf bytes.Buffer
			err := run(&buf, "install.sh", false)
			tests.AssertNoError(t, err)

			path := filepath.Join(wd, "install.sh")
			tests.AssertFileExists(t, path)
		})
	})
	t.Run("eject install.sh to stdout: success", func(t *testing.T) {
		var buf bytes.Buffer
		err := run(&buf, "install.sh", true)
		tests.AssertNoError(t, err)

		assert.NotEmpty(t, buf.String())
	})
}
