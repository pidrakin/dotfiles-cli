package eject

import (
	_ "embed"
	"fmt"
	"gitlab.com/pidrakin/dotfiles-cli/common/logging"
	"gitlab.com/pidrakin/go/maps"
	"io"
	"os"
	"path/filepath"
	"strings"
)

//go:embed install.sh
var installScript []byte

var files = map[string][]byte{
	"install.sh": installScript,
}

func InstallScript() []byte {
	return installScript
}

func NewListEjectable() error {
	return listEjectable(os.Stdout)
}

func listEjectable(writer io.Writer) error {
	fileNames := maps.Keys(files)
	list := strings.Join(fileNames, "\n")
	_, err := fmt.Fprintf(writer, "files available for ejection:\n\n%s\n", list)
	return err
}

func Run(file string, stdout bool) error {
	return run(os.Stdout, file, stdout)
}

func run(writer io.Writer, file string, stdout bool) error {
	if _, ok := files[file]; !ok {
		return fmt.Errorf("file [%s] does not exist", file)
	}

	if stdout {
		_, err := fmt.Fprint(writer, string(files[file]))
		return err
	}

	wd, err := os.Getwd()
	if err != nil {
		return err
	}
	filePath := filepath.Join(wd, file)
	if writeErr := os.WriteFile(filePath, files[file], os.FileMode(0755)); err != nil {
		return writeErr
	}
	return logging.Successfully("successfully ejected [install.sh]")
}
