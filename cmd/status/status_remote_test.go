//go:build keep

package status

import (
	"bytes"
	"flag"
	"fmt"
	"os"
	"path/filepath"
	"strings"
	"testing"

	"github.com/fatih/color"
	"github.com/kevinburke/ssh_config"
	"github.com/mitchellh/go-homedir"
	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"gitlab.com/pidrakin/dotfiles-cli/cli"
	"gitlab.com/pidrakin/dotfiles-cli/cmd/link"
	"gitlab.com/pidrakin/dotfiles-cli/common/logging"
	"gitlab.com/pidrakin/dotfiles-cli/config"
	"gitlab.com/pidrakin/dotfiles-cli/git"
	"gitlab.com/pidrakin/dotfiles-cli/ssh_ultimate"
	output2 "gitlab.com/pidrakin/go/output"
	"gitlab.com/pidrakin/go/tests"
)

var statusHost string
var statusPort int

func init() {
	flag.StringVar(&statusHost, "status-host", "localhost", "status host")
	flag.IntVar(&statusPort, "status-port", 2222, "status port")
}

func statusSSHConfig() string {
	return fmt.Sprintf(`
Host Test
  HostName %s
  Port %d
  User test
`, statusHost, statusPort)
}

func Test_statusRemote(t *testing.T) {
	ssh_ultimate.Prepare(t, statusSSHConfig, func(t *testing.T, sshConfig *ssh_config.Config) {
		ssh_ultimate.PrepareAgent(t, func(t *testing.T) {
			logging.SetLogLevel(log.FatalLevel)
			version := cli.Version
			cli.Version = "v99.1.3"
			noColor := color.NoColor
			color.NoColor = true
			defer func() {
				cli.Version = version
				color.NoColor = noColor
			}()

			home, err := homedir.Dir()
			tests.AssertNoError(t, err)
			installDir := filepath.Join(home, config.GetLocalInstallDir(), "rollouts")
			err = os.MkdirAll(installDir, os.FileMode(0755))
			tests.AssertNoError(t, err)
			err = os.WriteFile(filepath.Join(installDir, "Test"), []byte("host: Test\ntrack: true"), os.FileMode(0644))
			tests.AssertNoError(t, err)

			t.Run("table test", func(t *testing.T) {
				reader := strings.NewReader("")
				var buf bytes.Buffer
				var errBuf bytes.Buffer
				var exit *int
				err = run(
					reader,
					&buf,
					&errBuf,
					func(i int) {
						exit = &i
					},
					false,
					true,
					output2.WideTableFormat,
					"",
				)
				tests.AssertNoError(t, err)
				expected := "HOST  CLI   INSTALLED  LINKED  COLLECTION  LINK_STATUS  REPO                                                     GIT_STATUS  \n" +
					"Test  test  true       true    linux       up-to-date   https://gitlab.com/pidrakin/dotfiles-expert-example.git  up-to-date  \n"
				assert.Equal(t, expected, buf.String())
				assert.Nil(t, exit)
			})
		})
	})
}

func Test_statusCombined(t *testing.T) {
	ssh_ultimate.Prepare(t, statusSSHConfig, func(t *testing.T, sshConfig *ssh_config.Config) {
		ssh_ultimate.PrepareAgent(t, func(t *testing.T) {
			defer logging.SetLogLevel(log.FatalLevel)()
			runHooks := true
			version := cli.Version
			cli.Version = "v99.1.3"
			noColor := color.NoColor
			color.NoColor = true
			defer func() {
				cli.Version = version
				color.NoColor = noColor
			}()

			stateConfig := config.StateConfig{
				Version:         config.Version(),
				Name:            "b3f2035551f6e48c67489ace9f588f94",
				Source:          "repository",
				InstallLocation: ".local/share/dotfiles",
				Remote:          "https://gitlab.com/pidrakin/dotfiles-example.git",
			}
			err := stateConfig.Save()
			tests.AssertNoError(t, err)

			_, err = git.Clone(stateConfig.Remote, stateConfig.GetContextPath())
			tests.AssertNoError(t, err)

			err = link.Run(false, false, true, &runHooks, "")
			tests.AssertNoError(t, err)

			home, err := homedir.Dir()
			tests.AssertNoError(t, err)
			installDir := filepath.Join(home, config.GetLocalInstallDir(), "rollouts")
			err = os.MkdirAll(installDir, os.FileMode(0755))
			tests.AssertNoError(t, err)
			err = os.WriteFile(filepath.Join(installDir, "Test"), []byte("host: Test\ntrack: true"), os.FileMode(0644))
			tests.AssertNoError(t, err)

			t.Run("table test", func(t *testing.T) {
				reader := strings.NewReader("")
				var buf bytes.Buffer
				var errBuf bytes.Buffer
				var exit *int
				err = run(
					reader,
					&buf,
					&errBuf,
					func(i int) {
						exit = &i
					},
					true,
					true,
					output2.WideTableFormat,
					"",
				)
				expected := "HOST   CLI      INSTALLED  LINKED  COLLECTION  LINK_STATUS  REPO                                                     GIT_STATUS  \n" +
					"local  v99.1.3  true       true                up-to-date   https://gitlab.com/pidrakin/dotfiles-example.git         up-to-date  \n" +
					"Test   test     true       true    linux       up-to-date   https://gitlab.com/pidrakin/dotfiles-expert-example.git  up-to-date  \n"
				tests.AssertNoError(t, err)
				assert.Equal(t, expected, buf.String())
				assert.Nil(t, exit)
			})
		})
	})
}

func Test_statusCombinedRemoteNotReachable(t *testing.T) {
	ssh_ultimate.Prepare(t, statusSSHConfig, func(t *testing.T, sshConfig *ssh_config.Config) {
		ssh_ultimate.PrepareAgent(t, func(t *testing.T) {
			defer logging.SetLogLevel(log.FatalLevel)()
			runHooks := true
			version := cli.Version
			cli.Version = "v99.1.3"
			noColor := color.NoColor
			color.NoColor = true
			defer func() {
				cli.Version = version
				color.NoColor = noColor
			}()

			stateConfig := config.StateConfig{
				Version:         config.Version(),
				Name:            "b3f2035551f6e48c67489ace9f588f94",
				Source:          "repository",
				InstallLocation: ".local/share/dotfiles",
				Remote:          "https://gitlab.com/pidrakin/dotfiles-example.git",
			}
			err := stateConfig.Save()
			tests.AssertNoError(t, err)

			_, err = git.Clone(stateConfig.Remote, stateConfig.GetContextPath())
			tests.AssertNoError(t, err)

			err = link.Run(false, false, true, &runHooks, "")
			tests.AssertNoError(t, err)

			home, err := homedir.Dir()
			tests.AssertNoError(t, err)
			installDir := filepath.Join(home, config.GetLocalInstallDir(), "rollouts")
			err = os.MkdirAll(installDir, os.FileMode(0755))
			tests.AssertNoError(t, err)
			err = os.WriteFile(filepath.Join(installDir, "foo@bar:2222"), []byte("host: bar\nuser: foo\nport: 2222\ntrack: true"), os.FileMode(0644))
			tests.AssertNoError(t, err)

			t.Run("table test", func(t *testing.T) {
				reader := strings.NewReader("")
				var buf bytes.Buffer
				var errBuf bytes.Buffer
				var exit *int
				err = run(
					reader,
					&buf,
					&errBuf,
					func(i int) {
						exit = &i
					},
					true,
					true,
					output2.WideTableFormat,
					"",
				)
				expected := "HOST                        CLI      INSTALLED  LINKED  LINK_STATUS  REPO                                              GIT_STATUS  \nlocal                       v99.1.3  true       true    up-to-date   https://gitlab.com/pidrakin/dotfiles-example.git  up-to-date  \nfoo@bar:2222 (unreachable)           false      false                                                                              \n"
				tests.AssertNoError(t, err)
				assert.Equal(t, expected, buf.String())
				assert.Nil(t, exit)
			})
		})
	})
}
