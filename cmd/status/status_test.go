package status

import (
	"bytes"
	"fmt"
	"strings"
	"testing"

	"github.com/fatih/color"
	"github.com/kevinburke/ssh_config"
	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"gitlab.com/pidrakin/dotfiles-cli/cli"
	"gitlab.com/pidrakin/dotfiles-cli/cmd/link"
	"gitlab.com/pidrakin/dotfiles-cli/common/logging"
	"gitlab.com/pidrakin/dotfiles-cli/config"
	"gitlab.com/pidrakin/dotfiles-cli/git"
	"gitlab.com/pidrakin/dotfiles-cli/ssh_ultimate"
	output2 "gitlab.com/pidrakin/go/output"
	"gitlab.com/pidrakin/go/tests"
)

type FormatCombination struct {
	Format   output2.FormatType
	Template string
}

var formats = map[string]*FormatCombination{
	"table":          {Format: output2.TableFormat},
	"wide table":     {Format: output2.WideTableFormat},
	"custom columns": {Format: output2.WideTableFormat, Template: "installed:linked"},
	"yaml":           {Format: output2.YamlFormat},
	"json":           {Format: output2.JsonFormat},
	"json compact":   {Format: output2.JsonFormat, Template: "compact"},
	"go template":    {Format: output2.GoTemplateFormat, Template: "{{ . }}"},
}

func Test_statusLocalNotInstalled(t *testing.T) {
	ssh_ultimate.Prepare(t, nil, func(t *testing.T, sshConfig *ssh_config.Config) {
		logging.SetLogLevel(log.FatalLevel)
		version := cli.Version
		cli.Version = "v99.1.3"
		noColor := color.NoColor
		color.NoColor = true
		defer func() {
			cli.Version = version
			color.NoColor = noColor
		}()

		expected := map[string]string{
			"table":          "HOST   INSTALLED  LINKED  \nlocal  false      false   \n",
			"wide table":     "HOST   CLI      INSTALLED  LINKED  \nlocal  v99.1.3  false      false   \n",
			"custom columns": "INSTALLED  LINKED  \nfalse      false   \n",
			"yaml":           "- host: local\n  cli: v99.1.3\n  installed: false\n  linked: false\n",
			"json":           "[\n  {\n    \"host\": \"local\",\n    \"cli\": \"v99.1.3\",\n    \"installed\": false,\n    \"linked\": false,\n    \"repo\": \"\"\n  }\n]",
			"json compact":   "[{\"host\":\"local\",\"cli\":\"v99.1.3\",\"installed\":false,\"linked\":false,\"repo\":\"\"}]",
			"go template":    "[map[Cli:v99.1.3 Collection: GitStatus: Host:local Installed:false LinkStatus: Linked:false Repo:]]",
		}

		for name, c := range formats {
			t.Run(fmt.Sprintf("%s test", name), func(t *testing.T) {
				reader := strings.NewReader("")
				var buf bytes.Buffer
				var errBuf bytes.Buffer
				var exit *int
				err := run(
					reader,
					&buf,
					&errBuf,
					func(i int) {
						exit = &i
					},
					true,
					false,
					c.Format,
					c.Template,
				)
				tests.AssertNoError(t, err)
				assert.Equal(t, expected[name], buf.String())
				assert.Nil(t, errBuf.Bytes())
				assert.Nil(t, exit)
			})
		}
	})
}

func Test_statusLocalInstalledNotLinked(t *testing.T) {
	ssh_ultimate.Prepare(t, nil, func(t *testing.T, sshConfig *ssh_config.Config) {
		logging.SetLogLevel(log.FatalLevel)
		version := cli.Version
		cli.Version = "v99.1.3"
		noColor := color.NoColor
		color.NoColor = true
		defer func() {
			cli.Version = version
			color.NoColor = noColor
		}()

		stateConfig := config.StateConfig{
			Version:         config.Version(),
			Name:            "b3f2035551f6e48c67489ace9f588f94",
			Source:          "repository",
			InstallLocation: ".local/share/dotfiles",
			Remote:          "https://gitlab.com/pidrakin/dotfiles-example.git",
		}
		err := stateConfig.Save()
		tests.AssertNoError(t, err)

		_, err = git.Clone(stateConfig.Remote, stateConfig.GetContextPath())
		tests.AssertNoError(t, err)

		expected := map[string]string{
			"table":          "HOST   INSTALLED  LINKED  GIT_STATUS  \nlocal  true       false   up-to-date  \n",
			"wide table":     "HOST   CLI      INSTALLED  LINKED  REPO                                              GIT_STATUS  \nlocal  v99.1.3  true       false   https://gitlab.com/pidrakin/dotfiles-example.git  up-to-date  \n",
			"custom columns": "INSTALLED  LINKED  \ntrue       false   \n",
			"yaml":           "- host: local\n  cli: v99.1.3\n  installed: true\n  linked: false\n  repo: https://gitlab.com/pidrakin/dotfiles-example.git\n  gitStatus: up-to-date\n",
			"json":           "[\n  {\n    \"host\": \"local\",\n    \"cli\": \"v99.1.3\",\n    \"installed\": true,\n    \"linked\": false,\n    \"repo\": \"https://gitlab.com/pidrakin/dotfiles-example.git\",\n    \"gitStatus\": \"up-to-date\"\n  }\n]",
			"json compact":   "[{\"host\":\"local\",\"cli\":\"v99.1.3\",\"installed\":true,\"linked\":false,\"repo\":\"https://gitlab.com/pidrakin/dotfiles-example.git\",\"gitStatus\":\"up-to-date\"}]",
			"go template":    "[map[Cli:v99.1.3 Collection: GitStatus:up-to-date Host:local Installed:true LinkStatus: Linked:false Repo:https://gitlab.com/pidrakin/dotfiles-example.git]]",
		}

		for name, c := range formats {
			t.Run(fmt.Sprintf("%s test", name), func(t *testing.T) {
				reader := strings.NewReader("")
				var buf bytes.Buffer
				var errBuf bytes.Buffer
				var exit *int
				err = run(
					reader,
					&buf,
					&errBuf,
					func(i int) {
						exit = &i
					},
					true,
					false,
					c.Format,
					c.Template,
				)
				tests.AssertNoError(t, err)
				assert.Equal(t, expected[name], buf.String())
				assert.Nil(t, errBuf.Bytes())
				assert.Nil(t, exit)
			})
		}
	})
}

func Test_statusLocalInstalledNotLinkedExpert(t *testing.T) {
	ssh_ultimate.Prepare(t, nil, func(t *testing.T, sshConfig *ssh_config.Config) {
		logging.SetLogLevel(log.FatalLevel)
		version := cli.Version
		cli.Version = "v99.1.3"
		noColor := color.NoColor
		color.NoColor = true
		defer func() {
			cli.Version = version
			color.NoColor = noColor
		}()

		stateConfig := config.StateConfig{
			Version:         config.Version(),
			Name:            "b3f2035551f6e48c67489ace9f588f94",
			Source:          "repository",
			InstallLocation: ".local/share/dotfiles",
			Remote:          "https://gitlab.com/pidrakin/dotfiles-expert-example.git",
		}
		err := stateConfig.Save()
		tests.AssertNoError(t, err)

		_, err = git.Clone(stateConfig.Remote, stateConfig.GetContextPath())
		tests.AssertNoError(t, err)

		expected := map[string]string{
			"table":          "HOST   INSTALLED  LINKED  GIT_STATUS  \nlocal  true       false   up-to-date  \n",
			"wide table":     "HOST   CLI      INSTALLED  LINKED  REPO                                                     GIT_STATUS  \nlocal  v99.1.3  true       false   https://gitlab.com/pidrakin/dotfiles-expert-example.git  up-to-date  \n",
			"custom columns": "INSTALLED  LINKED  \ntrue       false   \n",
			"yaml":           "- host: local\n  cli: v99.1.3\n  installed: true\n  linked: false\n  repo: https://gitlab.com/pidrakin/dotfiles-expert-example.git\n  gitStatus: up-to-date\n",
			"json":           "[\n  {\n    \"host\": \"local\",\n    \"cli\": \"v99.1.3\",\n    \"installed\": true,\n    \"linked\": false,\n    \"repo\": \"https://gitlab.com/pidrakin/dotfiles-expert-example.git\",\n    \"gitStatus\": \"up-to-date\"\n  }\n]",
			"json compact":   "[{\"host\":\"local\",\"cli\":\"v99.1.3\",\"installed\":true,\"linked\":false,\"repo\":\"https://gitlab.com/pidrakin/dotfiles-expert-example.git\",\"gitStatus\":\"up-to-date\"}]",
			"go template":    "[map[Cli:v99.1.3 Collection: GitStatus:up-to-date Host:local Installed:true LinkStatus: Linked:false Repo:https://gitlab.com/pidrakin/dotfiles-expert-example.git]]",
		}

		for name, c := range formats {
			t.Run(fmt.Sprintf("%s test", name), func(t *testing.T) {
				reader := strings.NewReader("")
				var buf bytes.Buffer
				var errBuf bytes.Buffer
				var exit *int
				err = run(
					reader,
					&buf,
					&errBuf,
					func(i int) {
						exit = &i
					},
					true,
					false,
					c.Format,
					c.Template,
				)
				tests.AssertNoError(t, err)
				assert.Equal(t, expected[name], buf.String())
				assert.Nil(t, errBuf.Bytes())
				assert.Nil(t, exit)
			})
		}
	})
}

func Test_statusLocalInstalledLinked(t *testing.T) {
	ssh_ultimate.Prepare(t, nil, func(t *testing.T, sshConfig *ssh_config.Config) {
		logging.SetLogLevel(log.FatalLevel)
		runHooks := true
		version := cli.Version
		cli.Version = "v99.1.3"
		noColor := color.NoColor
		color.NoColor = true
		defer func() {
			cli.Version = version
			color.NoColor = noColor
		}()

		stateConfig := config.StateConfig{
			Version:         config.Version(),
			Name:            "b3f2035551f6e48c67489ace9f588f94",
			Source:          "repository",
			InstallLocation: ".local/share/dotfiles",
			Remote:          "https://gitlab.com/pidrakin/dotfiles-example.git",
		}
		err := stateConfig.Save()
		tests.AssertNoError(t, err)

		_, err = git.Clone(stateConfig.Remote, stateConfig.GetContextPath())
		tests.AssertNoError(t, err)

		err = link.Run(false, false, true, &runHooks, "")
		tests.AssertNoError(t, err)

		expected := map[string]string{
			"table":          "HOST   INSTALLED  LINKED  LINK_STATUS  GIT_STATUS  \nlocal  true       true    up-to-date   up-to-date  \n",
			"wide table":     "HOST   CLI      INSTALLED  LINKED  LINK_STATUS  REPO                                              GIT_STATUS  \nlocal  v99.1.3  true       true    up-to-date   https://gitlab.com/pidrakin/dotfiles-example.git  up-to-date  \n",
			"custom columns": "INSTALLED  LINKED  \ntrue       true    \n",
			"yaml":           "- host: local\n  cli: v99.1.3\n  installed: true\n  linked: true\n  linkStatus: up-to-date\n  repo: https://gitlab.com/pidrakin/dotfiles-example.git\n  gitStatus: up-to-date\n",
			"json":           "[\n  {\n    \"host\": \"local\",\n    \"cli\": \"v99.1.3\",\n    \"installed\": true,\n    \"linked\": true,\n    \"linkStatus\": \"up-to-date\",\n    \"repo\": \"https://gitlab.com/pidrakin/dotfiles-example.git\",\n    \"gitStatus\": \"up-to-date\"\n  }\n]",
			"json compact":   "[{\"host\":\"local\",\"cli\":\"v99.1.3\",\"installed\":true,\"linked\":true,\"linkStatus\":\"up-to-date\",\"repo\":\"https://gitlab.com/pidrakin/dotfiles-example.git\",\"gitStatus\":\"up-to-date\"}]",
			"go template":    "[map[Cli:v99.1.3 Collection: GitStatus:up-to-date Host:local Installed:true LinkStatus:up-to-date Linked:true Repo:https://gitlab.com/pidrakin/dotfiles-example.git]]",
		}

		for name, c := range formats {
			t.Run(fmt.Sprintf("%s test", name), func(t *testing.T) {
				reader := strings.NewReader("")
				var buf bytes.Buffer
				var errBuf bytes.Buffer
				var exit *int
				err = run(
					reader,
					&buf,
					&errBuf,
					func(i int) {
						exit = &i
					},
					true,
					false,
					c.Format,
					c.Template,
				)
				tests.AssertNoError(t, err)
				assert.Equal(t, expected[name], buf.String())
				assert.Nil(t, errBuf.Bytes())
				assert.Nil(t, exit)
			})
		}
	})
}

func Test_statusLocalInstalledLinkedExpert(t *testing.T) {
	ssh_ultimate.Prepare(t, nil, func(t *testing.T, sshConfig *ssh_config.Config) {
		logging.SetLogLevel(log.FatalLevel)
		runHooks := true
		version := cli.Version
		cli.Version = "v99.1.3"
		noColor := color.NoColor
		color.NoColor = true
		defer func() {
			cli.Version = version
			color.NoColor = noColor
		}()

		stateConfig := config.StateConfig{
			Version:         config.Version(),
			Name:            "b3f2035551f6e48c67489ace9f588f94",
			Source:          "repository",
			InstallLocation: ".local/share/dotfiles",
			Remote:          "https://gitlab.com/pidrakin/dotfiles-expert-example.git",
		}
		err := stateConfig.Save()
		tests.AssertNoError(t, err)

		_, err = git.Clone(stateConfig.Remote, stateConfig.GetContextPath())
		tests.AssertNoError(t, err)

		err = link.Run(false, false, true, &runHooks, "linux")
		tests.AssertNoError(t, err)

		expected := map[string]string{
			"table":          "HOST   INSTALLED  LINKED  LINK_STATUS  GIT_STATUS  \nlocal  true       true    up-to-date   up-to-date  \n",
			"wide table":     "HOST   CLI      INSTALLED  LINKED  COLLECTION  LINK_STATUS  REPO                                                     GIT_STATUS  \nlocal  v99.1.3  true       true    linux       up-to-date   https://gitlab.com/pidrakin/dotfiles-expert-example.git  up-to-date  \n",
			"custom columns": "INSTALLED  LINKED  \ntrue       true    \n",
			"yaml":           "- host: local\n  cli: v99.1.3\n  installed: true\n  linked: true\n  collection: linux\n  linkStatus: up-to-date\n  repo: https://gitlab.com/pidrakin/dotfiles-expert-example.git\n  gitStatus: up-to-date\n",
			"json":           "[\n  {\n    \"host\": \"local\",\n    \"cli\": \"v99.1.3\",\n    \"installed\": true,\n    \"linked\": true,\n    \"collection\": \"linux\",\n    \"linkStatus\": \"up-to-date\",\n    \"repo\": \"https://gitlab.com/pidrakin/dotfiles-expert-example.git\",\n    \"gitStatus\": \"up-to-date\"\n  }\n]",
			"json compact":   "[{\"host\":\"local\",\"cli\":\"v99.1.3\",\"installed\":true,\"linked\":true,\"collection\":\"linux\",\"linkStatus\":\"up-to-date\",\"repo\":\"https://gitlab.com/pidrakin/dotfiles-expert-example.git\",\"gitStatus\":\"up-to-date\"}]",
			"go template":    "[map[Cli:v99.1.3 Collection:linux GitStatus:up-to-date Host:local Installed:true LinkStatus:up-to-date Linked:true Repo:https://gitlab.com/pidrakin/dotfiles-expert-example.git]]",
		}

		for name, c := range formats {
			t.Run(fmt.Sprintf("%s test", name), func(t *testing.T) {
				reader := strings.NewReader("")
				var buf bytes.Buffer
				var errBuf bytes.Buffer
				var exit *int
				err = run(
					reader,
					&buf,
					&errBuf,
					func(i int) {
						exit = &i
					},
					true,
					false,
					c.Format,
					c.Template,
				)
				tests.AssertNoError(t, err)
				assert.Equal(t, expected[name], buf.String())
				assert.Nil(t, errBuf.Bytes())
				assert.Nil(t, exit)
			})
		}
	})
}
