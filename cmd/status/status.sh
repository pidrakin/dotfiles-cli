#!/bin/sh

export PATH=${HOME}/.local/bin/:${PATH}

command -v dotfiles >/dev/null
dotfiles_installed=$?

if [ ${dotfiles_installed} -ne 0 ]; then
  exit 2
fi

dotfiles status --local -o yaml
if [ $? -ne 0 ]; then
  echo "[status.sh] dotfiles status failed"
  exit 3
fi
