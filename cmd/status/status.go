package status

import (
	"bytes"
	_ "embed"
	"fmt"
	"io"
	"net"
	"os"

	"github.com/fatih/color"
	log "github.com/sirupsen/logrus"
	"gitlab.com/pidrakin/dotfiles-cli/cli"
	"gitlab.com/pidrakin/dotfiles-cli/cmd/common"
	"gitlab.com/pidrakin/dotfiles-cli/common/logging"
	"gitlab.com/pidrakin/dotfiles-cli/config"
	"gitlab.com/pidrakin/dotfiles-cli/git"
	"gitlab.com/pidrakin/dotfiles-cli/ssh_ultimate"
	output2 "gitlab.com/pidrakin/go/output"
	"gitlab.com/pidrakin/go/slices"
	"golang.org/x/crypto/ssh"
	"gopkg.in/yaml.v3"
)

//go:embed status.sh
var statusScript []byte

type Status struct {
	Host       string `yaml:"host" json:"host"`
	Cli        string `yaml:"cli" json:"cli" wide:""`
	Installed  bool   `yaml:"installed" json:"installed"`
	Linked     bool   `yaml:"linked" json:"linked"`
	Collection string `yaml:"collection,omitempty" json:"collection,omitempty" wide:""`
	LinkStatus string `yaml:"linkStatus,omitempty" json:"linkStatus,omitempty"`
	Repo       string `yaml:"repo,omitempty" json:"repo" wide:""`
	GitStatus  string `yaml:"gitStatus,omitempty" json:"gitStatus,omitempty"`
}

var formatter = func(name string, value string) string {
	var c color.Attribute
	if name == "INSTALLED" && value == "false" {
		c = color.FgRed
	}
	if name == "LINKED" && value == "false" {
		c = color.FgRed
	}
	if name == "LINK_STATUS" && value == "diff" {
		c = color.FgRed
	}
	if name == "GIT_STATUS" && value == "behind" {
		c = color.FgRed
	}
	if name == "CLI" && value != cli.Version {
		c = color.FgRed
	}
	if c != color.Reset {
		return color.New(c, color.Bold).Sprintf(value)
	}
	return value
}

func Run(local bool, remote bool, format output2.FormatType, template string) error {
	return run(os.Stdin, os.Stdout, nil, nil, local, remote, format, template)
}

func run(reader io.Reader, writer io.Writer, errWriter io.Writer, exitFunc func(int), local bool, remote bool, format output2.FormatType, template string) error {
	logging.SetOutputWriter(writer)
	logging.SetLogOutput(errWriter)
	logging.SetLogExitFunc(exitFunc)

	var statusList []*Status
	if local {
		localStatus, err := getLocalStatus()
		if err != nil {
			return err
		}
		statusList = append(statusList, localStatus)
	}
	if remote {
		remoteStatusList, remoteStatusErr := getRemoteStatus(reader, writer, errWriter, exitFunc)
		if remoteStatusErr != nil {
			return remoteStatusErr
		}
		statusList = append(statusList, remoteStatusList...)
	}
	return output2.Format(writer, format, formatter, template, statusList)
}

func getLocalStatus() (*Status, error) {
	s := &Status{
		Host: "local",
		Cli:  cli.Version,
	}

	cfgMgr, err := config.NewManager()
	if err != nil {
		return nil, err
	}

	if cfgMgr.StateConfig != nil {
		s.Installed = true
		s.Linked = cfgMgr.StateConfig.Linked != nil

		if cfgMgr.StateConfig.Linked != nil {
			s.Collection = cfgMgr.StateConfig.Collection
			differ := &common.Differ{}
			diffs, err := differ.Diff(cfgMgr.StateConfig.Collection)
			if err != nil {
				return nil, err
			}
			someDiffs := slices.Map(diffs, func(d common.Diff) bool {
				return d.ToAdd != nil || d.ToDelete != nil || d.ToModify != nil
			})
			anyDiff := slices.Contains(someDiffs, true)
			if anyDiff {
				s.LinkStatus = "diff"
			} else {
				s.LinkStatus = "up-to-date"
			}
		}

		s.Repo = cfgMgr.StateConfig.Remote

		behind, gitErr := git.Behind(cfgMgr.StateConfig.GetContextPath())
		if gitErr == nil {
			versionString := "up-to-date"
			if behind {
				versionString = "behind"
			}
			s.GitStatus = versionString
		}
	}

	return s, nil
}

func makeStatus(params *common.NamedRolloutParameters, tap func(status *[]*Status) error) (*Status, error) {
	status := []*Status{{}}
	if err := tap(&status); err != nil {
		return status[0], err
	}
	status[0].Host = params.Name
	return status[0], nil
}

func getRemoteStatus(reader io.Reader, writer io.Writer, errWriter io.Writer, exitFunc func(int)) ([]*Status, error) {
	var statusList []*Status
	paramList, rolloutErr := common.LoadRollouts()
	if rolloutErr != nil {
		return nil, rolloutErr
	}
	for _, params := range paramList {
		if !params.Track {
			continue
		}
		r, err := common.NewRemote(
			params.Host,
			append(
				params.Options,
				ssh_ultimate.ReaderOption(reader),
				ssh_ultimate.WriterOption(writer),
				ssh_ultimate.ErrWriterOption(errWriter),
				ssh_ultimate.ExitFuncOption(exitFunc),
			)...,
		)
		if err != nil {
			if netErr, ok := err.(*net.OpError); ok {
				if _, ok := netErr.Err.(*net.DNSError); ok {
					status, makeErr := makeStatus(params, func(status *[]*Status) error {
						return nil
					})
					status.Host += " (unreachable)"
					if makeErr != nil {
						return nil, makeErr
					}
					statusList = append(statusList, status)
				}
			}
			continue
		}
		err = r.Perform(func(remote *common.Remote) error {
			statusResponse, performErr := runStatusScript(remote.Connection, remote.LocalBinPath)
			if performErr != nil {
				return performErr
			}

			status, makeErr := makeStatus(params, func(status *[]*Status) error {
				return yaml.Unmarshal([]byte(statusResponse), status)
			})
			if makeErr != nil {
				return makeErr
			}
			statusList = append(statusList, status)
			return nil
		})
		if err != nil {
			return nil, err
		}
	}
	return statusList, nil
}

func runStatusScript(connection *ssh_ultimate.Connection, localBinPath string) (string, error) {
	if err := connection.SftpWriteUpload(statusScript, fmt.Sprintf("%s/status.sh", localBinPath), os.FileMode(0755)); err != nil {
		return "", err
	}

	statusCmd := fmt.Sprintf("%s/status.sh", localBinPath)
	var stdout, stderr bytes.Buffer
	cmdErr := connection.Run(false, &stdout, &stderr, statusCmd)
	if cmdErr != nil {
		if exitErr, ok := cmdErr.(*ssh.ExitError); ok {
			errorCode := exitErr.ExitStatus()
			log.Errorf("[dotfiles status] status failed %s\nExited: %v", stdout.String()+stderr.String(), errorCode)

			switch errorCode {
			case 2:
				log.Errorf("[dotfiles status] no dotfiles installed on remote")
			case 3:
				log.Errorf("[dotfiles status] dotfiles status failed")
			}
		} else {
			log.WithField("error", cmdErr.Error()).Errorf("[dotfiles status] unexpected error encountered")
		}

		return "", cmdErr
	}
	log.Debugf("[dotfiles status] status successful: %s", stdout.String()+stderr.String())

	return stdout.String(), nil
}
