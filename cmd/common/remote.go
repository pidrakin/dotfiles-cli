package common

import (
	"bytes"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"strings"

	log "github.com/sirupsen/logrus"
	"gitlab.com/pidrakin/dotfiles-cli/ssh_ultimate"
)

var SftpUploadBinary bool = false

type Remote struct {
	Reader       io.Reader
	Writer       io.Writer
	SshConfigs   []*ssh_ultimate.Config
	Connection   *ssh_ultimate.Connection
	LocalBinPath string
}

func NewRemote(host string, options ...func(*ssh_ultimate.Parameterizable) error) (*Remote, error) {
	r := &Remote{}

	if err := r.setup(host, options...); err != nil {
		return nil, err
	}

	localBinPath, err := makeLocalBinPath(r.Connection)
	if err != nil {
		return nil, err
	}
	r.LocalBinPath = localBinPath

	if err = uploadLocalBuild(r.Connection, r.LocalBinPath); err != nil {
		return nil, err
	}

	return r, nil
}

func setup(host string, options ...func(*ssh_ultimate.Parameterizable) error) (reader io.Reader, writer io.Writer, sshConfigs []*ssh_ultimate.Config, connection *ssh_ultimate.Connection, err error) {
	p, err := ssh_ultimate.NewParameterizable(options...)
	if err != nil {
		return
	}

	reader = p.Reader
	writer = p.Writer

	sshConfigs, err = ssh_ultimate.NewConfigs(host, options...)
	if err != nil {
		return
	}

	connection, err = ssh_ultimate.NewConnection(sshConfigs)
	if err != nil {
		return
	}

	return
}

func (r *Remote) setup(host string, options ...func(*ssh_ultimate.Parameterizable) error) error {
	reader, writer, sshConfigs, connection, err := setup(host, options...)
	if err != nil {
		return err
	}
	r.Reader = reader
	r.Writer = writer
	r.SshConfigs = sshConfigs
	r.Connection = connection
	return nil
}

func makeLocalBinPath(connection *ssh_ultimate.Connection) (string, error) {
	mkdirCmd := " mkdir -p ~/.local/bin/ 2>&1 > /dev/null && dirname ~/.local/bin/"
	var stdout, stderr bytes.Buffer
	cmdErr := connection.Run(false, &stdout, &stderr, mkdirCmd)
	if cmdErr != nil {
		log.Errorf("[dotfiles rollout] creating ~/.local/bin/ failed %s", stdout.String()+stderr.String())
		return "", cmdErr
	}
	log.Debugf("[dotfiles rollout] creating ~/.local/bin/ succesful %s", stdout.String()+stderr.String())
	localBinPath := fmt.Sprintf("%s/bin", strings.TrimSpace(stdout.String()))
	if !filepath.IsAbs(localBinPath) {
		return "", fmt.Errorf("[dotfiles rollout] creating ~/.local/bin/ failed: %s", localBinPath)
	}
	return localBinPath, nil
}

func uploadLocalBuild(connection *ssh_ultimate.Connection, localBinPath string) error {
	if SftpUploadBinary {
		remoteBinaryPath := fmt.Sprintf("%s/dotfiles", localBinPath)
		debugBinaryPath := "/tmp/dotfiles-test-builds/dotfiles"
		log.Debugf("[dotfiles rollout] upload %s to %s", debugBinaryPath, remoteBinaryPath)

		if err := connection.SftpUploadFile(debugBinaryPath, remoteBinaryPath, os.FileMode(0755)); err != nil {
			return err
		}
	}
	return nil
}

func (r *Remote) Perform(wrapper func(*Remote) error) error {
	return wrapper(r)
}
