package common

import (
	"os"
	"path/filepath"

	"github.com/mitchellh/go-homedir"
	"gitlab.com/pidrakin/dotfiles-cli/common/logging"
	regex2 "gitlab.com/pidrakin/dotfiles-cli/common/regex"
	"gitlab.com/pidrakin/dotfiles-cli/config"
	"gitlab.com/pidrakin/dotfiles-cli/ssh_ultimate"
	"gitlab.com/pidrakin/go/regex"
	"gitlab.com/pidrakin/go/slices"
	"gitlab.com/pidrakin/go/sysfs"
	"gopkg.in/yaml.v3"
)

type identityFile struct {
	Path       string `yaml:"path"`
	Passphrase string `yaml:"passphrase,omitempty"`
}

type sshRolloutYAML struct {
	Host         string        `yaml:"host"`
	Port         string        `yaml:"port,omitempty"`
	User         string        `yaml:"user,omitempty"`
	IdentityFile *identityFile `yaml:"identityFile,omitempty"`
	RootInstall  bool          `yaml:"rootInstall"`
	Uri          string        `yaml:"uri"`
	Collection   *string       `yaml:"collection,omitempty"`
	Track        bool          `yaml:"track"`
}

func SaveRollout(rootInstall bool, remote string, uri string, collection string, track bool, options ...func(*ssh_ultimate.Parameterizable) error) error {
	host := remote
	groups := regex.MatchGroups(regex2.UserHostPortRegex, remote)
	if h, ok := groups["host"]; ok {
		host = h
	}
	if p, ok := groups["port"]; ok {
		options = slices.Prepend(options, ssh_ultimate.PortOption(p))
	}
	if u, ok := groups["user"]; ok {
		options = slices.Prepend(options, ssh_ultimate.UserOption(u))
	}

	p, err := ssh_ultimate.NewParameterizable(options...)
	if err != nil {
		return err
	}

	rollout := &sshRolloutYAML{
		Host:        host,
		Port:        p.Port,
		User:        p.User,
		RootInstall: rootInstall,
		Uri:         uri,
		Track:       track,
	}

	if p.IdentityFilePath != "" {
		rollout.IdentityFile = &identityFile{
			Path:       p.IdentityFilePath,
			Passphrase: p.IdentityFilePassphrase,
		}
	}

	if collection != "" {
		rollout.Collection = &collection
	}

	rolloutsDir, err := buildRolloutsPath()
	if err != nil {
		return err
	}

	if err = os.MkdirAll(rolloutsDir, os.FileMode(0755)); err != nil {
		return err
	}

	fileName := remote
	fileContent, err := yaml.Marshal(rollout)
	if err != nil {
		return err
	}
	filePath := filepath.Join(rolloutsDir, fileName)
	return os.WriteFile(filePath, fileContent, os.FileMode(0644))
}

type RolloutParameters struct {
	Host        string
	Options     []func(*ssh_ultimate.Parameterizable) error
	RootInstall bool
	Uri         string
	Collection  string
	LinkerFlags []string
	Track       bool
}

type NamedRolloutParameters struct {
	Name string
	*RolloutParameters
}

func loadRollout(rolloutsDir string, name string) (*NamedRolloutParameters, error) {
	var rollout sshRolloutYAML
	filePath := filepath.Join(rolloutsDir, name)
	fileContent, err := os.ReadFile(filePath)
	if err != nil {
		return nil, err
	}
	if err = yaml.Unmarshal(fileContent, &rollout); err != nil {
		return nil, err
	}

	params := &NamedRolloutParameters{
		Name: name,
		RolloutParameters: &RolloutParameters{
			Host: rollout.Host,
			Options: []func(*ssh_ultimate.Parameterizable) error{
				ssh_ultimate.PortOption(rollout.Port),
				ssh_ultimate.UserOption(rollout.User),
			},
			RootInstall: rollout.RootInstall,
			Uri:         rollout.Uri,
		},
	}
	if rollout.IdentityFile != nil {
		params.Options = append(
			params.Options,
			ssh_ultimate.IdentityFilePathOption(rollout.IdentityFile.Path),
			ssh_ultimate.IdentityFilePassphraseOption(rollout.IdentityFile.Passphrase),
		)
	}
	if rollout.Collection != nil {
		params.Collection = *rollout.Collection
	}
	if rollout.Track {
		params.Track = rollout.Track
	}

	return params, nil
}

func LoadRollout(name string) (*NamedRolloutParameters, error) {
	rolloutsDir, err := buildRolloutsPath()
	if err != nil {
		return nil, err
	}
	return loadRollout(rolloutsDir, name)
}

func LoadRollouts() ([]*NamedRolloutParameters, error) {
	rolloutsDir, err := buildRolloutsPath()
	if err != nil {
		return nil, err
	}

	var paramList []*NamedRolloutParameters

	if !sysfs.DirExists(rolloutsDir) {
		return paramList, nil
	}

	files, err := os.ReadDir(rolloutsDir)
	if err != nil {
		return nil, err
	}

	for _, file := range files {
		params, err := loadRollout(rolloutsDir, file.Name())
		if err != nil {
			return nil, err
		}
		paramList = append(paramList, params)
	}

	return paramList, nil
}

func buildRolloutsPath() (string, error) {
	var err error
	installLocation := config.GetLocalInstallDir()
	if !filepath.IsAbs(installLocation) {
		installLocation, err = homedir.Expand(installLocation)
		if err != nil {
			return "", err
		}
		if !filepath.IsAbs(installLocation) {
			homeDir, homeErr := homedir.Dir()
			if homeErr != nil {
				return "", homeErr
			}

			installLocation = filepath.Join(homeDir, installLocation)
		}
	}
	return filepath.Join(installLocation, "rollouts"), err
}

func RemoveRollout(name string) error {
	rollouts, err := LoadRollouts()
	if err != nil {
		return err
	}
	var rollout *NamedRolloutParameters
	for _, r := range rollouts {
		if r.Name == name {
			rollout = r
			break
		}
	}
	if rollout == nil {
		rolloutsAvailable := make([]string, len(rollouts))
		for i, r := range rollouts {
			rolloutsAvailable[i] = r.Name
		}
		logging.Failed("rollout with name [%s] not found; available %v", name, rolloutsAvailable)
		return nil
	}
	// remove
	rolloutsDir, err := buildRolloutsPath()
	if err != nil {
		return err
	}
	filePath := filepath.Join(rolloutsDir, name)
	return os.Remove(filePath)
}
