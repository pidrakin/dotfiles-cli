package common

import (
	"os"
	"path/filepath"
	"testing"

	"github.com/kevinburke/ssh_config"
	"github.com/stretchr/testify/assert"
	"gitlab.com/pidrakin/dotfiles-cli/config"
	"gitlab.com/pidrakin/dotfiles-cli/ssh_ultimate"
	"gitlab.com/pidrakin/go/tests"
	"gopkg.in/yaml.v3"
)

func TestSaveAndLoad(t *testing.T) {
	ssh_ultimate.Prepare(t, nil, func(t *testing.T, sshConfig *ssh_config.Config) {
		stateConfig := config.StateConfig{
			Version:         config.Version(),
			Name:            "b3f2035551f6e48c67489ace9f588f94",
			Source:          "repository",
			InstallLocation: ".local/share/dotfiles",
			Remote:          "https://gitlab.com/satanik/dotfiles.git",
		}
		err := stateConfig.Save()
		tests.AssertNoError(t, err)

		cfg := config.NewConfig()
		yamlData, err := yaml.Marshal(cfg)
		tests.AssertNoError(t, err)

		parent := filepath.Join(stateConfig.GetContextPath(), cfg.Dir())
		err = os.MkdirAll(parent, os.FileMode(0755))
		tests.AssertNoError(t, err)
		cfgPath := filepath.Join(parent, cfg.Name())

		err = os.WriteFile(cfgPath, yamlData, os.FileMode(0644))
		tests.AssertNoError(t, err)

		err = SaveRollout(false, "localhost", stateConfig.Remote, "", true)
		tests.AssertNoError(t, err)

		paramList, err := LoadRollouts()
		tests.AssertNoError(t, err)
		assert.Equal(t, 1, len(paramList))
		assert.Equal(t, "localhost", paramList[0].Host)
		assert.Equal(t, stateConfig.Remote, paramList[0].Uri)
		assert.Equal(t, false, paramList[0].RootInstall)
		assert.Equal(t, "", paramList[0].Collection)
		assert.Equal(t, true, paramList[0].Track)

		p, err := ssh_ultimate.NewParameterizable(paramList[0].Options...)
		tests.AssertNoError(t, err)

		assert.Empty(t, p.Port)
		assert.Empty(t, p.User)
		assert.Empty(t, p.IdentityFilePath)
		assert.Empty(t, p.IdentityFilePassphrase)

		err = SaveRollout(
			false,
			"example.com",
			stateConfig.Remote,
			"",
			true,
			ssh_ultimate.PortOption("2022"),
			ssh_ultimate.UserOption("user"),
			ssh_ultimate.IdentityFilePathOption("~/.ssh/id_rsa"),
		)
		tests.AssertNoError(t, err)

		paramList, err = LoadRollouts()
		tests.AssertNoError(t, err)
		assert.Equal(t, 2, len(paramList))
		assert.Equal(t, "example.com", paramList[0].Host)

		p, err = ssh_ultimate.NewParameterizable(paramList[0].Options...)
		tests.AssertNoError(t, err)

		assert.Equal(t, "2022", p.Port)
		assert.Equal(t, "user", p.User)
		assert.Equal(t, "~/.ssh/id_rsa", p.IdentityFilePath)
		assert.Empty(t, p.IdentityFilePassphrase)
	})
}

func TestSaveAndLoadNoTrack(t *testing.T) {
	ssh_ultimate.Prepare(t, nil, func(t *testing.T, sshConfig *ssh_config.Config) {
		stateConfig := config.StateConfig{
			Version:         config.Version(),
			Name:            "b3f2035551f6e48c67489ace9f588f94",
			Source:          "repository",
			InstallLocation: ".local/share/dotfiles",
			Remote:          "https://gitlab.com/satanik/dotfiles.git",
		}
		err := stateConfig.Save()
		tests.AssertNoError(t, err)

		cfg := config.NewConfig()
		yamlData, err := yaml.Marshal(cfg)
		tests.AssertNoError(t, err)

		parent := filepath.Join(stateConfig.GetContextPath(), cfg.Dir())
		err = os.MkdirAll(parent, os.FileMode(0755))
		tests.AssertNoError(t, err)
		cfgPath := filepath.Join(parent, cfg.Name())

		err = os.WriteFile(cfgPath, yamlData, os.FileMode(0644))
		tests.AssertNoError(t, err)

		err = SaveRollout(false, "localhost", stateConfig.Remote, "", false)
		tests.AssertNoError(t, err)

		paramList, err := LoadRollouts()
		tests.AssertNoError(t, err)
		assert.Equal(t, 1, len(paramList))
		assert.Equal(t, "localhost", paramList[0].Host)
		assert.Equal(t, stateConfig.Remote, paramList[0].Uri)
		assert.Equal(t, false, paramList[0].RootInstall)
		assert.Equal(t, "", paramList[0].Collection)
		assert.Equal(t, false, paramList[0].Track)
	})
}
