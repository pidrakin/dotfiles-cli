package common

import (
	"github.com/mitchellh/go-homedir"
	log "github.com/sirupsen/logrus"
	"gitlab.com/pidrakin/dotfiles-cli/common/logging"
	"gitlab.com/pidrakin/dotfiles-cli/config"
	"gitlab.com/pidrakin/dotfiles-cli/text"
	sysfs2 "gitlab.com/pidrakin/go/sysfs"
	"os"
	"path"
)

func Unlink(stateConfig *config.StateConfig, exitFunc func(int)) (deletedFiles []string, deletedDirs []string, err error) {
	logging.SetOutputExitFunc(exitFunc)
	linked := stateConfig.Linked
	if linked == nil || linked.Empty() {
		log.Info(text.NothingToUnlink)
		return
	}

	if linked.IsPackages() {
		for pkg, files := range linked.Packages {
			delFiles, delDirs, err := unlinkFiles(stateConfig, pkg, files)
			if err != nil {
				return nil, nil, err
			}
			deletedFiles = append(deletedFiles, delFiles...)
			deletedDirs = append(deletedDirs, delDirs...)
		}
	} else {
		if deletedFiles, deletedDirs, err = unlinkFiles(stateConfig, "", linked.Files); err != nil {
			return
		}
	}

	stateConfig.Linked = nil
	return
}

func unlinkFiles(stateConfig *config.StateConfig, pkg string, files []string) (deletedFiles []string, deletedDirs []string, err error) {
	homeDir, err := homedir.Dir()
	if err != nil {
		return nil, nil, err
	}

	for _, file := range files {
		contextPath := path.Join(stateConfig.GetContextPath(), pkg, file)
		linkPath := path.Join(homeDir, file)
		dirs, linkErr := unlinkFile(linkPath, contextPath)
		if linkErr != nil {
			return nil, nil, linkErr
		}
		deletedDirs = append(deletedDirs, dirs...)
		deletedFiles = append(deletedFiles, linkPath)
	}
	return
}

func unlinkFile(link string, target string) (deletedDirs []string, err error) {
	homeDir, err := homedir.Dir()
	if err != nil {
		return nil, err
	}

	linkTarget, _ := sysfs2.ReadLink(link)
	if linkTarget == target {
		err = sysfs2.DeleteSymlink("", link)
		if err != nil {
			return
		}
		parent := path.Dir(link)
		for empty := true; empty == true; {
			if parent == homeDir {
				break
			}
			if empty, err = sysfs2.IsEmpty(parent); empty && err == nil {
				log.WithField("dir", parent).Debug(text.RemovingFile)
				if !sysfs2.DryRun {
					if err = os.Remove(parent); err != nil {
						return
					}
				}
				deletedDirs = append(deletedDirs, parent)
			}
			parent = path.Dir(parent)
		}
	}
	return
}
