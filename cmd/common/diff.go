package common

import (
	"fmt"
	fs2 "io/fs"
	"os"
	"path"
	"path/filepath"
	"strings"

	"gitlab.com/pidrakin/dotfiles-cli/common/logging"
	"gitlab.com/pidrakin/go/interactive"
	"gitlab.com/pidrakin/go/maps"
	"gitlab.com/pidrakin/go/slices"
	sysfs2 "gitlab.com/pidrakin/go/sysfs"

	"github.com/mitchellh/go-homedir"
	log "github.com/sirupsen/logrus"
	"gitlab.com/pidrakin/dotfiles-cli/config"
	"gitlab.com/pidrakin/dotfiles-cli/text"
)

type Diff struct {
	Root       string
	Collection string
	ToAdd      []string
	ToDelete   []string
	ToIgnore   []string
	ToModify   []string
}

func (diff *Diff) merge(other Diff) (result Diff) {
	result.ToAdd = slices.Unique(append(diff.ToAdd, other.ToAdd...))
	result.ToDelete = slices.Unique(append(diff.ToDelete, other.ToDelete...))
	result.ToIgnore = slices.Unique(append(diff.ToIgnore, other.ToIgnore...))
	result.ToModify = slices.Unique(append(diff.ToModify, other.ToModify...))

	return
}

type Differ struct {
	repoPath          string
	ConfigManager     *config.Manager
	collectionsConfig *config.CollectionsConfig
	stateConfig       *config.StateConfig
	ignored           []string
	configPath        string

	prompter *interactive.Prompter
}

func (differ *Differ) Diff(collection ...string) (diffs []Diff, err error) {

	if differ.prompter == nil {
		differ.prompter, err = interactive.NewPrompter(interactive.Interactive)
		if err != nil {
			return
		}
	}

	if err = differ.Init(); err != nil {
		return
	}

	if len(collection) > 0 && collection[0] != "" {
		if !slices.Contains(maps.Keys(differ.collectionsConfig.Collections), collection[0]) {
			if err = logging.Failed("collection [%s] does not exist", collection[0]); err != nil {
				return
			}
		}
		differ.stateConfig.Collection = collection[0]
	}

	if differ.collectionsConfig != nil {
		return differ.diffExpert()
	} else {
		return differ.diffSimple()
	}
}

func (differ *Differ) Init() error {
	var err error
	differ.ConfigManager, err = config.NewManager()
	if err != nil {
		return err
	}
	differ.stateConfig = differ.ConfigManager.StateConfig
	if differ.stateConfig == nil {
		return fmt.Errorf(text.InstallFirst)
	}
	differ.collectionsConfig = differ.ConfigManager.CollectionsConfig
	differ.configPath = differ.ConfigManager.ConfigPath()

	differ.repoPath = differ.ConfigManager.StateConfig.GetContextPath()
	log.WithField("repo path", differ.repoPath).Debugf("setting repo path")

	differ.ignored = differ.ConfigManager.Config.Ignore
	dotfilesignorepath := path.Join(differ.repoPath, differ.ConfigManager.Config.IgnoreFile)
	if _, err = os.Stat(dotfilesignorepath); err == nil {
		content, err := os.ReadFile(dotfilesignorepath)
		if err != nil {
			return err
		}
		differ.ignored = append(differ.ignored, strings.Split(string(content), "\n")...)
	}

	return nil
}

func (differ *Differ) diffSimple() (diffs []Diff, err error) {
	diff, err := differ.diffRecursive(differ.stateConfig.GetContextPath(), "")
	if err != nil {
		return
	}
	diffs = append(diffs, diff)
	return
}

func (differ *Differ) diffExpert() (diffs []Diff, err error) {
	files, err := os.ReadDir(differ.stateConfig.GetContextPath())
	if err != nil {
		return
	}

	names := []string{}
	for _, file := range files {
		contains := slices.Contains(differ.ConfigManager.Config.Ignore, file.Name())
		if !contains && file.IsDir() {
			names = append(names, file.Name())
		}
	}

	var key string
	err = differ.prompter.TmpInteractiveChange(func() error {
		if differ.stateConfig.Collection == "" {
			differ.prompter.SetInteractive(true)
			key = differ.interactiveCollectionPrompt(differ.collectionsConfig.Collections)
			differ.stateConfig.Collection = key
		} else {
			key = differ.stateConfig.Collection
		}
		return nil
	})
	if err != nil {
		return
	}
	collection := differ.collectionsConfig.Collections[key]

	fileList := make([]string, len(names))
	_ = copy(fileList, names)
	if len(collection.Simple) > 0 {
		fileList = slices.Intersect(fileList, ([]string)(collection.Simple))
	} else {
		if len(collection.Expert.Ignore) > 0 {
			fileList, _ = slices.Diff(fileList, collection.Expert.Ignore)
		}

		if len(collection.Expert.Default) > 0 {
			fileList = slices.Intersect(fileList, collection.Expert.Default)
		}

		if len(collection.Expert.Optional) > 0 {
			optionals := slices.Intersect(names, collection.Expert.Optional)
			if len(optionals) > 0 {
				chosen, err := differ.interactiveOptionalPrompt(optionals)
				if err != nil {
					return diffs, err
				}
				fileList = append(fileList, chosen...)
			}
		}

		if len(collection.Expert.Mutex) > 0 {
			for _, mutex := range collection.Expert.Mutex {
				invalid := len(slices.Intersect(fileList, mutex)) > 1
				if invalid {
					return diffs, fmt.Errorf("collectionsConfig does not allow multiple of %v", mutex)
				}
			}
		}
	}
	for _, dir := range fileList {
		root := filepath.Join(differ.stateConfig.GetContextPath(), dir)
		diff, err := differ.diffRecursive(root, dir)
		if err != nil {
			return diffs, err
		}
		diffs = append(diffs, diff)
	}
	return
}

func (differ *Differ) diffRecursive(root string, collection string) (diff Diff, err error) {
	homeDir, err := homedir.Dir()
	if err != nil {
		return
	}

	diff.Root = root
	diff.Collection = collection
	filesFound, err := differ.findLinkTargets(root)
	if err != nil {
		return
	}
	if diff.ToDelete, err = differ.findStaleLinks(root); err != nil {
		return
	}

	for _, rel := range filesFound {
		source := filepath.Join(root, rel)
		target := filepath.Join(homeDir, rel)

		_, pathErr := os.Stat(target)
		isSymlink, _ := sysfs2.ReadLink(target)
		if os.IsNotExist(pathErr) {
			diff.ToAdd = append(diff.ToAdd, rel)
		} else if isSymlink != source {
			diff.ToModify = append(diff.ToModify, rel)
		} else {
			diff.ToIgnore = append(diff.ToIgnore, rel)
		}
	}
	return
}

func (differ *Differ) findStaleLinks(root string) (linkList []string, err error) {
	homeDir, err := homedir.Dir()
	if err != nil {
		return
	}

	for _, file := range differ.stateConfig.Linked.Get(root) {
		contextPath := filepath.Join(differ.stateConfig.GetContextPath(), file)

		linkPath := filepath.Join(homeDir, file)
		linkTarget, _ := sysfs2.ReadLink(linkPath)
		if linkTarget == contextPath {
			if _, err := os.Stat(linkTarget); os.IsNotExist(err) {
				linkList = append(linkList, file)
			}
		}
	}
	return
}

func (differ *Differ) findLinkTargets(root string) (filesFound []string, err error) {

	if _, err = os.Stat(root); err != nil {
		return
	}

	ignored := make([]string, len(differ.ignored))
	_ = copy(ignored, differ.ignored)

	err = sysfs2.Walk(root, func(p string, entry fs2.FileInfo, err error) error {
		rel, err := filepath.Rel(root, p)
		if err != nil {
			return err
		}
		if rel == "." {
			return nil
		}
		if slices.Contains(ignored, rel) {
			if entry.IsDir() {
				return filepath.SkipDir
			} else {
				return nil
			}
		}
		if entry.IsDir() {
			return nil
		}

		filesFound = append(filesFound, rel)
		return nil
	})
	return
}

func (differ *Differ) interactiveCollectionPrompt(collections config.Collections) (collection string) {
	return config.InteractiveCollectionPrompt(differ.prompter, collections)
}

func (differ *Differ) interactiveOptionalPrompt(optionals []string) (chosen []string, err error) {

	if len(optionals) == 0 {
		return
	}

	for _, option := range optionals {
		result, promptErr := differ.prompter.YesNo(fmt.Sprintf("use candidate [%s]", option))
		if promptErr != nil {
			return chosen, promptErr
		}
		if result {
			chosen = append(chosen, option)
		}
	}

	return chosen, err
}
