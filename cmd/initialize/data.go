package initialize

import _ "embed"

//go:embed data/.dotfilescollections
var dotfilesExpertYAML []byte

//go:embed data/pre-link
var preLinkHookFile []byte

type DefaultData struct {
	DotfilesYAML File
}

var simpleDefaultData = DefaultData{}

var expertDefaultData = DefaultData{
	DotfilesYAML: File{
		Name:    ".dotfilescollections",
		Content: dotfilesExpertYAML,
	},
}
