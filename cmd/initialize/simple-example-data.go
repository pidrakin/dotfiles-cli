package initialize

import _ "embed"

//go:embed data/example/simple/post-link
var simpleExamplePostLinkHookFile []byte

//go:embed data/example/simple/.bashrc
var simpleExampleBASHRC []byte

//go:embed data/example/simple/.profile
var simpleExamplePROFILE []byte

type SimpleExampleData struct {
	PostLinkHook File
	PreLinkHook  File
	BASHRC       File
	PROFILE      File
}

var simpleData = SimpleExampleData{
	PreLinkHook: File{
		Name:    "pre-link",
		Content: preLinkHookFile,
	},
	PostLinkHook: File{
		Name:    "post-link",
		Content: simpleExamplePostLinkHookFile,
	},
	BASHRC: File{
		Name:    ".bashrc",
		Content: simpleExampleBASHRC,
	},
	PROFILE: File{
		Name:    ".profile",
		Content: simpleExamplePROFILE,
	},
}
