package initialize

import (
	log "github.com/sirupsen/logrus"
	"gitlab.com/pidrakin/dotfiles-cli/cli"
	"gitlab.com/pidrakin/dotfiles-cli/common/logging"
	"gitlab.com/pidrakin/dotfiles-cli/config"
	"gitlab.com/pidrakin/dotfiles-cli/text"
	sysfs2 "gitlab.com/pidrakin/go/sysfs"
	"gopkg.in/yaml.v3"
	"io"
	"os"
	"path"
)

func Run(expert bool, example bool) error {
	return initialize(cli.DryRun, expert, example, os.Stderr)
}

func getWd(writer io.Writer) (string, error) {
	defer logging.SetLogOutput(writer)()
	wd, err := os.Getwd()
	if err != nil {
		return "", err
	}
	log.WithField("dir", wd).Info(text.CWD)
	return wd, nil
}

func initialize(dryRun bool, expert bool, example bool, writer io.Writer) error {
	wd, err := getWd(writer)
	if err != nil {
		return err
	}

	cfg := config.NewConfig()

	// ensure .dotfiles directory is created
	if err = ensureDir(dryRun, wd, cfg.Dir(), writer); err != nil {
		return err
	}
	if err = ensureConfigFile(dryRun, wd, *cfg, writer); err != nil {
		return err
	}
	hooksDirectory, err := ensureHooksDirectory(dryRun, wd, *cfg, writer)
	if err != nil {
		return err
	}

	if !example {
		if !expert {
			err = ensureBasicSimpleStructure(dryRun, wd, writer)
		} else {
			err = ensureBasicExpertStructure(dryRun, wd, writer)
		}
	} else {
		if !expert {
			err = ensureExampleSimpleStructure(dryRun, wd, hooksDirectory, writer)
		} else {
			err = ensureExampleExpertStructure(dryRun, wd, hooksDirectory, writer)
		}
	}
	if err != nil {
		return err
	}
	return logging.Successfully("initialized")
}

func ensureConfigFile(dryRun bool, wd string, cfg config.Config, writer io.Writer) error {
	if err := ensureDir(dryRun, wd, cfg.Dir(), writer); err != nil {
		return err
	}
	yamlData, err := yaml.Marshal(&cfg)
	if err != nil {
		return err
	}
	cfgFile := File{
		Name:    cfg.Name(),
		Content: yamlData,
	}
	return ensureFile(dryRun, path.Join(wd, cfg.Dir()), cfgFile, false, writer)
}

func ensureHooksDirectory(dryRun bool, wd string, cfg config.Config, writer io.Writer) (string, error) {
	hooksDirectory := path.Join(cfg.Dir(), cfg.HooksDir)
	err := ensureDir(dryRun, wd, hooksDirectory, writer)
	return path.Join(wd, hooksDirectory), err
}

func ensureBasicSimpleStructure(dryRun bool, wd string, writer io.Writer) error {
	return nil
}

func ensureBasicExpertStructure(dryRun bool, wd string, writer io.Writer) error {
	return ensureFile(dryRun, wd, expertDefaultData.DotfilesYAML, false, writer)
}

func ensureExampleSimpleStructure(dryRun bool, wd string, hooksDir string, writer io.Writer) error {
	data := simpleData

	files := []File{
		data.BASHRC,
		data.PROFILE,
	}

	for _, file := range files {
		if err := ensureFile(dryRun, wd, file, false, writer); err != nil {
			return err
		}
	}

	return initHooks(dryRun, hooksDir, data.PreLinkHook, data.PostLinkHook, writer)
}

func ensureExampleExpertStructure(dryRun bool, wd string, hooksDir string, writer io.Writer) error {
	data := expertData

	packages := map[string][]File{
		"": {
			data.DotfilesCollectionsYAML,
			data.DotfilesIgnoreYAML,
		},
		"ignore": {
			data.FoobarTXT,
		},
		"bash": {
			data.BASHRC,
			data.PROFILE,
		},
		"vim": {
			data.VIMRC,
		},
		"nano": {
			data.NANORC,
		},
		"tmux": {
			data.TMUXCONF,
		},
		"git": {
			data.GITCONFIG,
		},
		"ssh/.ssh": {
			data.SSHCONFIG,
		},
	}

	if err := ensureFiles(dryRun, wd, packages, false, writer); err != nil {
		return err
	}

	hooksList := map[string][]File{
		"vim": {
			data.PostLinkVIM,
		},
		"ssh": {
			data.PostLinkSSH,
		},
	}

	if err := ensureFiles(dryRun, hooksDir, hooksList, true, writer); err != nil {
		return err
	}

	return initHooks(dryRun, hooksDir, data.PreLinkHook, data.PostLinkHook, writer)
}

func initHooks(dryRun bool, hooksDir string, preHookFile File, postHookFile File, writer io.Writer) error {
	if err := ensureFile(dryRun, hooksDir, preHookFile, true, writer); err != nil {
		return err
	}
	return ensureFile(dryRun, hooksDir, postHookFile, true, writer)
}

func ensureDir(dryRun bool, wd string, name string, writer io.Writer) error {
	defer logging.SetLogOutput(writer)()
	dirpath := path.Join(wd, name)
	log.WithFields(log.Fields{
		"name": name,
		"dir":  dirpath,
	}).Debug(text.EnsureFileExists)
	if !sysfs2.DirExists(dirpath) {
		if !dryRun {
			if err := os.MkdirAll(dirpath, os.FileMode(0755)); err != nil {
				return err
			}
		}
		log.WithFields(log.Fields{
			"name": name,
			"file": dirpath,
			"perm": os.FileMode(0755),
		}).Info(text.CreatedFile)
	}
	return nil
}

func ensureFile(dryRun bool, wd string, file File, executable bool, writer io.Writer) error {
	defer logging.SetLogOutput(writer)()
	var mode os.FileMode
	if executable {
		mode = os.FileMode(0755)
	} else {
		mode = os.FileMode(0644)
	}
	filepath := path.Join(wd, file.Name)
	log.WithFields(log.Fields{
		"name": file.Name,
		"file": filepath,
	}).Debug(text.EnsureFileExists)
	if !sysfs2.FileExists(filepath) {
		if !dryRun {
			if err := os.WriteFile(filepath, file.Content, mode); err != nil {
				return err
			}
		}
		log.WithFields(log.Fields{
			"name": file.Name,
			"file": filepath,
			"perm": mode,
		}).Info(text.CreatedFile)
	}
	return nil
}

func ensureFiles(dryRun bool, basepath string, collection map[string][]File, executable bool, writer io.Writer) error {
	for subdir, files := range collection {
		dir := path.Join(basepath, subdir)
		if err := ensureDir(dryRun, basepath, subdir, writer); err != nil {
			return err
		}
		for _, file := range files {
			if err := ensureFile(dryRun, dir, file, executable, writer); err != nil {
				return err
			}
		}
	}
	return nil
}
