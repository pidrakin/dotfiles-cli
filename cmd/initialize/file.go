package initialize

type File struct {
	Name    string
	Content []byte
}
