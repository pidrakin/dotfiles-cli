package initialize

import (
	"bytes"
	"fmt"
	"github.com/stretchr/testify/assert"
	"gitlab.com/pidrakin/dotfiles-cli/config"
	"gitlab.com/pidrakin/go/tests"
	"os"
	"path"
	"strings"
	"testing"
)

func TestGetWd(t *testing.T) {
	tests.RunInTmp(t, func(t *testing.T, expectedWd string) {
		var buf bytes.Buffer
		wd, err := getWd(&buf)
		tests.AssertNoError(t, err)

		assert.Equal(t, expectedWd, wd)
		dirMsg := strings.Split(buf.String(), "=")[4]
		assert.Equal(t, fmt.Sprintf("%s\n", expectedWd), dirMsg)
	})
}

// TODO: test dryRun
func TestEnsureConfigFile(t *testing.T) {
	tests.RunInTmp(t, func(t *testing.T, wd string) {
		cfg := config.NewConfig()

		var buf bytes.Buffer
		err := ensureConfigFile(false, wd, *cfg, &buf)
		tests.AssertNoError(t, err)

		tests.AssertDirExists(t, ".dotfiles")
		tests.AssertFileExists(t, ".dotfiles/config")

		// TODO: assert buf
	})
}

// TODO: test dryRun
func TestEnsureHooksDirectory(t *testing.T) {
	tests.RunInTmp(t, func(t *testing.T, wd string) {
		cfg := config.NewConfig()

		var buf bytes.Buffer
		hooksDir, err := ensureHooksDirectory(false, wd, *cfg, &buf)
		tests.AssertNoError(t, err)

		tests.AssertDirExists(t, hooksDir)
		assert.Equal(t, fmt.Sprintf("%s/.dotfiles/hooks", wd), hooksDir)

		// TODO: assert buf
	})
}

func TestEnsureBasicSimpleStructure(t *testing.T) {
	err := ensureBasicSimpleStructure(false, "", nil)
	tests.AssertNoError(t, err)
}

// TODO: test dryRun
func TestEnsureBasicExpertStructure(t *testing.T) {
	tests.RunInTmp(t, func(t *testing.T, wd string) {
		var buf bytes.Buffer
		err := ensureBasicExpertStructure(false, wd, &buf)
		tests.AssertNoError(t, err)

		tests.AssertFileExists(t, ".dotfilescollections")

		// TODO: assert buf
	})
}

// TODO: test dryRun
func TestEnsureExampleSimpleStructure(t *testing.T) {
	tests.RunInTmp(t, func(t *testing.T, wd string) {
		err := os.MkdirAll(".dotfiles/hooks", os.FileMode(0755))
		tests.AssertNoError(t, err)

		hooksDir := path.Join(wd, ".dotfiles/hooks")

		var buf bytes.Buffer
		err = ensureExampleSimpleStructure(false, wd, hooksDir, &buf)
		tests.AssertNoError(t, err)

		tests.AssertFileExists(t, ".bashrc")
		tests.AssertFileExists(t, ".profile")

		tests.AssertFileExists(t, path.Join(hooksDir, "pre-link"))
		tests.AssertFileExists(t, path.Join(hooksDir, "post-link"))

		// TODO: assert buf
	})
}

// TODO: test dryRun
func TestEnsureExampleExpertStructure(t *testing.T) {
	tests.RunInTmp(t, func(t *testing.T, wd string) {
		err := os.MkdirAll(".dotfiles/hooks", os.FileMode(0755))
		tests.AssertNoError(t, err)

		hooksDir := path.Join(wd, ".dotfiles/hooks")

		var buf bytes.Buffer
		err = ensureExampleExpertStructure(false, wd, hooksDir, &buf)
		tests.AssertNoError(t, err)

		dirs := []string{
			".dotfiles/hooks/ssh",
			".dotfiles/hooks/vim",
			"bash",
			"git",
			"ignore",
			"nano",
			"ssh",
			"ssh/.ssh",
			"tmux",
			"vim",
		}

		for _, dir := range dirs {
			tests.AssertDirExists(t, dir)
		}

		files := []string{
			".dotfilescollections",
			".dotfilesignore",
			".dotfiles/hooks/ssh/post-link",
			".dotfiles/hooks/vim/post-link",
			"bash/.bashrc",
			"bash/.profile",
			"git/.gitconfig",
			"ignore/foobar.txt",
			"nano/.nanorc",
			"ssh/.ssh/config",
			"tmux/.tmux.conf",
			"vim/.vimrc",
		}

		for _, file := range files {
			tests.AssertFileExists(t, file)
		}

		// TODO: assert buf
	})
}

// TODO: test dryRun
func TestEnsureFile(t *testing.T) {
	tests.RunInTmp(t, func(t *testing.T, wd string) {
		file := File{
			Name:    "foobar.txt",
			Content: []byte("foobar"),
		}
		var buf bytes.Buffer
		err := ensureFile(false, wd, file, false, &buf)
		tests.AssertNoError(t, err)

		tests.AssertFileExists(t, file.Name)

		buf.Truncate(0)

		err = ensureFile(false, wd, file, false, &buf)
		tests.AssertNoError(t, err)

		tests.AssertFileExists(t, file.Name)
		assert.Equal(t, "", buf.String())

		// TODO: assert buf
	})
}

// TODO: test dryRun
func TestEnsureFiles(t *testing.T) {
	tests.RunInTmp(t, func(t *testing.T, wd string) {
		files := map[string][]File{
			"foo": {
				File{Name: "bar", Content: []byte("")},
				File{Name: "baz", Content: []byte("")},
			},
			"bar": {
				File{Name: "alice", Content: []byte("")},
				File{Name: "bob", Content: []byte("")},
			},
		}

		var buf bytes.Buffer
		err := ensureFiles(false, wd, files, false, &buf)
		tests.AssertNoError(t, err)

		tests.AssertDirExists(t, "foo")
		tests.AssertDirExists(t, "bar")

		for _, file := range []string{"foo/bar", "foo/baz", "bar/alice", "bar/bob"} {
			tests.AssertFileExists(t, file)
		}

		// TODO: assert buf
	})
}

// TODO: test dryRun
func TestEnsureDir(t *testing.T) {
	tests.RunInTmp(t, func(t *testing.T, wd string) {
		var buf bytes.Buffer
		err := ensureDir(false, wd, "foobar", &buf)
		tests.AssertNoError(t, err)

		tests.AssertDirExists(t, "foobar")

		buf.Truncate(0)

		err = ensureDir(false, wd, "foobar", &buf)

		tests.AssertDirExists(t, "foobar")
		assert.Equal(t, "", buf.String())

		// TODO: assert buf
	})
}

// TODO: test dryRun
func TestInitHooks(t *testing.T) {
	tests.RunInTmp(t, func(t *testing.T, wd string) {
		preHookFile := File{
			Name:    "pre-hook",
			Content: []byte("pre-hook"),
		}
		postHookFile := File{
			Name:    "post-hook",
			Content: []byte("post-hook"),
		}
		var buf bytes.Buffer
		err := initHooks(false, wd, preHookFile, postHookFile, &buf)
		tests.AssertNoError(t, err)

		tests.AssertFileExists(t, preHookFile.Name)
		tests.AssertFileExists(t, postHookFile.Name)

		// TODO: assert buf
	})
}
