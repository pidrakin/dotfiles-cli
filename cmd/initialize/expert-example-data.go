package initialize

import _ "embed"

//go:embed data/example/expert/.dotfilescollections
var expertExampleDotfilesCollectionsYAML []byte

//go:embed data/example/expert/.dotfilesignore
var expertExampleDotfilesIgnoreYAML []byte

//go:embed data/example/expert/vim/.vimrc
var expertExampleVIMRC []byte

//go:embed data/example/expert/nano/.nanorc
var expertExampleNANORC []byte

//go:embed data/example/expert/tmux/.tmux.conf
var expertExampleTMUXCONF []byte

//go:embed data/example/expert/vim/post-link
var expertExamplePostLinkVIM []byte

//go:embed data/example/expert/bash/.bashrc
var expertExampleBASHRC []byte

//go:embed data/example/expert/bash/.profile
var expertExamplePROFILE []byte

//go:embed data/example/expert/git/.gitconfig
var expertExampleGITCONFIG []byte

//go:embed data/example/expert/ssh/config
var expertExampleSSHCONFIG []byte

//go:embed data/example/expert/ssh/post-link
var expertExamplePostLinkSSH []byte

//go:embed data/example/expert/ignore/foobar.txt
var expertExampleFoobarTXT []byte

type ExpertExampleData struct {
	DotfilesCollectionsYAML File
	DotfilesIgnoreYAML      File
	PostLinkHook            File
	PreLinkHook             File
	BASHRC                  File
	PROFILE                 File
	VIMRC                   File
	PostLinkVIM             File
	NANORC                  File
	TMUXCONF                File
	GITCONFIG               File
	SSHCONFIG               File
	PostLinkSSH             File
	FoobarTXT               File
}

var expertData = ExpertExampleData{
	DotfilesCollectionsYAML: File{
		Name:    ".dotfilescollections",
		Content: expertExampleDotfilesCollectionsYAML,
	},
	DotfilesIgnoreYAML: File{
		Name:    ".dotfilesignore",
		Content: expertExampleDotfilesIgnoreYAML,
	},
	PreLinkHook: File{
		Name:    "pre-link",
		Content: preLinkHookFile,
	},
	PostLinkHook: File{
		Name:    "post-link",
		Content: simpleExamplePostLinkHookFile,
	},
	BASHRC: File{
		Name:    ".bashrc",
		Content: expertExampleBASHRC,
	},
	PROFILE: File{
		Name:    ".profile",
		Content: expertExamplePROFILE,
	},
	VIMRC: File{
		Name:    ".vimrc",
		Content: expertExampleVIMRC,
	},
	NANORC: File{
		Name:    ".nanorc",
		Content: expertExampleNANORC,
	},
	TMUXCONF: File{
		Name:    ".tmux.conf",
		Content: expertExampleTMUXCONF,
	},
	PostLinkVIM: File{
		Name:    "post-link",
		Content: expertExamplePostLinkVIM,
	},
	GITCONFIG: File{
		Name:    ".gitconfig",
		Content: expertExampleGITCONFIG,
	},
	SSHCONFIG: File{
		Name:    "config",
		Content: expertExampleSSHCONFIG,
	},
	PostLinkSSH: File{
		Name:    "post-link",
		Content: expertExamplePostLinkSSH,
	},
	FoobarTXT: File{
		Name:    "foobar.txt",
		Content: expertExampleFoobarTXT,
	},
}
