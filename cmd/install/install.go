package install

import (
	"fmt"
	"github.com/fatih/color"
	"gitlab.com/pidrakin/dotfiles-cli/cmd/common"
	"gitlab.com/pidrakin/dotfiles-cli/common/logging"
	"gitlab.com/pidrakin/go/interactive"
	"gitlab.com/pidrakin/go/slices"
	sysfs2 "gitlab.com/pidrakin/go/sysfs"
	"io"
	"os"
	"strings"

	log "github.com/sirupsen/logrus"
	"gitlab.com/pidrakin/dotfiles-cli/config"
	"gitlab.com/pidrakin/dotfiles-cli/git"
	"gitlab.com/pidrakin/dotfiles-cli/text"
)

func Run(global bool, interactive bool, source string) error {
	return run(global, interactive, source, nil, nil, nil, os.Exit)
}

func run(global bool, interactive bool, source string, reader io.Reader, writer io.Writer, errWriter io.Writer, exitFunc func(int)) error {
	cfgMgr, err := config.NewManager()
	if err != nil {
		return err
	}
	target := getInstallTarget(cfgMgr.Config, interactive, global, reader, writer)

	validateSource(cfgMgr, source, writer, errWriter, exitFunc)

	if source != "" {
		source, err = git.ParseUri(source)
		if err != nil {
			return err
		}
	}

	switched, err := updateContext(cfgMgr, source, target, writer)
	if err != nil {
		return err
	}

	commandType, err := determineCommandType(cfgMgr, switched)
	if err != nil {
		return err
	}

	if err = install(cfgMgr.StateConfig, source); err != nil {
		return err
	}

	if err = logging.Successfully(commandType); err != nil {
		return err
	}
	return outputStats()
}

func getInstallTarget(cfg *config.Config, interactive bool, global bool, reader io.Reader, writer io.Writer) string {
	defer logging.SetLogOutput(writer)()

	if res := promptInstallLocation(interactive, cfg, reader, writer); res != nil {
		global = *res
	}

	target := cfg.LocalInstallDir()
	if global {
		target = cfg.GlobalInstallDir()
	}
	log.WithField("target", target).Info(text.Install)
	return target
}

func determineCommandType(cfgMgr *config.Manager, switched bool) (string, error) {
	var suffix string
	if switched {
		suffix = " and switched context"
	}

	if sysfs2.DirExists(cfgMgr.StateConfig.GetContextPath()) {
		isRepo, err := git.IsRepo(cfgMgr.StateConfig.GetContextPath())
		if err != nil {
			return "", err
		}

		if isRepo {
			return "updated" + suffix, nil
		}
	}
	return "installed" + suffix, nil
}

func promptInstallLocation(interactiveFlag bool, cfg *config.Config, reader io.Reader, writer io.Writer) *bool {
	options := []string{
		fmt.Sprintf("local  %s", cfg.LocalInstallDir()),
		fmt.Sprintf("global %s", cfg.GlobalInstallDir()),
	}

	prompter, err := interactive.NewPrompter(
		interactive.SetInteractive(interactiveFlag),
		interactive.SetReader(reader),
		interactive.SetWriter(writer),
	)
	if err != nil {
		return nil
	}
	i, _, err := prompter.SingleChoice(
		options,
		"Where do you want to install your dotfiles?",
	)

	if err != nil || i == -1 {
		return nil
	}
	global := strings.HasPrefix(options[i], "global")
	return &global
}

func isInstalled(cfgMgr *config.Manager) bool {
	return cfgMgr.StateConfig != nil
}

func validateSource(cfgMgr *config.Manager, source string, writer io.Writer, errWriter io.Writer, exitFunc func(int)) {
	defer logging.SetOutputWriter(writer)()
	defer logging.SetOutputExitFunc(exitFunc)()
	defer logging.SetLogOutput(errWriter)()

	if source == "" && !isInstalled(cfgMgr) {
		_ = logging.Failed(text.NothingToInstall)
	}
}

func updateContext(cfgMgr *config.Manager, source string, target string, errWriter io.Writer) (bool, error) {
	defer logging.SetLogOutput(errWriter)()
	if source == "" {
		return false, nil
	}

	// TODO: CreateCommand switchContext function with interactive
	stateConfig, switched, err := config.UpdateStateConfig(errWriter, cfgMgr, source, target)
	if err != nil {
		return false, err
	}
	if switched {
		log.WithFields(log.Fields{
			"from": cfgMgr.StateConfig.Source,
			"to":   stateConfig.Source,
		}).Info(text.TrySwitchContext)
	}
	cfgMgr.StateConfig = stateConfig

	return switched, nil
}

func install(stateCfg *config.StateConfig, source string) error {
	target := stateCfg.GetContextPath()
	linkTarget, err := sysfs2.ReadLink(target)
	if err == nil && linkTarget != "" {
		target = linkTarget
	}

	check, err := git.IsRepo(target)

	var remote string
	if check {
		remote, err = git.Pull(target)
	} else {
		remote, err = git.Clone(source, target)
	}
	if err != nil {
		return logging.Failed("install: %s", err)
	}
	stateCfg.Remote = remote

	return stateCfg.Save()
}

func outputStats() error {
	var err error
	var diff bool
	differ := common.Differ{}
	if err = differ.Init(); err != nil {
		return err
	}
	stateConfig := differ.ConfigManager.StateConfig
	if stateConfig.Linked == nil {
		diff = true
	} else {
		collectionsConfig := differ.ConfigManager.CollectionsConfig
		var diffs []common.Diff
		if collectionsConfig != nil && len(collectionsConfig.Collections) > 0 {
			if stateConfig.Collection == "" {
				diff = true
			} else {
				if diffs, err = differ.Diff(); err != nil {
					return err
				}
			}
		} else {
			if diffs, err = differ.Diff(); err != nil {
				return err
			}
		}
		someDiffs := slices.Map(diffs, func(d common.Diff) bool {
			return d.ToAdd != nil || d.ToDelete != nil || d.ToModify != nil
		})
		diff = slices.Contains(someDiffs, true)
	}

	if diff {
		colored := color.New(color.FgRed, color.Bold).Sprintf("diff")
		_, err = fmt.Fprintf(os.Stdout, "%s available\nplease run dotfiles link\n", colored)
	}
	return err
}
