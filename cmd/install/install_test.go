package install

import (
	"bytes"
	"crypto/md5"
	"encoding/hex"
	"gitlab.com/pidrakin/go/interactive"
	"gitlab.com/pidrakin/go/tests"
	"os"
	"path"
	"path/filepath"
	"strings"
	"testing"
	"time"

	"github.com/go-git/go-git/v5"
	"github.com/go-git/go-git/v5/plumbing/object"
	"github.com/mitchellh/go-homedir"
	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"gitlab.com/pidrakin/dotfiles-cli/cmd/initialize"
	"gitlab.com/pidrakin/dotfiles-cli/config"
)

func init() {
	homedir.DisableCache = true
}

func Test_PromptInstallLocation(t *testing.T) {
	interactive.Debug = true
	cfg := config.NewConfig()
	reader := strings.NewReader("1\n")
	var buf bytes.Buffer
	global := promptInstallLocation(true, cfg, reader, &buf)
	assert.NotNil(t, global)
	assert.True(t, *global)

	reader = strings.NewReader("0\n")
	global = promptInstallLocation(true, cfg, reader, &buf)
	assert.NotNil(t, global)
	assert.False(t, *global)
}

func Test_IsInstalled(t *testing.T) {
	tests.RunInTmp(t, func(t *testing.T, wd string) {
		err := os.Setenv("HOME", wd)
		tests.AssertNoError(t, err)

		cfgMgr, err := config.NewManager()
		tests.AssertNoError(t, err)

		installed := isInstalled(cfgMgr)
		assert.False(t, installed)

		cfgMgr.StateConfig = &config.StateConfig{}
		installed = isInstalled(cfgMgr)
		assert.True(t, installed)
	})
}

func Test_ValidateSource(t *testing.T) {
	tests.RunInTmp(t, func(t *testing.T, wd string) {
		err := os.Setenv("HOME", wd)
		tests.AssertNoError(t, err)

		cfgMgr, err := config.NewManager()
		tests.AssertNoError(t, err)

		t.Run("fails", func(t *testing.T) {
			var outBuf bytes.Buffer
			var buf bytes.Buffer
			var exit *int
			validateSource(cfgMgr, "", &outBuf, &buf, func(i int) {
				exit = &i
			})
			assert.Contains(t, buf.String(), `Nothing to install`)
			assert.NotNil(t, exit)
			assert.Equal(t, 1, *exit)
		})
		t.Run("succeeds", func(t *testing.T) {
			var buf bytes.Buffer
			var exit *int
			validateSource(cfgMgr, "foobar", nil, &buf, func(i int) {
				exit = &i
			})
			assert.Empty(t, buf.String())
			assert.Nil(t, exit)
		})
	})
}

func Test_UpdateContext(t *testing.T) {
	t.Run("create first context", func(t *testing.T) {
		tests.RunInTmp(t, func(t *testing.T, wd string) {
			err := os.Setenv("HOME", wd)
			tests.AssertNoError(t, err)

			cfgMgr, err := config.NewManager()
			tests.AssertNoError(t, err)

			var buf bytes.Buffer
			switched, err := updateContext(cfgMgr, "foobar", "bazbar", &buf)
			tests.AssertNoError(t, err)
			assert.False(t, switched)
			assert.NotNil(t, cfgMgr.StateConfig)
			assert.Equal(t, "3858f62230ac3c915f300c664312c63f", cfgMgr.StateConfig.Name)
			assert.Equal(t, "foobar", cfgMgr.StateConfig.Source)
			assert.Equal(t, "bazbar", cfgMgr.StateConfig.InstallLocation)
			assert.Equal(t, 1, len(cfgMgr.StateConfigs))
			assert.Empty(t, buf.String())
		})
	})
	t.Run("switch to new context", func(t *testing.T) {
		tests.RunInTmp(t, func(t *testing.T, wd string) {
			err := os.Setenv("HOME", wd)
			tests.AssertNoError(t, err)

			cfgMgr, err := config.NewManager()
			tests.AssertNoError(t, err)

			dir := filepath.Join(wd, ".local/share/dotfiles/")
			err = os.MkdirAll(dir, os.FileMode(0755))

			cfgMgr.StateConfig = &config.StateConfig{
				Name:            "3858f62230ac3c915f300c664312c63d",
				InstallLocation: dir,
			}

			tests.AssertNoError(t, err)
			err = os.WriteFile(filepath.Join(dir, "3858f62230ac3c915f300c664312c63d.yaml"), []byte{}, os.FileMode(0644))
			tests.AssertNoError(t, err)

			var buf bytes.Buffer
			switched, err := updateContext(cfgMgr, "foobar", "bazbar", &buf)
			tests.AssertNoError(t, err)
			assert.True(t, switched)
			assert.NotNil(t, cfgMgr.StateConfig)
			assert.Equal(t, "3858f62230ac3c915f300c664312c63f", cfgMgr.StateConfig.Name)
			assert.Equal(t, "foobar", cfgMgr.StateConfig.Source)
			assert.Equal(t, "bazbar", cfgMgr.StateConfig.InstallLocation)
			assert.Contains(t, buf.String(), "trying to switch context")
		})
	})
}

var (
	repoPath    = "repository"
	repoPath2   = "repository2"
	clonedRepo  = "clone"
	clonedRepo2 = "clone2"
	homeDir     = "home/user"
)

func setupRepo(t *testing.T, wd string, path string, cloned string) {
	log.SetLevel(log.FatalLevel)

	repo := filepath.Join(wd, path)
	err := os.MkdirAll(repo, os.FileMode(0755))
	tests.AssertNoError(t, err)

	err = os.Chdir(filepath.Join(wd, path))
	tests.AssertNoError(t, err)

	err = initialize.Run(true, true)
	tests.AssertNoError(t, err)

	_, err = git.PlainInit(repo, false)
	tests.AssertNoError(t, err)

	r, err := git.PlainOpen(repo)
	tests.AssertNoError(t, err)

	w, err := r.Worktree()
	tests.AssertNoError(t, err)

	_, err = w.Add(".")
	tests.AssertNoError(t, err)

	status, err := w.Status()
	tests.AssertNoError(t, err)
	assert.NotEmpty(t, status)

	commit, err := w.Commit("example go-git commit", &git.CommitOptions{
		Author: &object.Signature{
			Name:  "John Doe",
			Email: "john@doe.org",
			When:  time.Now(),
		},
	})
	tests.AssertNoError(t, err)

	obj, err := r.CommitObject(commit)
	tests.AssertNoError(t, err)
	assert.NotEmpty(t, obj)

	err = os.Chdir(wd)
	tests.AssertNoError(t, err)

	var buf bytes.Buffer
	_, err = git.PlainClone(cloned, false, &git.CloneOptions{
		URL:      path,
		Progress: &buf,
	})
	tests.AssertNoError(t, err)
	assert.NotEmpty(t, buf.String())
}

func installRepo(t *testing.T, wd string, path string, update bool) {
	err := os.Setenv("HOME", filepath.Join(wd, homeDir))
	tests.AssertNoError(t, err)

	var abspath string
	if !update {
		abspath = filepath.Join(wd, path)
	}

	reader := strings.NewReader("")
	var buf bytes.Buffer
	var errBuf bytes.Buffer
	var exitCode *int
	err = run(
		false,
		false,
		abspath,
		reader,
		&buf,
		&errBuf,
		func(i int) {
			*exitCode = i
		},
	)
	tests.AssertNoError(t, err)
	assert.Empty(t, buf.String())
	assert.Empty(t, errBuf.String())
	assert.Nil(t, exitCode)
}

func check(t *testing.T, wd string, dir string) {
	dotfilesDir := path.Join(wd, homeDir, ".local/share/dotfiles")
	tests.AssertDirExists(t, dotfilesDir)
	sum := md5.Sum([]byte(path.Join(wd, dir)))
	context := hex.EncodeToString(sum[:])
	installDir := filepath.Join(dotfilesDir, context)
	tests.AssertFileExists(t, installDir)
	tests.AssertFileExists(t, filepath.Join(dotfilesDir, "dotfiles.yaml"))

	//tests.AssertDirExists(t, installDir)
	//
	//dirs := []string{
	//	"bash",
	//	"git",
	//	"ignore",
	//	"nano",
	//	"ssh",
	//	"ssh/.ssh",
	//	"tmux",
	//	"vim",
	//}
	//
	//for _, dir = range dirs {
	//	tests.AssertDirExists(t, path.Join(installDir, dir))
	//}
	//
	//files := []string{
	//	".dotfilesignore",
	//	"bash/.bashrc",
	//	"bash/.profile",
	//	"git/.gitconfig",
	//	"ignore/foobar.txt",
	//	"nano/.nanorc",
	//	"nano/.nanorc",
	//	"ssh/.ssh/config",
	//	".dotfiles/hooks/ssh/post-link",
	//	"tmux/.tmux.conf",
	//	"vim/.vimrc",
	//	".dotfiles/hooks/vim/post-link",
	//}
	//
	//for _, file := range files {
	//	tests.AssertFileExists(t, path.Join(installDir, file))
	//}
}

func TestInstaller_Install(t *testing.T) {
	tests.RunInTmp(t, func(t *testing.T, wd string) {
		setupRepo(t, wd, repoPath, clonedRepo)
		installRepo(t, wd, clonedRepo, false)
		check(t, wd, clonedRepo)
	})
}

func TestInstaller_Install_Pull(t *testing.T) {
	tests.RunInTmp(t, func(t *testing.T, wd string) {
		setupRepo(t, wd, repoPath, clonedRepo)
		installRepo(t, wd, clonedRepo, false)
		installRepo(t, wd, clonedRepo, true)
		check(t, wd, clonedRepo)
	})
}

func TestInstaller_Install_Change_Context(t *testing.T) {
	tests.RunInTmp(t, func(t *testing.T, wd string) {
		setupRepo(t, wd, repoPath, clonedRepo)
		installRepo(t, wd, clonedRepo, false)
		setupRepo(t, wd, repoPath2, clonedRepo2)
		installRepo(t, wd, clonedRepo2, false)
		check(t, wd, clonedRepo2)
	})
}
