package link

import (
	log "github.com/sirupsen/logrus"
	"gitlab.com/pidrakin/dotfiles-cli/exec"
	"gitlab.com/pidrakin/dotfiles-cli/text"
	"gitlab.com/pidrakin/go/sysfs"
	"os"
	exec2 "os/exec"
	"path"
)

type HookType int

func (h HookType) String() string {
	return [...]string{"pre-link", "post-link"}[h-1]
}

type HookEnum struct {
	PRE  HookType
	POST HookType
}

var hook = HookEnum{
	PRE:  1,
	POST: 2,
}

func runHook(basepath string, hooktype HookType, collection string) (found bool, succeeded bool, err error) {
	var hookpath string
	found, hookpath = findHook(basepath, hooktype, collection)
	if err != nil {
		return
	}
	if found {
		succeeded = true
		if err = executeHook(hookpath); err != nil {
			if _, ok := err.(*exec2.ExitError); ok {
				succeeded = false
				err = nil
			}
		}
	}
	return
}

func isExecutable(mode os.FileMode) bool {
	return mode&0111 != 0
}

func findHook(basepath string, hooktype HookType, collection string) (found bool, hookpath string) {
	dir := path.Join(basepath, ".dotfiles/hooks")
	if collection != "" {
		dir = path.Join(dir, collection)
	}
	hookpath = path.Join(dir, hooktype.String())

	if info, osErr := os.Stat(hookpath); !os.IsNotExist(osErr) {
		found = true
		if !isExecutable(info.Mode()) {
			log.WithField("hook", hookpath).Debug(text.HookNotExecutable)
		}
		return
	}
	return
}

func executeHook(hook string) error {
	log.WithField("hook", hook).Info(text.ExecuteHook)
	if !sysfs.DryRun {
		out, err := exec.Command(hook)
		if err != nil {
			log.WithField("hook", hook).Errorf("hook failed\n%s", out)
			if _, ok := err.(*exec2.ExitError); ok {
				return err
			}
		} else {
			log.WithField("hook", hook).Debugf("hook succeeded\n%s", out)
		}
	}
	return nil
}
