package link

import (
	"bytes"
	"crypto/md5"
	"encoding/hex"
	"os"
	"path"
	"path/filepath"
	"testing"
	"time"

	"github.com/go-git/go-git/v5"
	"github.com/go-git/go-git/v5/plumbing/object"
	"github.com/mitchellh/go-homedir"
	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"gitlab.com/pidrakin/dotfiles-cli/cmd/initialize"
	"gitlab.com/pidrakin/dotfiles-cli/cmd/install"
	"gitlab.com/pidrakin/go/sysfs"
	"gitlab.com/pidrakin/go/tests"
)

var (
	repoPath         = "repository"
	repoExpertPath   = "repository-expert"
	clonedRepo       = "clone"
	clonedExpertRepo = "clone-expert"
	homeDir          = "home/user"
)

func init() {
	homedir.DisableCache = true
}

func setupRepo(t *testing.T, wd string, path string, cloned string, expert bool) {
	log.SetLevel(log.FatalLevel)

	repo := filepath.Join(wd, path)
	err := os.MkdirAll(repo, os.FileMode(0755))
	tests.AssertNoError(t, err)

	err = os.Chdir(filepath.Join(wd, path))
	tests.AssertNoError(t, err)

	err = initialize.Run(expert, true)
	tests.AssertNoError(t, err)

	_, err = git.PlainInit(repo, false)
	tests.AssertNoError(t, err)

	r, err := git.PlainOpen(repo)
	tests.AssertNoError(t, err)

	w, err := r.Worktree()
	tests.AssertNoError(t, err)

	_, err = w.Add(".")
	tests.AssertNoError(t, err)

	status, err := w.Status()
	tests.AssertNoError(t, err)
	assert.NotEmpty(t, status)

	commit, err := w.Commit("example go-git commit", &git.CommitOptions{
		Author: &object.Signature{
			Name:  "John Doe",
			Email: "john@doe.org",
			When:  time.Now(),
		},
	})
	tests.AssertNoError(t, err)

	obj, err := r.CommitObject(commit)
	tests.AssertNoError(t, err)
	assert.NotEmpty(t, obj)

	err = os.Chdir(wd)
	tests.AssertNoError(t, err)

	var buf bytes.Buffer
	_, err = git.PlainClone(cloned, false, &git.CloneOptions{
		URL:      path,
		Progress: &buf,
	})
	tests.AssertNoError(t, err)
	assert.NotEmpty(t, buf.String())
}

func installRepo(t *testing.T, wd string, path string) {
	err := os.Setenv("HOME", filepath.Join(wd, homeDir))
	tests.AssertNoError(t, err)

	err = install.Run(false, false, filepath.Join(wd, path))
	tests.AssertNoError(t, err)
}

func linkRepo(t *testing.T, collection string) {
	runHooks := true
	err := Run(false, false, false, &runHooks, collection)
	tests.AssertNoError(t, err)
}

func TestLinker_LinkSimple(t *testing.T) {
	tests.RunInTmp(t, func(t *testing.T, wd string) {
		setupRepo(t, wd, repoPath, clonedRepo, false)
		installRepo(t, wd, clonedRepo)
		linkRepo(t, "")

		files := []string{
			".bashrc",
			".profile",
		}

		for _, entry := range files {
			target := path.Join(wd, homeDir, entry)
			tests.AssertFileExists(t, target)
			source, err := sysfs.ReadLink(target)
			tests.AssertNoError(t, err)

			dotfilesDir := path.Join(wd, homeDir, ".local/share/dotfiles")
			tests.AssertDirExists(t, dotfilesDir)
			sum := md5.Sum([]byte(path.Join(wd, clonedRepo)))
			context := hex.EncodeToString(sum[:])
			installDir := filepath.Join(dotfilesDir, context)

			expected := path.Join(installDir, entry)
			assert.Equalf(t, expected, source, "expected link [%s] to point to [%s] but got [%s]", target, expected, source)
		}
	})
}

func TestLinker_LinkExpert(t *testing.T) {
	tests.RunInTmp(t, func(t *testing.T, wd string) {
		setupRepo(t, wd, repoPath, clonedRepo, true)
		installRepo(t, wd, clonedRepo)
		linkRepo(t, "mac")

		files := [][]string{
			{"vim", ".vimrc"},
			{"tmux", ".tmux.conf"},
		}

		for _, entry := range files {
			target := path.Join(wd, homeDir, entry[1])
			tests.AssertFileExists(t, target)
			source, err := sysfs.ReadLink(target)
			tests.AssertNoError(t, err)

			dotfilesDir := path.Join(wd, homeDir, ".local/share/dotfiles")
			tests.AssertDirExists(t, dotfilesDir)
			sum := md5.Sum([]byte(path.Join(wd, clonedRepo)))
			context := hex.EncodeToString(sum[:])
			installDir := filepath.Join(dotfilesDir, context)

			expected := path.Join(installDir, entry[0], entry[1])
			assert.Equalf(t, expected, source, "expected link [%s] to point to [%s] but got [%s]", target, expected, source)
		}
	})
}

func TestLinker_ReLinkSimple(t *testing.T) {
	tests.RunInTmp(t, func(t *testing.T, wd string) {
		setupRepo(t, wd, repoPath, clonedRepo, false)
		installRepo(t, wd, clonedRepo)
		linkRepo(t, "")
		linkRepo(t, "")

		files := []string{
			".bashrc",
			".profile",
		}

		for _, entry := range files {
			target := path.Join(wd, homeDir, entry)
			tests.AssertFileExists(t, target)
			source, err := sysfs.ReadLink(target)
			tests.AssertNoError(t, err)

			dotfilesDir := path.Join(wd, homeDir, ".local/share/dotfiles")
			tests.AssertDirExists(t, dotfilesDir)
			sum := md5.Sum([]byte(path.Join(wd, clonedRepo)))
			context := hex.EncodeToString(sum[:])
			installDir := filepath.Join(dotfilesDir, context)

			expected := path.Join(installDir, entry)
			assert.Equalf(t, expected, source, "expected link [%s] to point to [%s] but got [%s]", target, expected, source)
		}
	})
}

func TestLinker_ReLinkExpert(t *testing.T) {
	tests.RunInTmp(t, func(t *testing.T, wd string) {
		setupRepo(t, wd, repoPath, clonedRepo, true)
		installRepo(t, wd, clonedRepo)
		linkRepo(t, "mac")
		linkRepo(t, "mac")

		files := [][]string{
			{"vim", ".vimrc"},
			{"tmux", ".tmux.conf"},
		}

		for _, entry := range files {
			target := path.Join(wd, homeDir, entry[1])
			tests.AssertFileExists(t, target)
			source, err := sysfs.ReadLink(target)
			tests.AssertNoError(t, err)

			dotfilesDir := path.Join(wd, homeDir, ".local/share/dotfiles")
			tests.AssertDirExists(t, dotfilesDir)
			sum := md5.Sum([]byte(path.Join(wd, clonedRepo)))
			context := hex.EncodeToString(sum[:])
			installDir := filepath.Join(dotfilesDir, context)

			expected := path.Join(installDir, entry[0], entry[1])
			assert.Equalf(t, expected, source, "expected link [%s] to point to [%s] but got [%s]", target, expected, source)
		}
	})
}

func TestLinker_ReLinkSimpleExpert(t *testing.T) {
	tests.RunInTmp(t, func(t *testing.T, wd string) {
		setupRepo(t, wd, repoPath, clonedRepo, false)
		installRepo(t, wd, clonedRepo)
		linkRepo(t, "")
		setupRepo(t, wd, repoExpertPath, clonedExpertRepo, true)
		installRepo(t, wd, clonedExpertRepo)
		linkRepo(t, "mac")

		dotfilesDir := path.Join(wd, homeDir, ".local/share/dotfiles")
		tests.AssertDirExists(t, dotfilesDir)

		simpleSum := md5.Sum([]byte(path.Join(wd, clonedRepo)))
		simpleContext := hex.EncodeToString(simpleSum[:])
		simpleInstallDir := filepath.Join(dotfilesDir, simpleContext)

		expertSum := md5.Sum([]byte(path.Join(wd, clonedExpertRepo)))
		expertContext := hex.EncodeToString(expertSum[:])
		expertInstallDir := filepath.Join(dotfilesDir, expertContext)

		files := [][]string{
			{expertInstallDir, "vim", ".vimrc"},
			{expertInstallDir, "tmux", ".tmux.conf"},
			{simpleInstallDir, ".bashrc"},
			{simpleInstallDir, ".profile"},
		}

		for _, entry := range files {
			target := path.Join(wd, homeDir, entry[len(entry)-1])
			tests.AssertFileExists(t, target)
			source, err := sysfs.ReadLink(target)
			tests.AssertNoError(t, err)

			expected := path.Join(entry...)
			assert.Equalf(t, expected, source, "expected link [%s] to point to [%s] but got [%s]", target, expected, source)
		}
	})
}
