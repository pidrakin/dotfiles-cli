package link

import (
	"fmt"
	"os"
	"path"
	"path/filepath"
	"strings"
	"time"

	"github.com/fatih/color"
	"github.com/mitchellh/go-homedir"
	log "github.com/sirupsen/logrus"
	"gitlab.com/pidrakin/dotfiles-cli/cmd/common"
	"gitlab.com/pidrakin/dotfiles-cli/common/logging"
	"gitlab.com/pidrakin/dotfiles-cli/config"
	"gitlab.com/pidrakin/dotfiles-cli/text"
	"gitlab.com/pidrakin/go/interactive"
	"gitlab.com/pidrakin/go/slices"
	sysfs2 "gitlab.com/pidrakin/go/sysfs"
)

type stats struct {
	added          int
	deleted        int
	ignored        int
	modified       int
	backedUp       int
	createdDirs    int
	hooksSucceeded int
	hooksFailed    int
}

type Linker struct {
	diffs         []common.Diff
	configManager *config.Manager
	stateConfig   *config.StateConfig
	collection    string
	interactive   bool
	backup        bool
	force         bool
	runHooks      *bool
	stats         *stats
}

func Run(interactive bool, backup bool, force bool, runHooks *bool, collection string) error {
	l := Linker{
		interactive: interactive,
		backup:      backup,
		force:       force,
		collection:  collection,
		runHooks:    runHooks,
	}
	return l.Link()
}

func (linker *Linker) Link() (err error) {
	if err = linker.setup(); err != nil {
		return
	}
	if err = linker.handleHookDiffs(hook.PRE); err != nil {
		return
	}
	if err = linker.handleOldLinks(); err != nil {
		return
	}
	if err = linker.handleDiffs(); err != nil {
		return
	}
	if err = linker.handleHookDiffs(hook.POST); err != nil {
		return
	}
	if err = linker.stateConfig.Save(); err != nil {
		return
	}

	if err = logging.Successfully("linked"); err != nil {
		return
	}
	if err = linker.outputStats(); err != nil {
		return
	}

	return
}

func (linker *Linker) setup() (err error) {
	if err = linker.interactiveMethodPrompt(); err != nil {
		return
	}

	differ := common.Differ{}
	linker.diffs, err = differ.Diff(linker.collection)
	if err != nil {
		return
	}

	linker.configManager = differ.ConfigManager
	linker.stateConfig = differ.ConfigManager.StateConfig

	if linker.runHooks == nil {
		runHooks := linker.stateConfig.Linked == nil
		linker.runHooks = &runHooks
	}

	linker.stateConfig.Linked = config.NewLinked()

	linker.stats = &stats{}

	return
}

func (linker *Linker) interactiveMethodPrompt() error {

	prompter, err := interactive.NewPrompter(interactive.SetInteractive(linker.interactive))
	if err != nil {
		return err
	}

	if !prompter.Interactive() && linker.backup && linker.force {
		prompter.SetInteractive(true)
	}

	methodOptions := []string{
		fmt.Sprintf("skip   %s", "skip target if exists"),
		fmt.Sprintf("backup %s", "backup target if exists"),
		fmt.Sprintf("force  %s", "overwrite target even if exists"),
	}

	i, _, err := prompter.SingleChoice(
		methodOptions,
		"How do you want to handle already existing target files?",
	)

	if err != nil || i == -1 {
		return nil
	}
	linker.backup = methodOptions[i] == "backup"
	linker.force = methodOptions[i] == "force"

	return nil
}

func (linker *Linker) runHook(basepath string, hooktype HookType, collection string) error {
	found, succeeded, err := runHook(basepath, hooktype, collection)
	if found {
		if succeeded {
			linker.stats.hooksSucceeded += 1
		} else {
			linker.stats.hooksFailed += 1
		}
	}
	return err
}

func (linker *Linker) handleHookDiffs(hookType HookType) (err error) {
	if !*linker.runHooks {
		return
	}

	if _, err = fmt.Fprintf(os.Stdout, "running %s hooks...\n", hookType); err != nil {
		return
	}

	start := time.Now()

	basepath := strings.TrimSuffix(linker.diffs[0].Root, "/"+linker.diffs[0].Collection)

	if len(linker.diffs) > 0 {
		if err = linker.runHook(basepath, hookType, ""); err != nil {
			return
		}
		if linker.stateConfig.Collection != "" {
			if err = linker.runHook(basepath, hookType, linker.stateConfig.Collection); err != nil {
				return
			}
		}
	}

	for _, diff2 := range linker.diffs {
		if err = linker.handleHookDiff(hookType, basepath, diff2); err != nil {
			return
		}
	}

	elapsed := time.Since(start)
	elapsedString := color.New(color.FgGreen, color.Bold).Sprintf("%d", int(elapsed.Seconds()))
	if _, err = fmt.Fprintf(os.Stdout, "hooks took %s seconds\n\n", elapsedString); err != nil {
		return
	}

	return
}

func (linker *Linker) handleHookDiff(hookType HookType, basepath string, diff common.Diff) (err error) {
	if diff.Collection != "" {
		if err = linker.runHook(basepath, hookType, diff.Collection); err != nil {
			return
		}
	}
	return
}

func (linker *Linker) handleOldLinks() (err error) {
	var linkList []string
	homeDir, err := homedir.Dir()
	if err != nil {
		return
	}

	previousStateConfig := linker.configManager.PreviousStateConfig
	if previousStateConfig != nil && previousStateConfig.Linked != nil {
		if previousStateConfig.Linked.IsPackages() {
			for _, list := range previousStateConfig.Linked.Packages {
				for _, file := range list {
					linkPath := filepath.Join(homeDir, file)
					if _, err := os.Stat(linkPath); !os.IsNotExist(err) {
						linkList = append(linkList, file)
					}
				}
			}
		} else {
			for _, file := range previousStateConfig.Linked.Files {
				linkPath := filepath.Join(homeDir, file)
				if _, err := os.Stat(linkPath); !os.IsNotExist(err) {
					linkList = append(linkList, file)
				}
			}
		}
		return linker.deleteStaleLinks(homeDir, linkList)
	}
	return nil
}

func (linker *Linker) handleDiffs() (err error) {
	for _, diff2 := range linker.diffs {
		if err = linker.handleDiff(diff2); err != nil {
			return
		}
	}
	return
}

func (linker *Linker) handleDiff(diff common.Diff) (err error) {
	homeDir, err := homedir.Dir()
	if err != nil {
		return
	}

	linker.stats.ignored += len(diff.ToIgnore)

	if err = linker.deleteStaleLinks(homeDir, diff.ToDelete); err != nil {
		return
	}

	filesToAdd := append(diff.ToAdd, diff.ToModify...)
	if err = linker.link(diff.Root, homeDir, filesToAdd); err != nil {
		return
	}
	pkg, ioErr := filepath.Rel(linker.stateConfig.GetContextPath(), diff.Root)
	if ioErr != nil {
		return ioErr
	}
	if pkg == "." {
		pkg = ""
	}
	paths := append(diff.ToIgnore, filesToAdd...)
	if len(paths) > 0 {
		if err = linker.stateConfig.Linked.Set(paths, pkg); err != nil {
			return
		}
	}

	return
}

func (linker *Linker) deleteStaleLinks(linkRoot string, linkList []string) error {
	for _, linkPath := range linkList {
		if err := sysfs2.DeleteSymlink(linkRoot, linkPath); err != nil {
			return err
		}
		linker.stats.deleted += len(linkList)
	}
	return nil
}

func (linker *Linker) link(root string, linkRoot string, filesFound []string) (err error) {
	date := time.Now().Format(time.RFC3339)
	backupDir := path.Join(linkRoot, fmt.Sprintf("dotfiles-backup-%s", date))

	for _, rel := range filesFound {
		source := filepath.Join(root, rel)
		target := filepath.Join(linkRoot, rel)

		if sysfs2.FileExists(target) {
			linkTarget, _ := sysfs2.ReadLink(target)
			if linkTarget == source {
				return nil
			} else if linker.backup {
				log.WithField("backupDir", backupDir).Debug(text.EnsureFileExists)
				if !sysfs2.DryRun {
					if err = os.MkdirAll(backupDir, os.FileMode(0755)); err != nil {
						return err
					}
					if err = os.Rename(target, path.Join(backupDir, rel)); err != nil {
						return err
					}
				}
				log.WithField("file", target).Debug(text.MoveToBackup)
				linker.stats.backedUp += 1
			} else if linker.force {
				if err = sysfs2.DeleteSymlink(linkRoot, rel); err != nil {
					return err
				}
			}
			linker.stats.modified += 1
		} else {
			dir := filepath.Dir(target)
			if !sysfs2.DirExists(dir) {
				log.WithField("dir", dir).Debug(text.EnsureFileExists)
				if !sysfs2.DryRun {
					if err = os.MkdirAll(dir, os.FileMode(0755)); err != nil {
						return err
					}
				}
				linker.stats.createdDirs += 1
			}
		}
		if err = sysfs2.Symlink(source, target, nil); err != nil {
			return
		}
		linker.stats.added += 1
	}
	return nil
}

func (linker *Linker) outputStats() error {
	if linker.stateConfig.Collection != "" {
		if _, err := fmt.Fprintf(os.Stdout, "\ncollection [%s]\n", linker.stateConfig.Collection); err != nil {
			return err
		}
	}

	sums := []int{
		linker.stats.added,
		linker.stats.deleted,
		linker.stats.ignored,
		linker.stats.modified,
		linker.stats.backedUp,
		linker.stats.createdDirs,
		linker.stats.hooksSucceeded,
		linker.stats.hooksFailed,
	}

	if slices.Sum(sums) > 0 {
		if _, err := fmt.Fprintf(os.Stdout, "\n"); err != nil {
			return err
		}
	}

	if err := outputStat(sums[0], "added file links", color.FgGreen, color.Bold); err != nil {
		return err
	}
	if err := outputStat(sums[1], "deleted file link", color.FgRed, color.Bold); err != nil {
		return err
	}
	if err := outputStat(sums[2], "ignored file links", color.FgYellow, color.Bold); err != nil {
		return err
	}
	if err := outputStat(sums[3], "modified file links", color.FgBlue, color.Bold); err != nil {
		return err
	}
	if err := outputStat(sums[4], "backed upped file links", color.FgWhite, color.Bold); err != nil {
		return err
	}
	if err := outputStat(sums[5], "dirs created", color.FgGreen, color.Bold); err != nil {
		return err
	}
	if err := outputStat(sums[6], "hooks succeeded", color.FgGreen, color.Bold); err != nil {
		return err
	}
	if err := outputStat(sums[7], "hooks failed", color.FgRed, color.Bold); err != nil {
		return err
	}

	return nil
}

func outputStat(stat int, mod string, attrs ...color.Attribute) error {
	if stat == 0 {
		return nil
	}
	colored := color.New(attrs...).Sprintf("%d", stat)
	_, err := fmt.Fprintf(os.Stdout, "%s %s\n", colored, mod)
	return err
}
