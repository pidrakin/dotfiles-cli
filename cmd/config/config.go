package config

import (
	"fmt"
	"io"
	"os"
	"reflect"
	"strings"

	"github.com/fatih/color"
	"gitlab.com/pidrakin/dotfiles-cli/common/logging"
	"gitlab.com/pidrakin/dotfiles-cli/config"
	output2 "gitlab.com/pidrakin/go/output"
	"gitlab.com/pidrakin/go/slices"
)

type Type = int

//type OutputFormat = int

const (
	STATE Type = 0
	REPO  Type = 1
	//YAML     OutputFormat = 0
	//TEMPLATE OutputFormat = 1
)

type ViewConfig struct{}

type Set struct {
	Key   string
	Value string
}

type SetConfig struct{}

func (vc *ViewConfig) View(configType Type, outputFormat output2.FormatType, template ...string) error {
	return view(os.Stdout, nil, os.Exit, configType, outputFormat, template...)
}

func view(writer io.Writer, errWriter io.Writer, exitFunc func(int), configType Type, outputFormat output2.FormatType, template ...string) error {
	logging.SetOutputWriter(writer)
	logging.SetOutputExitFunc(exitFunc)
	logging.SetLogOutput(errWriter)
	cfgMgr, err := config.NewManager()
	if err != nil {
		return err
	}
	if configType == STATE {
		stateConfig := cfgMgr.StateConfig
		if stateConfig == nil {
			if err = logging.Failed("nothing installed yet"); err != nil {
				return err
			}
		}

		outputType := struct {
			LogTarget string `yaml:"log_target"`
			Track     *bool  `yaml:"track,omitempty"`
			Once      *bool  `yaml:"once,omitempty"`
		}{
			LogTarget: stateConfig.LogTarget,
			Track:     stateConfig.Track,
			Once:      stateConfig.Once,
		}

		return output2.Format(writer, outputFormat, nil, template[0], outputType)
		//if outputFormat == YAML {
		//	return outputStateConfigYaml(writer, stateConfig)
		//} else {
		//	return outputStateConfigTemplate(writer, stateConfig, template[0])
		//}
	} else {
		cfg := cfgMgr.Config
		if cfg == nil {
			if err = logging.Failed("nothing installed yet"); err != nil {
				return err
			}
		}

		return output2.Format(writer, outputFormat, nil, template[0], cfg)
		//if outputFormat == YAML {
		//	return outputConfigYaml(writer, cfg)
		//} else {
		//	return outputConfigTemplate(writer, cfg, template[0])
		//}
	}
}

//func outputStateConfigYaml(writer io.Writer, stateConfig *config.StateConfig) error {
//	yamlType := struct {
//		LogTarget string `yaml:"log_target"`
//	}{
//		LogTarget: stateConfig.LogTarget,
//	}
//	yamlBytes, err := yaml.Marshal(yamlType)
//	if err != nil {
//		return err
//	}
//	_, err = writer.Write(yamlBytes)
//	return err
//}

//func outputConfigYaml(writer io.Writer, cfg *config.Config) error {
//	yamlBytes, err := yaml.Marshal(cfg)
//	if err != nil {
//		return err
//	}
//	_, err = writer.Write(yamlBytes)
//	return err
//}

//type templateType struct {
//	Key   string
//	Value interface{}
//}

//func outputStateConfigTemplate(writer io.Writer, stateConfig *config.StateConfig, template string) error {
//	o := []*templateType{
//		{
//			Key:   "log_target",
//			Value: stateConfig.LogTarget,
//		},
//	}
//	templateData := templates.TemplateData{
//		"Root": o,
//	}
//	template = fmt.Sprintf("{{- with .Root -}}%s{{- end -}}", template)
//	rendered := templates.Tprintf(template, templateData)
//	_, err := fmt.Fprintf(writer, rendered)
//	return err
//}

//func outputConfigTemplate(writer io.Writer, cfg *config.Config, template string) error {
//	o := []*templateType{
//		{
//			Key:   "local_install_dir",
//			Value: cfg.LocalInstallDir(),
//		},
//		{
//			Key:   "global_install_dir",
//			Value: cfg.GlobalInstallDir(),
//		},
//		{
//			Key:   "ignore",
//			Value: cfg.Ignore,
//		},
//		{
//			Key:   "ignore_file",
//			Value: cfg.IgnoreFile,
//		},
//		{
//			Key:   "hooks_dir",
//			Value: cfg.HooksDir,
//		},
//		{
//			Key:   "collections_config",
//			Value: cfg.CollectionsConfig,
//		},
//		{
//			Key:   "log_target",
//			Value: cfg.LogTarget,
//		},
//	}
//	templateData := templates.TemplateData{
//		"Root": o,
//	}
//	template = fmt.Sprintf("{{- with .Root -}}%s{{- end -}}", template)
//	rendered := templates.Tprintf(template, templateData)
//	_, err := fmt.Fprintf(writer, rendered)
//	return err
//}

func (sc *SetConfig) Set(configType Type, setters ...*Set) error {
	return set(configType, setters...)
}

func set(configType Type, setters ...*Set) error {
	cfgMgr, err := config.NewManager()
	if err != nil {
		return err
	}
	if configType == STATE {
		err = setStateConfig(cfgMgr.StateConfig, setters...)
	} else {
		err = setConfig(cfgMgr.Config, setters...)
	}
	if err != nil {
		return err
	}
	qty := "values"
	if len(setters) == 1 {
		qty = "value"
	}
	if err = logging.Successfully("set"); err != nil {
		return err
	}
	colored := color.New(color.FgGreen, color.Bold).Sprintf("%d", len(setters))
	_, err = fmt.Fprintf(os.Stdout, "\n%s %s set\n", colored, qty)
	return err
}

func setStateConfig(stateConfig *config.StateConfig, setters ...*Set) error {
	var err error
	if stateConfig == nil {
		if err = logging.Failed("nothing installed yet"); err != nil {
			return err
		}
	}

	for _, setter := range setters {
		switch setter.Key {
		case "log_target":
			stateConfig.LogTarget, err = setLogTarget(setter)
			if err != nil {
				return err
			}
		case "track":
			track := setter.Value == "true"
			stateConfig.Track = &track
		case "once":
			once := setter.Value == "true"
			stateConfig.Once = &once
		default:
			if err = logging.Failed("only [log_target, track, once] can be set"); err != nil {
				return err
			}
		}
	}
	return stateConfig.Save()
}

func setConfig(cfg *config.Config, setters ...*Set) error {
	var err error
	if cfg == nil {
		if err = logging.Failed("nothing installed yet"); err != nil {
			return err
		}
	}

	v := reflect.ValueOf(cfg)
	typeOf := v.Elem().Type()
	fields := slices.Map(slices.Range(typeOf.NumField()), func(i int) reflect.StructField {
		return typeOf.Field(i)
	})

	for _, setter := range setters {
		if setter.Key == "log_target" {
			cfg.LogTarget, err = setLogTarget(setter)
			if err != nil {
				return err
			}
		} else {
			field, ok := slices.Find(fields, func(f reflect.StructField) bool {
				name, ok := f.Tag.Lookup("yaml")
				return ok && strings.Contains(name, setter.Key)
			})
			if !ok || field.Type == nil {
				if err = logging.Failed("no property with name [%s] exists", setter.Key); err != nil {
					return err
				}
			}
			v.Elem().FieldByName(field.Name).SetString(setter.Value)
		}
	}
	return cfg.Save()
}

func setLogTarget(setter *Set) (string, error) {
	allowed := []string{"STDERR", "STDOUT", "FILE"}
	if !slices.Contains(allowed, setter.Value) {
		if err := logging.Failed("log_target only accepts [STDERR], [STDOUT] or [FILE]"); err != nil {
			return "", err
		}
	}
	return setter.Value, nil
}
