package install

import (
	"fmt"

	"github.com/spf13/cobra"
	gitUrls "github.com/whilp/git-urls"
	"gitlab.com/pidrakin/dotfiles-cli/cli/common"
	"gitlab.com/pidrakin/dotfiles-cli/cmd/install"
	"gitlab.com/pidrakin/dotfiles-cli/config"
	"gitlab.com/pidrakin/dotfiles-cli/text"
)

type Handler struct {
	command     *cobra.Command
	global      bool
	interactive bool
}

func (handler *Handler) Command() *cobra.Command {
	return handler.command
}

func NewHandler(use string, short string, long string) (handler Handler, err error) {
	handler.command = &cobra.Command{
		Use:     use,
		Short:   short,
		Long:    long,
		Aliases: []string{"update", "switch-context"},
		Args:    handler.validateArguments,
		RunE:    handler.run,
	}

	cfg := config.NewConfig()

	helper := common.UsageHelper{Cmd: handler.command}
	helper.AddOptArg("uri", "uri from which to install or update")
	if err = helper.Parse(); err != nil {
		return
	}

	handler.command.Flags().BoolVarP(&handler.interactive, "interactive", "i", false, "interactive mode")
	handler.command.Flags().BoolVarP(&handler.global, "global", "g", false, fmt.Sprintf("install dotfiles globally into %s", cfg.GlobalInstallDir()))

	handler.command.Flags().SortFlags = false

	return handler, nil
}

func (handler *Handler) validateArguments(cmd *cobra.Command, args []string) error {
	if err := cobra.MaximumNArgs(1)(cmd, args); err != nil {
		return err
	}
	if len(args) == 0 || handler.isValidUri(args[0]) {
		return nil
	}
	return fmt.Errorf(text.InvalidUri, args[0])
}

func (handler *Handler) run(command *cobra.Command, args []string) (err error) {
	var source string
	if len(args) == 1 {
		source = args[0]
	}

	return install.Run(handler.global, handler.interactive, source)
}

func (handler *Handler) isValidUri(uri string) bool {
	_, err := gitUrls.Parse(uri)
	return err == nil
}
