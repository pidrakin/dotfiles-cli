package link

import (
	"github.com/spf13/cobra"
	"gitlab.com/pidrakin/dotfiles-cli/cli/common"
	"gitlab.com/pidrakin/dotfiles-cli/cmd/link"
)

type Handler struct {
	command     *cobra.Command
	force       bool
	backup      bool
	hooks       *bool
	interactive bool
}

func (handler *Handler) Command() *cobra.Command {
	return handler.command
}

func NewHandler(use string, short string, long string) (handler Handler, err error) {
	handler.command = &cobra.Command{
		Use:   use,
		Short: short,
		Long:  long,
		Args:  cobra.MaximumNArgs(1),
		RunE:  handler.run,
	}

	helper := common.UsageHelper{Cmd: handler.command}
	helper.AddOptArg("collection", "specify the collection to use")
	if err = helper.Parse(); err != nil {
		return
	}

	handler.command.Flags().BoolVarP(&handler.interactive, "interactive", "i", false, "interactive mode")
	// force = true; backup = true => interactive
	// force = true; backup = false => force
	// force = false; backup = true => backup
	// force = false; backup = false => skip
	handler.command.Flags().BoolVarP(&handler.force, "force", "f", false, "overwrite target files (no backup)")
	handler.command.Flags().BoolVarP(&handler.backup, "backup", "b", false, "backup target files if already existing")
	handler.command.Flags().BoolP("hooks", "k", true, "execute hooks")

	handler.command.Flags().SortFlags = false

	return
}

func (handler *Handler) run(command *cobra.Command, args []string) (err error) {
	var collection string
	if len(args) > 0 {
		collection = args[0]
	}

	if handler.command.Flags().Lookup("hooks").Changed {
		hooks, _ := handler.command.Flags().GetBool("hooks")
		handler.hooks = &hooks
	}

	return link.Run(handler.interactive, handler.backup, handler.force, handler.hooks, collection)
}
