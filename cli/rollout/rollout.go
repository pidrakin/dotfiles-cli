package rollout

import (
	"fmt"
	"strconv"

	"github.com/spf13/cobra"
	"gitlab.com/pidrakin/dotfiles-cli/cli/common"
	"gitlab.com/pidrakin/dotfiles-cli/cmd/rollout"
	regex2 "gitlab.com/pidrakin/dotfiles-cli/common/regex"
	"gitlab.com/pidrakin/dotfiles-cli/ssh_ultimate"
)

type Handler struct {
	command      *cobra.Command
	uri          string
	collection   string
	linkerFlags  []string
	user         string
	identityFile string
	keyPass      string
	rootInstall  *bool
	track        *bool
	once         *bool
	remove       *string
}

func (handler *Handler) Command() *cobra.Command {
	return handler.command
}

func NewHandler(use string, short string, long string) (handler Handler, err error) {
	handler.command = &cobra.Command{
		Use:   use,
		Short: short,
		Long:  long,
		Args:  handler.validateArguments,
		RunE:  handler.run,
	}

	helper := common.UsageHelper{Cmd: handler.command}
	helper.AddOptArg("[ssh://][user@]host[:port]", "remote to install and link the dotfiles to")
	if err = helper.Parse(); err != nil {
		return
	}

	handler.command.Flags().StringVarP(&handler.uri, "uri", "u", "", "URI to install and link on the remote")
	handler.command.Flags().StringVarP(&handler.collection, "collection", "c", "", "collection for installation or update")
	handler.command.Flags().BoolP("force", "f", false, "overwrite target files while linking (no backup)")
	handler.command.Flags().BoolP("backup", "b", false, "backup target files if already existing")
	handler.command.Flags().Bool("hooks", true, "execute hooks")
	handler.command.Flags().StringVarP(&handler.user, "login", "l", "", "login_name")
	handler.command.Flags().IntP("port", "p", 22, "port")
	handler.command.Flags().StringVarP(&handler.identityFile, "identity-file", "i", "", "identity file")
	handler.command.Flags().StringVarP(&handler.keyPass, "key-pass", "k", "", "identity file password")
	handler.command.Flags().Bool("root-install", false, "install dotfiles as root")
	handler.command.Flags().Bool("track", true, "show rollout status in dotfiles status")
	handler.command.Flags().Bool("once", true, "do not store rollout config")
	handler.command.Flags().String("remove", "", "remove rollout config")

	handler.command.Flags().SortFlags = false

	return
}

func (handler *Handler) validateArguments(cmd *cobra.Command, args []string) error {
	if cmd.Flags().Lookup("remove").Changed {
		return nil
	}
	if err := cobra.ExactArgs(1)(cmd, args); err != nil {
		return err
	}
	if !handler.isValidRemote(args[0]) {
		return fmt.Errorf("invalid remote [%s] specified", args[0])
	}
	return nil
}

func (handler *Handler) run(command *cobra.Command, args []string) (err error) {
	var uri string
	if len(args) > 0 {
		uri = args[0]
	}
	if f, _ := command.Flags().GetBool("force"); f {
		handler.linkerFlags = append(handler.linkerFlags, "-f")
	}
	if f, _ := command.Flags().GetBool("backup"); f {
		handler.linkerFlags = append(handler.linkerFlags, "-b")
	}
	if f, _ := command.Flags().GetBool("hooks"); !f {
		handler.linkerFlags = append(handler.linkerFlags, "-k=false")
	}
	if command.Flags().Lookup("root-install").Changed {
		f, _ := command.Flags().GetBool("root-install")
		handler.rootInstall = &f
	}
	if command.Flags().Lookup("track").Changed {
		f, _ := command.Flags().GetBool("track")
		handler.track = &f
	}
	if command.Flags().Lookup("once").Changed {
		f, _ := command.Flags().GetBool("once")
		handler.once = &f
	}
	if command.Flags().Lookup("remove").Changed {
		f, _ := command.Flags().GetString("remove")
		if f == "" {
			return fmt.Errorf("no rollout config specified for removal")
		}
		handler.remove = &f
	}

	var options []func(*ssh_ultimate.Parameterizable) error
	if handler.user != "" {
		options = append(options, ssh_ultimate.UserOption(handler.user))
	}
	if command.Flags().Lookup("port").Changed {
		f, _ := command.Flags().GetInt("port")
		options = append(options, ssh_ultimate.PortOption(strconv.Itoa(f)))
	}
	if handler.identityFile != "" {
		options = append(options, ssh_ultimate.IdentityFilePathOption(handler.identityFile))
	}
	if handler.keyPass != "" {
		options = append(options, ssh_ultimate.IdentityFilePassphraseOption(handler.keyPass))
	}

	return rollout.Run(handler.rootInstall, handler.track, handler.once, uri, handler.uri, handler.collection, handler.linkerFlags, handler.remove, options)
}

func isValidRemote(remote string) bool {
	return regex2.UserHostPortRegex.MatchString(remote)
}

func (handler *Handler) isValidRemote(remote string) bool {
	return isValidRemote(remote)
}
