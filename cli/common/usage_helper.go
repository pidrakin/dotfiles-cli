package common

import (
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/pidrakin/go/slices"
)

type Arg struct {
	Name        string
	Description string
}

type UsageHelper struct {
	Cmd     *cobra.Command
	args    []Arg
	optArgs []Arg
}

func (helper *UsageHelper) init(cmd *cobra.Command) {
	helper.Cmd = cmd
}

func (helper *UsageHelper) AddArg(name string, description string) {
	helper.args = append(helper.args, Arg{name, description})
}

func (helper *UsageHelper) AddOptArg(name string, description string) {
	helper.optArgs = append(helper.optArgs, Arg{name, description})
}

func (helper *UsageHelper) Parse() error {
	if helper.Cmd == nil {
		return fmt.Errorf("UsageHelper.Cmd has to be set")
	}
	var argsTemplate string
	argsTemplate += helper.parseArgs("Args", helper.args)
	argsTemplate += helper.parseArgs("Optional Args", helper.optArgs)

	usageTemplate := `Usage:{{if .Runnable}}
  {{.UseLine}}{{end}}{{if .HasAvailableSubCommands}}
  {{.CommandPath}} [command]{{end}}{{if gt (len .Aliases) 0}}

Aliases:
  {{.NameAndAliases}}{{end}}{{if .HasExample}}

Examples:
{{.Example}}{{end}}{{if .HasAvailableSubCommands}}

Available Commands:{{- range .Commands -}}
{{if (or .IsAvailableCommand (eq .Name "help"))}}
  {{rpad .Name .NamePadding }} {{.Short}}{{end}}{{end}}{{end}}
` + argsTemplate +
		`{{if .HasAvailableLocalFlags}}
Flags:
{{.LocalFlags.FlagUsages | trimTrailingWhitespaces}}{{end}}
{{if .HasAvailableInheritedFlags}}
Global Flags:
{{.InheritedFlags.FlagUsages | trimTrailingWhitespaces}}{{end}}
{{if .HasHelpSubCommands}}

Additional help topics:{{range .Commands}}
{{if .IsAdditionalHelpTopicCommand}}
  {{rpad .CommandPath .CommandPathPadding}} {{.Short}}{{end}}{{end}}{{end}}
{{if .HasAvailableSubCommands}}

Use "{{.CommandPath}} [command] --help" for more information about a command.{{end}}`
	helper.Cmd.SetUsageTemplate(usageTemplate)
	return nil
}

func (helper *UsageHelper) parseArgs(name string, args []Arg) (template string) {
	if len(args) > 0 {
		template = fmt.Sprintf("%s:\n", name)
		max := slices.Max(slices.Map(args, func(arg Arg) int {
			return len(arg.Name)
		}))
		formatString := fmt.Sprintf("  %%-%ds   %%s\n", max)
		for _, arg := range args {
			template += fmt.Sprintf(formatString, arg.Name, arg.Description)
		}
		template += "\n"
	}
	return
}
