package common

import (
	"fmt"
	"gitlab.com/pidrakin/go/output"
	"gitlab.com/pidrakin/go/regex"
	"regexp"
)

func ParseOutputFormat(outputFormat string) (output.FormatType, string, error) {
	regCustomColumns := regexp.MustCompile(`^(?P<key>custom-columns)=(?P<value>.+)$`)
	regJson := regexp.MustCompile(`^(?P<key>json)(?:=(?P<value>.+))?$`)
	regGoTemplate := regexp.MustCompile(`^(?P<key>go-template)=(?P<value>.+)$`)
	var format output.FormatType
	var template string
	if outputFormat == "text" {
		format = output.TableFormat
	} else if outputFormat == "wide" {
		format = output.WideTableFormat
	} else if regCustomColumns.MatchString(outputFormat) {
		groups := regex.MatchGroups(regCustomColumns, outputFormat)
		format = output.WideTableFormat
		template = groups["value"]
	} else if outputFormat == "yaml" {
		format = output.YamlFormat
	} else if regJson.MatchString(outputFormat) {
		groups := regex.MatchGroups(regJson, outputFormat)
		format = output.JsonFormat
		template = groups["value"]
	} else if regGoTemplate.MatchString(outputFormat) {
		groups := regex.MatchGroups(regGoTemplate, outputFormat)
		format = output.GoTemplateFormat
		template = groups["value"]
	} else {
		return 0, "", fmt.Errorf("logging format must be either [text], [wide], [custom-columns], [yaml], [json] or [go-template=template]")
	}
	return format, template, nil
}
