package cli

import (
	"bytes"
	"fmt"
	"gitlab.com/pidrakin/dotfiles-cli/common/logging"
	"gitlab.com/pidrakin/dotfiles-cli/config"
	"gitlab.com/pidrakin/dotfiles-cli/git"
	"gitlab.com/pidrakin/dotfiles-cli/text"
	"gitlab.com/pidrakin/go/sysfs"
	"runtime"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
)

var Version string
var buildTime string
var DryRun bool

type RootHandler struct {
	command *cobra.Command
	cfgFile string
	verbose int
	quiet   bool
}

func NewRootHandler(use string, short string, long string) (handler RootHandler) {
	cobra.OnInitialize(handler.onInit)

	handler.command = &cobra.Command{
		Use:     use,
		Short:   short,
		Long:    long,
		Version: fmt.Sprintf("v%s %s/%s Build Time: %s", Version, runtime.GOOS, runtime.GOARCH, buildTime),
	}

	handler.command.PersistentFlags().BoolVarP(&DryRun, "dry-run", "d", false, "only print, but do not apply changes")
	handler.command.PersistentFlags().CountVarP(&handler.verbose, "verbose", "v", "verbose debugging")
	handler.command.PersistentFlags().BoolVarP(&handler.quiet, "quiet", "q", false, "do not print anything to stdout/stderr")

	handler.command.Flags().SortFlags = false

	return handler
}

func (handler *RootHandler) AddCommand(command *cobra.Command) {
	handler.command.AddCommand(command)
}

func (handler *RootHandler) Execute() error {
	return handler.command.Execute()
}

func (handler *RootHandler) onInit() {
	if err := handler.initLogs(); err != nil {
		_ = fmt.Errorf("Error occurred during log initilaization: %v\n", err)
	}

	if DryRun {
		sysfs.DryRun = true
		git.SetDryRun(true)
		log.Warn(text.DryRunActive)
	}
}

func (handler *RootHandler) initLogs() error {
	var buf bytes.Buffer
	log.SetOutput(&buf)
	target := logging.STDERR

	cfgMgr, err := config.NewManager()
	if err != nil {
		return err
	}

	cfg := cfgMgr.Config
	if cfg != nil && cfg.LogTarget != "" {
		if cfg.LogTarget == "STDOUT" {
			target = logging.STDOUT
		} else if cfg.LogTarget == "FILE" {
			target = logging.FILE
		}
	}

	stateConfig := cfgMgr.StateConfig
	if stateConfig != nil && stateConfig.LogTarget != "" {
		if stateConfig.LogTarget == "STDERR" {
			target = logging.STDERR
		} else if stateConfig.LogTarget == "STDOUT" {
			target = logging.STDOUT
		} else if stateConfig.LogTarget == "FILE" {
			target = logging.FILE
		}
	}

	hook, err := logging.NewLoggingHook(target)
	if err != nil {
		return err
	}
	log.AddHook(hook)

	if target <= logging.STDOUT {
		loglevel := log.StandardLogger().Level
		if handler.quiet {
			loglevel = log.ErrorLevel
		}
		loglevel = loglevel + log.Level(handler.verbose)
		log.SetLevel(loglevel)
		if handler.quiet && handler.verbose > 0 {
			log.Warn(text.VerboseOverrulesQuite)
		}
	}
	log.Debug("Enabled Debug Logging")
	return nil
}
