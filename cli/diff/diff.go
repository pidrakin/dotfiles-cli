package diff

import (
	"github.com/spf13/cobra"
	"gitlab.com/pidrakin/dotfiles-cli/cli/common"
	"gitlab.com/pidrakin/dotfiles-cli/cmd/diff"
)

type Handler struct {
	command    *cobra.Command
	collection string
}

func (handler *Handler) Command() *cobra.Command {
	return handler.command
}

func NewHandler(use string, short string, long string) (handler Handler, err error) {

	handler.command = &cobra.Command{
		Use:   use,
		Short: short,
		Long:  long,
		Args:  cobra.MaximumNArgs(1),
		RunE:  handler.run,
	}

	helper := common.UsageHelper{Cmd: handler.command}
	helper.AddOptArg("collection", "specify the collection to use")
	if err = helper.Parse(); err != nil {
		return
	}

	return
}

func (handler *Handler) run(cmd *cobra.Command, args []string) (err error) {
	var collection string
	if len(args) > 0 {
		collection = args[0]
	}

	return diff.Run(collection)
}
