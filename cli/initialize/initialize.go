package initialize

import (
	_ "embed"
	"github.com/spf13/cobra"
	"gitlab.com/pidrakin/dotfiles-cli/cmd/initialize"
)

type Handler struct {
	command *cobra.Command
	expert  bool
	example bool
}

func (handler *Handler) Command() *cobra.Command {
	return handler.command
}

func NewHandler(use string, short string, long string) (handler Handler, err error) {
	handler.command = &cobra.Command{
		Use:   use,
		Short: short,
		Long:  long,
		RunE:  handler.run,
	}
	handler.command.Flags().BoolVarP(&handler.expert, "expert", "e", false, "supply expert mode file structure")
	handler.command.Flags().BoolVar(&handler.example, "example", false, "create example files")
	return
}

func (handler *Handler) run(command *cobra.Command, args []string) (err error) {
	return initialize.Run(handler.expert, handler.example)
}
