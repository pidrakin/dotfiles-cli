package config

import (
	"fmt"
	"github.com/spf13/cobra"
	"gitlab.com/pidrakin/dotfiles-cli/cli/common"
	"gitlab.com/pidrakin/dotfiles-cli/cmd/config"
	"gitlab.com/pidrakin/go/regex"
	"regexp"
)

type Handler struct {
	command      *cobra.Command
	configType   string
	outputFormat string
}

func (handler *Handler) Command() *cobra.Command {
	return handler.command
}

func NewHandler(use string, short string, long string) (handler Handler, err error) {
	handler.command = &cobra.Command{
		Use:   use,
		Short: short,
		Long:  long,
	}

	viewCommand := &cobra.Command{
		Use:   "view [state|repo]",
		Short: "output config",
		Long:  "output either state config or installed repo config (default: state)",
		Args:  handler.validateViewArguments,
		RunE:  handler.runView,
	}
	viewCommand.Flags().StringVarP(&handler.outputFormat, "output", "o", "yaml", "output format: text, wide, custom-columns, yaml, json, go-template")
	viewCommand.Flags().SortFlags = false
	viewCommand.Example = `  # Output Format
  refer to dotfiles status --help`

	viewHelper := common.UsageHelper{Cmd: viewCommand}
	if err = viewHelper.Parse(); err != nil {
		return
	}

	setCommand := &cobra.Command{
		Use:   "set [state|repo] key=value [...key=value]",
		Short: "set a config value",
		Long:  "set a config value either in the state config or the installed repo config (default: state)",
		Args:  handler.validateSetArguments,
		RunE:  handler.runSet,
	}
	viewCommand.Flags().SortFlags = false

	helper := common.UsageHelper{Cmd: handler.command}
	if err = helper.Parse(); err != nil {
		return
	}

	handler.command.AddCommand(viewCommand, setCommand)

	return handler, nil
}

func (handler *Handler) validateViewArguments(cmd *cobra.Command, args []string) error {
	if err := cobra.MaximumNArgs(1)(cmd, args); err != nil {
		return err
	}
	if len(args) > 0 && args[0] != "state" && args[0] != "repo" {
		return fmt.Errorf("argument can only be [state] or [repo]")
	}
	return nil
}

func (handler *Handler) validateSetArguments(cmd *cobra.Command, args []string) error {
	if err := cobra.MinimumNArgs(1)(cmd, args); err != nil {
		return err
	}
	reg := regexp.MustCompile(`^[a-z0-9_]+=.+$`)
	if len(args) > 0 && args[0] != "state" && args[0] != "repo" && !reg.MatchString(args[0]) {
		return fmt.Errorf("first argument can only be [state] or [repo] or of the format [key=value]")
	}
	if len(args) > 1 && (args[0] == "state" || args[0] == "repo") && len(args) < 2 {
		return fmt.Errorf("arguments must be of the format [key=value]")
	}
	if len(args) > 0 {
		for _, arg := range args[1:] {
			if !reg.MatchString(arg) {
				return fmt.Errorf("arguments must be of the format [key=value]")
			}
		}
	}
	return nil
}

func (handler *Handler) runView(command *cobra.Command, args []string) (err error) {
	configType := config.STATE
	if len(args) == 1 {
		if args[0] == "state" {

		} else if args[0] == "repo" {
			configType = config.REPO
		}
	}
	//reg := regexp.MustCompile(`^(?P<key>go-template)=(?P<value>.+)$`)
	//if handler.outputFormat != "yaml" && !reg.MatchString(handler.outputFormat) {
	//	return fmt.Errorf("output format must be either [yaml] or [go-template=template]")
	//}
	//var format config.OutputFormat
	//var template string
	//if handler.outputFormat == "yaml" {
	//	format = config.YAML
	//} else {
	//	groups := regex.MatchGroups(reg, handler.outputFormat)
	//	key := groups["key"]
	//	if key != "go-template" {
	//		return fmt.Errorf("output format must be either [yaml] or [go-template=template]")
	//	}
	//	format = config.TEMPLATE
	//	template = groups["value"]
	//}
	format, template, err := common.ParseOutputFormat(handler.outputFormat)
	if err != nil {
		return err
	}
	view := config.ViewConfig{}
	return view.View(configType, format, template)
}

func (handler *Handler) runSet(command *cobra.Command, args []string) (err error) {
	configType := config.STATE
	if len(args) > 0 {
		if args[0] == "state" {

		} else if args[0] == "repo" {
			configType = config.REPO
		}
	}
	reg := regexp.MustCompile(`^(?P<key>[a-z0-9_]+)=(?P<value>.+)$`)
	startIdx := 0
	if len(args) > 1 && (args[0] == "state" || args[0] == "repo") {
		startIdx = 1
	}
	var setter []*config.Set
	for _, arg := range args[startIdx:] {
		groups := regex.MatchGroups(reg, arg)
		setter = append(setter, &config.Set{
			Key:   groups["key"],
			Value: groups["value"],
		})
	}
	set := config.SetConfig{}
	return set.Set(configType, setter...)
}
