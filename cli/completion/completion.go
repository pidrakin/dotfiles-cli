package completion

import (
	"os"

	"github.com/spf13/cobra"
)

type Handler struct {
	command *cobra.Command
}

func (handler *Handler) Command() *cobra.Command {
	return handler.command
}

func NewHandler(use string, short string, long string) (handler Handler) {

	handler.command = &cobra.Command{
		Use:     use,
		Short:   short,
		Aliases: []string{"completions"},
		Long: `To load completions:

Bash:
  $ source <(dotfiles completion bash)

  # Linux:
  $ dotfiles completion bash > /etc/bash_completion.d/dotfiles
  # macOS:
  $ dotfiles completion bash > /usr/local/etc/bash_completion.d/dotfiles
`,
		DisableFlagsInUseLine: true,
		ValidArgs:             []string{"bash"},
		Args:                  cobra.ExactValidArgs(1),
		RunE: func(cmd *cobra.Command, args []string) (err error) {
			switch args[0] {
			case "bash":
				err = cmd.Root().GenBashCompletion(os.Stdout)
			}
			return
		},
	}

	return handler
}
