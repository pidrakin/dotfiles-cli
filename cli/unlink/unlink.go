package unlink

import (
	"github.com/spf13/cobra"
	"gitlab.com/pidrakin/dotfiles-cli/cmd/unlink"
)

type Handler struct {
	command *cobra.Command
}

func (handler *Handler) Command() *cobra.Command {
	return handler.command
}

func NewHandler(use string, short string, long string) (handler Handler) {
	handler.command = &cobra.Command{
		Use:     use,
		Short:   short,
		Long:    long,
		Aliases: []string{"delete", "remove", "purge"},
		RunE:    handler.run,
	}
	return
}

func (handler *Handler) run(command *cobra.Command, args []string) (err error) {
	return unlink.Run()
}
