package status

import (
	"github.com/spf13/cobra"
	"gitlab.com/pidrakin/dotfiles-cli/cli/common"
	"gitlab.com/pidrakin/dotfiles-cli/cmd/status"
)

type Handler struct {
	command      *cobra.Command
	local        bool
	remote       bool
	outputFormat string
}

func (handler *Handler) Command() *cobra.Command {
	return handler.command
}

func NewHandler(use string, short string, long string) (handler Handler, err error) {
	handler.command = &cobra.Command{
		Use:   use,
		Short: short,
		Long:  long,
		RunE:  handler.run,
	}

	helper := common.UsageHelper{Cmd: handler.command}
	if err = helper.Parse(); err != nil {
		return
	}

	handler.command.Flags().BoolVarP(&handler.local, "local", "l", false, "show status of local dotfiles environment")
	handler.command.Flags().BoolVarP(&handler.remote, "remote", "r", false, "show status of remotely managed dotfiles environments")
	handler.command.Flags().StringVarP(&handler.outputFormat, "output", "o", "text", "output format: text, wide, custom-columns, yaml, json, go-template")

	handler.command.Flags().SortFlags = false

	handler.command.Example = `# outputs a table with host, installed, linked, link status and git status
  dotfiles status
  dotfiles status --output text

  # outputs a table with host, cli, installed, linked, collection, link status, repo and git status
  dotfiles status --output wide

  # outputs a table with any of the custom columns specified, separated by a colon
  dotfiles status --output custom-columns=host:linked:link_status

  # outputs yaml
  dotfiles status --output yaml

  # outputs json in pretty format
  dotfiles status --output json

  # outputs json in compact format
  dotfiles status --output json=compact

  # outputs what the go template specifies
  dotfiles status --output go-template='{{ range . }}{{ printf "%s: %s\n" .Host . LinkStatus }}{{ end }}'`

	return handler, nil
}

func (handler *Handler) run(command *cobra.Command, args []string) (err error) {
	format, template, err := common.ParseOutputFormat(handler.outputFormat)
	if err != nil {
		return err
	}

	if !handler.local && !handler.remote {
		handler.local = true
		handler.remote = true
	}

	return status.Run(handler.local, handler.remote, format, template)
}
