package eject

import (
	"fmt"
	"github.com/spf13/cobra"
	"gitlab.com/pidrakin/dotfiles-cli/cli/common"
	"gitlab.com/pidrakin/dotfiles-cli/cmd/eject"
)

type Handler struct {
	command *cobra.Command
	file    string
	stdout  bool
}

func (handler *Handler) Command() *cobra.Command {
	return handler.command
}

func NewHandler(use string, short string, long string) (handler Handler, err error) {
	handler.command = &cobra.Command{
		Use:   use,
		Short: short,
		Long:  long,
		Args:  handler.validateArguments,
		RunE:  handler.run,
	}

	helper := common.UsageHelper{Cmd: handler.command}
	helper.AddOptArg("config-file", "config file to eject")
	if err = helper.Parse(); err != nil {
		return
	}

	handler.command.Flags().BoolVarP(&handler.stdout, "stdout", "O", false, "eject on stdout")

	handler.command.Flags().SortFlags = false

	return handler, nil
}

func (handler *Handler) validateArguments(cmd *cobra.Command, args []string) error {
	if err := cobra.MaximumNArgs(1)(cmd, args); err != nil {
		return err
	}
	if len(args) == 1 {
		if args[0] == "install.sh" {
			return nil
		}
		return fmt.Errorf("cannot eject config file [%s], only install.sh can be ejected", args[0])
	}
	return nil
}

func (handler *Handler) run(command *cobra.Command, args []string) (err error) {
	if len(args) == 0 {
		return eject.NewListEjectable()
	}
	return eject.Run(args[0], handler.stdout)
}
