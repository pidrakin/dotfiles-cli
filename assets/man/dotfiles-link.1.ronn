# dotfiles-link(1) -- link *dotfiles* from install directory to home directory

## SYNOPSIS

*dotfiles* \[options\] link [&lt;collection&gt;] [&lt;flags&gt;]

## Description

Link dotfiles from installed repository to the home directory.
Updates stateconfig (see dotfiles(1)) with the linked files.

## OPTIONS

See dotfiles(1)

## FLAGS

`-i`, `--interactive`
: interactive mode

`-f`, `--force`
: overwrite target files (no backup)

`-b`, `--backup`
: backup target files if already existing

`-k`, `--hooks`
: exeute hooks (default true)

## NOTES

It is possible to prepare a set of `pre-` and `post-link` hooks. Those are executables placed in the repository under `.dotfiles/hooks/`. There are several possible locations available depending on the repository structure.

`.dotfiles/hooks/[pre|post]-link`
: runs before and after every `link` call

`.dotfiles/hooks/<package>/[pre|post]-link`
: runs before and after the specific package (see dotfiles-collections(1)) has been processed during the linking

`.dotfiles/hooks/<collection>/[pre|post]-link`
: runs before and after the specific collection (see dotfiles-collections(1)) has been processed during the linking

To disable the hooks run `dotfiles link --hooks false`.

## AUTHOR

Written by pidrakin

## Copyright

THE HOOKAH-WARE LICENSE (Revision 42)

As long as you retain this notice you can do whatever you want with this stuff.
If we meet some day, and you think this stuff is worth it, you can buy me a
hookah in return.

## SEE ALSO

dotfiles(1)
dotfiles-init(1)
dotfiles-install(1)
dotfiles-unlink(1)
dotfiles-diff(1)
dotfiles-rollout(1)
dotfiles-status(1)
dotfiles-config(1)
dotfiles-eject(1)
dotfiles-completion(1)
dotfiles-stateconfig(1)
dotfiles-repoconfig(1)
dotfiles-remote(1)
