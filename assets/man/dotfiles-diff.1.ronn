# dotfiles-diff(1) -- show changes between linked *dotfiles* files and repository *dotfiles*

## SYNOPSIS

*dotfiles* \[options\] diff [&lt;collection&gt;]

## Description

Show changes between linked files and repository

## OPTIONS

See dotfiles(1)

## AUTHOR

Written by pidrakin

## Copyright

THE HOOKAH-WARE LICENSE (Revision 42)

As long as you retain this notice you can do whatever you want with this stuff.
If we meet some day, and you think this stuff is worth it, you can buy me a
hookah in return.

## SEE ALSO

dotfiles(1)
dotfiles-init(1)
dotfiles-install(1)
dotfiles-link(1)
dotfiles-unlink(1)
dotfiles-rollout(1)
dotfiles-status(1)
dotfiles-config(1)
dotfiles-eject(1)
dotfiles-completion(1)
dotfiles-stateconfig(1)
dotfiles-repoconfig(1)
dotfiles-remote(1)
