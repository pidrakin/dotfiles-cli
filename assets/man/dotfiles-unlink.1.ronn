# dotfiles-unlink(1) -- unlinks previously linked *dotfiles* in the home directory

## SYNOPSIS

*dotfiles* \[options\] unlink

## Description

Link dotfiles from installed repository to the home directory.
Updates stateconfig (see dotfiles(1)) with the linked files.

## OPTIONS

See dotfiles(1)

## AUTHOR

Written by pidrakin

## Copyright

THE HOOKAH-WARE LICENSE (Revision 42)

As long as you retain this notice you can do whatever you want with this stuff.
If we meet some day, and you think this stuff is worth it, you can buy me a
hookah in return.

## SEE ALSO

dotfiles(1)
dotfiles-help(1)
dotfiles-init(1)
dotfiles-install(1)
dotfiles-link(1)
dotfiles-diff(1)
dotfiles-rollout(1)
dotfiles-status(1)
dotfiles-config(1)
dotfiles-completion(1)
dotfiles-stateconfig(1)
dotfiles-repoconfig(1)
dotfiles-remote(1)
