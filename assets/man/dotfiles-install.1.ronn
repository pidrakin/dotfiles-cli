# dotfiles-install(1) -- clones *dotfiles* from repository into a newly created directory

## SYNOPSIS

*dotfiles* \[options\] install [&lt;uri&gt;] [&lt;flags&gt;]

## Description

Clones `uri` into a newly created directory, either under `.local/share/dotfiles/` or if `--global` is set under `/usr/share/dotfiles`.
Creates the stateconfig (see dotfiles(1)) and sets the context.

`uri` may also be in the format `gl:<username>` or `gh:<username>` to find and install a dotfiles repository from a Gitlab.com and Github.com user's repository respectively.

## OPTIONS

See dotfiles(1)

## FLAGS

`-i`, `--interactive`
: interactive mode

`-g`, `--global`
: install dotfiles globally into `/usr/share/dotfiles`

## NOTES

If there is a need for configuration in case of an SSH-URI, use `~/.ssh/config` **(ssh_config(5))**.<br>
If it is a private repository use git `credential.helper` or add an SSH key.

## AUTHOR

Written by pidrakin

## Copyright

THE HOOKAH-WARE LICENSE (Revision 42)

As long as you retain this notice you can do whatever you want with this stuff.
If we meet some day, and you think this stuff is worth it, you can buy me a
hookah in return.

## SEE ALSO

dotfiles(1)
dotfiles-help(1)
dotfiles-init(1)
dotfiles-link(1)
dotfiles-unlink(1)
dotfiles-diff(1)
dotfiles-rollout(1)
dotfiles-status(1)
dotfiles-config(1)
dotfiles-completion(1)
dotfiles-stateconfig(1)
dotfiles-repoconfig(1)
dotfiles-remote(1)
