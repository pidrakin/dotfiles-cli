#!/usr/bin/env sh

rebuild() {
  docker run --rm -t -v $(pwd):/usr/local/app -w /usr/local/app node bash -c "
corepack enable
corepack prepare pnpm@latest --activate
pnpm config set store-dir /root/.pnpm-store
pnpm dlx carbon-now-cli --config preview.carbon.now.sh.json -p preview --save-as preview preview.txt
addgroup --quiet --gid $(id -g) --system user 2>/dev/null
adduser --quiet --system --uid $(id -u) --gid $(id -g) --shell /sbin/nologin --no-create-home user 2>/dev/null
chown $(id -u):$(id -g) preview.png
"
}

if type docker >/dev/null 2>&1; then
  if [ -t 0 ]; then
    while true; do
      printf "rebuild preview.png (it may take some time) [yN]: "
      read -r REBUILD

      if [ -z "$REBUILD" ]; then
        echo "No action taken, exiting."
        exit 0
      fi

      case "$REBUILD" in
        [yY])
          rebuild
          exit 0;;
        [nN])
          echo "No action taken, exiting."
          exit 0;;
        *)
          echo "Please answer y or n.";;
      esac
    done
  fi
fi