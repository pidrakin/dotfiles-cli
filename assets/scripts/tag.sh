#!/usr/bin/env bash
git tag -f "v${VERSION}"
git push -f origin "v${VERSION}"
