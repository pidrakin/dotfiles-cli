#!/bin/sh

GANDI_APIKEY=${GANDI_APIKEY:-$1}
if [[ -z "$GANDI_APIKEY" ]]; then
  echo "SET GANDI_APIKEY"
  exit 1
fi


VERSION=${VERSION:-$2}

if [[ "${VERSION:0:1}" != "v" ]]; then
  download_result=$(curl -fsSL https://gitlab.com/api/v4/projects/24946569/releases/ | grep -o -E '"name":"v[^"]+')
  VERSION=$(echo "${download_result}" | head -1 | cut -d'"' -f4)
fi

echo $VERSION

function update_txt() {
  RECORD=${1}
  FILE=${2}

  if [[ -z "$2" ]]; then
    FILE="linux-amd64.${RECORD}"
  fi

  curl -X PUT -H "Content-Type: application/json" -H "Authorization: Apikey $GANDI_APIKEY" https://api.gandi.net/v5/livedns/domains/pidrak.in/records/${RECORD}.redirector.dotfiles/TXT -d "{\"rrset_values\": [\"https://gitlab.com/pidrakin/dotfiles-cli/-/releases/${VERSION}/downloads/dotfiles-${VERSION#v}-${FILE}\"], \"rrset_ttl\": 300}"

}

update_txt amd64.tar.gz linux-amd64.tar.gz
update_txt x86_64.tar.gz linux-amd64.tar.gz
update_txt arm64.tar.gz linux-arm64.tar.gz
update_txt i386.tar.gz linux-i386.tar.gz
update_txt tar.gz
update_txt amd64.rpm linux-amd64.rpm
update_txt x86_64.rpm linux-amd64.rpm
update_txt arm64.rpm linux-arm64.rpm
update_txt i386.rpm linux-i386.rpm
update_txt rpm
update_txt amd64.deb linux-amd64.deb
update_txt x86_64.deb linux-amd64.deb
update_txt arm64.deb linux-arm64.deb
update_txt i386.deb linux-i386.deb
update_txt deb
update_txt amd64.apk linux-amd64.apk
update_txt x86_64.apk linux-amd64.apk
update_txt arm64.apk linux-arm64.apk
update_txt i386.apk linux-i386.apk
update_txt apk
update_txt mac-amd64.tar.gz darwin-amd64.tar.gz
update_txt mac-arm64.gar.gz darwin-arm64.tar.gz
update_txt mac.tar.gz darwin-amd64.tar.gz
update_txt darwin-amd64.tar.gz darwin-amd64.tar.gz
update_txt darwin-x86_64.tar.gz darwin-amd64.tar.gz
update_txt darwin-arm64.tar.gz darwin-arm64.tar.gz
update_txt darwin.tar.gz darwin-amd64.tar.gz
curl -X PUT -H "Content-Type: application/json" -H "Authorization: Apikey $GANDI_APIKEY" https://api.gandi.net/v5/livedns/domains/pidrak.in/records/redirector.dotfiles/TXT -d "{\"rrset_values\": [\"https://gitlab.com/pidrakin/dotfiles-cli/-/releases/${VERSION}/downloads/install.sh\"], \"rrset_ttl\": 300}"
