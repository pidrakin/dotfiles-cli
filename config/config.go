package config

import (
	"fmt"
	"os"
	"path"
	"path/filepath"

	"github.com/mitchellh/go-homedir"
	log "github.com/sirupsen/logrus"
	"gitlab.com/pidrakin/dotfiles-cli/text"
	"gitlab.com/pidrakin/go/slices"
	"gitlab.com/pidrakin/go/sysfs"
	"gopkg.in/yaml.v3"
)

type configYAML struct {
	LocalInstallDir   string               `yaml:"local_install_dir"`
	GlobalInstallDir  string               `yaml:"global_install_dir"`
	Ignore            []string             `yaml:"ignore"`
	IgnoreFile        string               `yaml:"ignore_file"`
	HooksDir          string               `yaml:"hooks_dir"`
	CollectionsConfig CollectionsConfigSub `yaml:"collections_config"`
	LogTarget         string               `yaml:"log_target"`
	Track             *bool                `yaml:"track,omitempty"`
	Once              *bool                `yaml:"once,omitempty"`
}

type Config struct {
	localInstallDir   string               `yaml:"local_install_dir"`
	globalInstallDir  string               `yaml:"global_install_dir"`
	Ignore            []string             `yaml:"ignore"`
	IgnoreFile        string               `yaml:"ignore_file"`
	HooksDir          string               `yaml:"hooks_dir"`
	CollectionsConfig CollectionsConfigSub `yaml:"collections_config"`
	LogTarget         string               `yaml:"log_target"`
	Track             *bool                `yaml:"track,omitempty"`
	Once              *bool                `yaml:"once,omitempty"`

	dir  string
	name string
}

type CollectionsConfigSub struct {
	File string `yaml:"file"`
}

func NewConfig() *Config {
	c := &Config{}
	initConfig(c)
	return c
}

func initConfig(c *Config) {
	if c.localInstallDir == "" {
		c.localInstallDir = localInstallDir
	}
	if c.globalInstallDir == "" {
		c.globalInstallDir = globalInstallDir
	}
	if c.IgnoreFile == "" {
		c.IgnoreFile = ignoreFile
	}
	if c.HooksDir == "" {
		c.HooksDir = hooksDir
	}
	if c.CollectionsConfig.File == "" {
		c.CollectionsConfig.File = collectionsConfigFile
	}
	if c.LogTarget == "" {
		c.LogTarget = "STDERR"
	}
	c.dir = configDir
	c.name = configName
	c.buildIgnore()
}

func (c *Config) LocalInstallDir() string {
	homeDir, _ := homedir.Dir()
	return path.Join(homeDir, c.localInstallDir)
}

func (c *Config) GlobalInstallDir() string {
	return c.globalInstallDir
}

func (c *Config) Dir() string {
	return c.dir
}

func (c *Config) Name() string {
	return c.name
}

func (c *Config) buildIgnore() {
	c.Ignore = slices.Unique([]string{
		gitDir,
		c.dir,
		c.name,
		c.IgnoreFile,
		c.CollectionsConfig.File,
	})
}

func (c *Config) UnmarshalYAML(unmarshal func(interface{}) error) (err error) {
	cfgYAML := configYAML{}
	if err = unmarshal(&cfgYAML); err != nil {
		return
	}
	c.localInstallDir = cfgYAML.LocalInstallDir
	c.globalInstallDir = cfgYAML.GlobalInstallDir
	c.Ignore = cfgYAML.Ignore
	c.IgnoreFile = cfgYAML.IgnoreFile
	c.HooksDir = cfgYAML.HooksDir
	c.CollectionsConfig = cfgYAML.CollectionsConfig
	c.LogTarget = cfgYAML.LogTarget
	initConfig(c)
	return
}

func (c *Config) MarshalYAML() (inf interface{}, err error) {
	inf = configYAML{
		LocalInstallDir:   c.localInstallDir,
		GlobalInstallDir:  c.globalInstallDir,
		Ignore:            c.Ignore,
		IgnoreFile:        c.IgnoreFile,
		HooksDir:          c.HooksDir,
		CollectionsConfig: c.CollectionsConfig,
		LogTarget:         c.LogTarget,
	}
	return
}

func ReadConfig(source string) (*Config, error) {
	var err error
	cfg := NewConfig()
	if source == "" {
		source, err = os.Getwd()
		if err != nil {
			return nil, err
		}
	}
	file := path.Join(source, cfg.Dir(), cfg.Name())
	log.WithField("configFile", file).Debug(text.ConfigRead)

	if !sysfs.FileExists(file) {
		return nil, fmt.Errorf(text.NoConfig)
	}
	yamlFile, err := os.ReadFile(file)
	if err != nil {
		return nil, err
	}
	config := &Config{}
	if err = yaml.Unmarshal(yamlFile, config); err != nil {
		return nil, err
	}
	config.dir = filepath.Join(source, cfg.Dir())
	return config, nil
}

func (c *Config) Save() error {
	file := path.Join(c.Dir(), c.Name())
	yamlData, err := yaml.Marshal(c)
	if err != nil {
		return err
	}
	return os.WriteFile(file, yamlData, os.FileMode(0644))
}
