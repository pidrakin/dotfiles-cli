package config

import (
	"crypto/md5"
	"encoding/hex"
	"fmt"
	"io"
	"io/fs"
	"os"
	"path/filepath"
	"strings"

	"github.com/mitchellh/go-homedir"
	log "github.com/sirupsen/logrus"
	"gitlab.com/pidrakin/dotfiles-cli/common/logging"
	"gitlab.com/pidrakin/dotfiles-cli/text"
	"gitlab.com/pidrakin/go/sysfs"
	"gitlab.com/pidrakin/go/templates"
	"gopkg.in/yaml.v3"
)

const version string = "1.0"

func Version() string {
	return version
}

type Linked struct {
	Files    []string
	Packages map[string][]string
}

func NewLinked() *Linked {
	return &Linked{}
}

func (l *Linked) Empty() bool {
	if l.Files != nil {
		return len(l.Files) == 0
	} else if l.Packages != nil {
		return len(l.Packages) == 0
	}
	return true
}

func (l *Linked) IsPackages() bool {
	return l.Packages != nil
}

func (l *Linked) Get(packages ...string) (files []string) {
	if l == nil {
		return
	}

	if len(packages) == 0 || (len(packages) == 1 && packages[0] == "") {
		if l.Files == nil {
			return
		}
		files = l.Files
		return
	}
	if l.Packages == nil {
		return
	}
	for _, pkg := range packages {
		files = append(files, (l.Packages)[pkg]...)
	}
	return
}

func (l *Linked) Set(files []string, packages ...string) (err error) {
	if l == nil {
		return
	}

	if len(packages) == 0 || (len(packages) == 1 && packages[0] == "") {
		if l.Packages != nil {
			return fmt.Errorf("trying to set files but package files already set")
		}
		l.Files = files
	} else if l.Files != nil {
		return fmt.Errorf("trying to set package files but files already set")
	}
	if l.Packages == nil {
		l.Packages = map[string][]string{}
	}
	for _, pkg := range packages {
		l.Packages[pkg] = files
	}
	return
}

func (l *Linked) UnmarshalYAML(unmarshal func(interface{}) error) error {
	if err := unmarshal(&l.Packages); err == nil {
		return nil
	}
	return unmarshal(&l.Files)
}

func (l *Linked) MarshalYAML() (inf interface{}, err error) {
	if l.Files != nil {
		inf = l.Files
	} else {
		inf = l.Packages
	}
	return
}

type StateConfig struct {
	Version         string  `yaml:"version"`
	Name            string  `yaml:"-"`
	LogTarget       string  `yaml:"log_target,omitempty"`
	Source          string  `yaml:"source"`
	InstallLocation string  `yaml:"install_location"`
	Remote          string  `yaml:"remote,omitempty"`
	Collection      string  `yaml:"collection,omitempty"`
	Track           *bool   `yaml:"track,omitempty"`
	Once            *bool   `yaml:"once,omitempty"`
	Linked          *Linked `yaml:"linked"`
}

type yamlStateConfig struct {
	Version         string  `yaml:"version"`
	LogTarget       string  `yaml:"log_target,omitempty"`
	Source          string  `yaml:"source"`
	InstallLocation string  `yaml:"install_location"`
	Remote          string  `yaml:"remote,omitempty"`
	Collection      string  `yaml:"collection,omitempty"`
	Track           *bool   `yaml:"track,omitempty"`
	Once            *bool   `yaml:"once,omitempty"`
	Linked          *Linked `yaml:"linked"`
}

func (config *StateConfig) UnmarshalYAML(unmarshal func(interface{}) error) (err error) {
	s := yamlStateConfig{}
	if err = unmarshal(&s); err != nil {
		return
	}

	config.Version = s.Version
	if !config.CheckVersion() {
		return fmt.Errorf("StateConfig not compatible with current version of dotfiles CLI, read release notes")
	}
	config.LogTarget = s.LogTarget
	config.Source = s.Source
	config.InstallLocation = s.InstallLocation
	config.Remote = s.Remote
	config.Collection = s.Collection
	config.Track = s.Track
	config.Once = s.Once
	config.Linked = s.Linked
	return
}

func (config *StateConfig) CheckVersion() bool {
	return config.Version == version
}

func (config *StateConfig) FileName() string {
	return config.Name + ".yaml"
}

func getUserDirectory() (string, error) {
	homeDir, err := homedir.Dir()
	if err != nil {
		return "", err
	}

	absPath := filepath.Join(homeDir, localInstallDir)

	return absPath, nil
}

func createUserDirectory() error {
	userDir, err := getUserDirectory()
	if err != nil {
		return err
	}
	if !sysfs.DirExists(userDir) {
		if !sysfs.DryRun {
			return os.MkdirAll(userDir, os.FileMode(0755))
		}
	}
	return nil
}

func (config *StateConfig) GetContextPath() string {
	dir := config.InstallLocation
	if !filepath.IsAbs(dir) {
		homeDir, _ := homedir.Dir()
		dir = filepath.Join(homeDir, dir)
	}
	return filepath.Join(dir, config.Name)
}

func validateStateConfigFileName(name string, writer io.Writer) bool {
	defer logging.SetLogOutput(writer)()
	if name == stateConfigFile {
		return false
	}

	if !strings.HasSuffix(name, ".yaml") {
		return false
	}

	log.WithField("file", name).Debug(text.StateConfigFound)
	return true
}

func removeStateConfigIfNotInstalled(config *StateConfig, writer io.Writer) (bool, error) {
	defer logging.SetLogOutput(writer)()
	_, err := os.Stat(config.GetContextPath())
	if os.IsNotExist(err) {
		log.WithField("file", config.Name).Debug(text.StateConfigNoInstall)
		userDir, userDirErr := getUserDirectory()
		if userDirErr != nil {
			return true, userDirErr
		}
		stateConfigPath := filepath.Join(userDir, config.FileName())
		if !sysfs.DryRun {
			if err = os.Remove(stateConfigPath); err != nil {
				return true, nil
			}
		}
		log.WithField("file", config.Name).Info(text.StateConfigDelete)
		return true, nil
	}

	return false, nil
}

func validStateConfig(config *StateConfig, writer io.Writer) bool {
	defer logging.SetLogOutput(writer)()

	if config.Source == "" {
		log.WithField("empty", "source").Error(text.StateConfigValidationFailed)
		return false
	}

	if config.InstallLocation == "" {
		log.WithField("empty", "install location").Error(text.StateConfigValidationFailed)
		return false
	}

	return true
}

func loadStateConfig(path string, name string, writer io.Writer) (*StateConfig, error) {
	defer logging.SetLogOutput(writer)()
	if name == stateConfigFile {
		return nil, nil
	}

	absPath := filepath.Join(path, name)
	log.WithField("file", absPath).Debug(text.StateConfigUnmarshal)

	yamlFile, err := os.ReadFile(absPath)
	if err != nil {
		return nil, err
	}
	config := &StateConfig{Name: templates.FileNameBase(name)}
	err = yaml.Unmarshal(yamlFile, config)
	if err != nil {
		return nil, err
	}

	if !validStateConfig(config, writer) {
		return nil, fmt.Errorf(text.StateConfigValidationFailed)
	}

	log.WithField("file", absPath).Debug(text.StateConfigUnmarshalled)
	return config, err
}

func handleStateConfigLink(path string, name string, configs map[string]*StateConfig) (*StateConfig, error) {
	//if name != stateConfigFile {
	//	return nil, nil
	//}

	absPath := filepath.Join(path, name)

	if _, err := os.Stat(absPath); os.IsNotExist(err) {
		return nil, nil
	}

	link, err := sysfs.ReadLink(absPath)

	base := templates.FileNameBase(link)

	if ptr, ok := configs[base]; ok {
		return ptr, nil
	}
	if !sysfs.DryRun {
		err = os.Remove(absPath)
	}
	return nil, err
}

type HasIsDir interface {
	Name() string
	IsDir() bool
	Type() os.FileMode
}

func isDir(parent string, file os.DirEntry) bool {
	if file.IsDir() {
		return true
	}
	if file.Type()&os.ModeSymlink != os.ModeSymlink {
		return false
	}

	link, err := sysfs.ReadLink(filepath.Join(parent, file.Name()))
	if err != nil {
		return true
	}
	fi, err := os.Stat(link)
	if err != nil {
		return true
	}
	return fi.IsDir()
}

func loadStateConfigs(writer io.Writer) (*StateConfig, *StateConfig, map[string]*StateConfig, error) {
	defer logging.SetLogOutput(writer)()

	userDir, err := getUserDirectory()
	if err != nil {
		return nil, nil, nil, err
	}
	log.WithField("inside", userDir).Debug(text.StateConfigDir)

	configs := make(map[string]*StateConfig)
	var config *StateConfig

	if !sysfs.DirExists(userDir) {
		return config, nil, configs, nil
	}

	files, err := os.ReadDir(userDir)
	if err != nil {
		if pe := err.(*fs.PathError); sysfs.DryRun && pe != nil && pe.Op == "open" {
			return nil, nil, configs, nil
		}
		return nil, nil, nil, err
	}

	for _, file := range files {
		if !isDir(userDir, file) {
			if !validateStateConfigFileName(file.Name(), writer) {
				continue
			}
			config, err = loadStateConfig(userDir, file.Name(), writer)
			if err != nil {
				return nil, nil, nil, err
			}
			removed, deletedErr := removeStateConfigIfNotInstalled(config, writer)
			if deletedErr != nil {
				return nil, nil, nil, deletedErr
			}
			if removed {
				continue
			}
			if err != nil {
				return nil, nil, nil, err
			}
			if config != nil {
				log.WithField("config", file.Name()).Debug(text.StateConfigValid)
				configs[config.Name] = config
			}
		}
	}
	ptr, err := handleStateConfigLink(userDir, stateConfigFile, configs)
	if ptr != nil && err == nil {
		log.WithField("file", ptr.Name).Debug(text.StateConfigCurrent)
	}
	prevPtr, err := handleStateConfigLink(userDir, previousStateConfigFile, configs)
	if prevPtr != nil && err == nil {
		log.WithField("file", prevPtr.Name).Debug(text.StateConfigPrevious)
	}
	return ptr, prevPtr, configs, err
}

func LoadStateConfigs() (*StateConfig, *StateConfig, map[string]*StateConfig, error) {
	return loadStateConfigs(nil)
}

func stateConfigToYaml(config *StateConfig) ([]byte, error) {
	return yaml.Marshal(config)
}

func (config *StateConfig) ToYaml() (string, error) {
	yamlData, err := stateConfigToYaml(config)
	return string(yamlData), err
}

func fromYAML(yamlBytes []byte) (*StateConfig, error) {
	config := &StateConfig{Name: ""}
	err := yaml.Unmarshal(yamlBytes, config)
	if err != nil {
		return nil, err
	}
	return config, nil
}

func (config *StateConfig) clone() (*StateConfig, error) {
	yamlData, err := stateConfigToYaml(config)
	if err != nil {
		return nil, err
	}
	return fromYAML(yamlData)
}

func saveStateConfig(config *StateConfig, writer io.Writer) error {
	userDir, err := getUserDirectory()
	if err != nil {
		return err
	}
	stateConfigLink := filepath.Join(userDir, stateConfigFile)
	stateConfigAbsPath := filepath.Join(userDir, config.FileName())
	yamlData, err := stateConfigToYaml(config)
	if err != nil {
		return err
	}
	if !sysfs.DryRun {
		if err = createUserDirectory(); err != nil {
			return err
		}
		log.WithField("target", stateConfigAbsPath).Debug("Updating state config")
		if err = os.WriteFile(stateConfigAbsPath, yamlData, os.FileMode(0644)); err != nil {
			return err
		}
		err = sysfs.Symlink(stateConfigAbsPath, stateConfigLink, writer)
	}
	return err
}

func (config *StateConfig) Save() (err error) {
	return saveStateConfig(config, nil)
}

func sourceToName(source string) string {
	sum := md5.Sum([]byte(source))
	return hex.EncodeToString(sum[:])
}

func UpdateStateConfig(writer io.Writer, cfgMgr *Manager, source string, installLocation string) (*StateConfig, bool, error) {
	current := cfgMgr.StateConfig
	list := cfgMgr.StateConfigs
	name := sourceToName(source)

	next := &StateConfig{
		Name:            name,
		Source:          source,
		InstallLocation: installLocation,
		Version:         version,
	}

	if cfg, ok := list[name]; ok {
		next = cfg
	} else {
		list[next.Name] = next
	}

	if current != nil && current != next {
		userDir, err := getUserDirectory()
		if err != nil {
			return next, false, err
		}
		stateConfigLink := filepath.Join(userDir, previousStateConfigFile)
		if err = sysfs.Symlink(current.GetContextPath()+".yaml", stateConfigLink, writer); err != nil {
			return next, false, err
		}
		return next, true, nil
	}

	return next, false, nil
}
