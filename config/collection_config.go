package config

import (
	"gitlab.com/pidrakin/go/interactive"
	"sort"
)

type SimpleCollection []string

type ExpertCollection struct {
	Mutex    [][]string `yaml:"mutex"`
	Default  []string   `yaml:"default"`
	Ignore   []string   `yaml:"ignore"`
	Optional []string   `yaml:"optional"`
}

type Collection struct {
	Simple SimpleCollection
	Expert ExpertCollection
}

func (c *Collection) UnmarshalYAML(unmarshal func(interface{}) error) (err error) {
	if err = unmarshal(&c.Simple); err == nil {
		return
	}
	return unmarshal(&c.Expert)
}

type Collections map[string]Collection

type CollectionsConfig struct {
	Collections Collections
}

func (c *CollectionsConfig) UnmarshalYAML(unmarshal func(interface{}) error) (err error) {
	return unmarshal(&c.Collections)
}

func InteractiveCollectionPrompt(prompter *interactive.Prompter, collections Collections) string {
	var options []string
	for key := range collections {
		options = append(options, key)
	}

	if len(options) == 0 {
		return ""
	}

	sort.Slice(options[:], func(i, j int) bool {
		return options[i] < options[j]
	})

	i, _, err := prompter.SingleChoice(
		options,
		"Which collection would you want to link?",
	)
	if err != nil || i == -1 {
		return ""
	}
	return options[i]
}
