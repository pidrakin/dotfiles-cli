package config

import (
	"bytes"
	"fmt"
	"gitlab.com/pidrakin/go/tests"
	"os"
	"path/filepath"
	"regexp"
	"testing"

	"github.com/mitchellh/go-homedir"
	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
)

var Source = "repository"
var Name = sourceToName("repository")
var HomeDir = "home/user"
var InstallLocation = localInstallDir

var mockStateConfig = StateConfig{
	Version:         version,
	Name:            Name,
	Source:          Source,
	InstallLocation: InstallLocation,
	Linked: &Linked{
		Files: []string{"file"},
	},
}

var mockWrongVersionStateConfig = StateConfig{
	Version:         "0.0",
	Name:            Name,
	Source:          Source,
	InstallLocation: InstallLocation,
	Linked: &Linked{
		Files: []string{"file"},
	},
}

func init() {
	homedir.DisableCache = true
}

func setupEnvironment(t *testing.T, f func(t *testing.T, wd string)) {
	tests.RunInTmp(t, func(t *testing.T, wd string) {
		homeDir := filepath.Join(wd, HomeDir)
		err := os.MkdirAll(homeDir, os.FileMode(0755))
		tests.AssertNoError(t, err)

		err = os.Setenv("HOME", homeDir)
		tests.AssertNoError(t, err)

		log.SetLevel(log.DebugLevel)

		f(t, wd)
	})
}

func TestStateConfig_FileName(t *testing.T) {
	setupEnvironment(t, func(t *testing.T, wd string) {
		filename := mockStateConfig.FileName()
		expected := mockStateConfig.Name + ".yaml"
		assert.Equal(t, expected, filename)
	})
}

func Test_getUserDirectory(t *testing.T) {
	setupEnvironment(t, func(t *testing.T, wd string) {
		userDir, err := getUserDirectory()
		tests.AssertNoError(t, err)
		expected := filepath.Join(wd, HomeDir, localInstallDir)
		assert.Equal(t, expected, userDir)
	})
}

func Test_CheckVersion(t *testing.T) {
	t.Run("success: state config version is uptodate", func(t *testing.T) {
		result := mockStateConfig.CheckVersion()
		assert.True(t, result)
	})
	t.Run("fail: state config version is not uptodate", func(t *testing.T) {
		result := mockWrongVersionStateConfig.CheckVersion()
		assert.False(t, result)
	})
}

func Test_unmarshal(t *testing.T) {
	t.Run("success: state config unmarshal", func(t *testing.T) {
		_, err := mockStateConfig.clone()
		tests.AssertNoError(t, err)
	})
	t.Run("fail: state config unmarshal", func(t *testing.T) {
		_, err := mockWrongVersionStateConfig.clone()
		assert.NotNil(t, err)
		assert.Equal(t, "StateConfig not compatible with current version of dotfiles CLI, read release notes", err.Error())
	})
}

func Test_createUserDirectory(t *testing.T) {
	setupEnvironment(t, func(t *testing.T, wd string) {
		err := createUserDirectory()
		tests.AssertNoError(t, err)
		expected := filepath.Join(wd, HomeDir, localInstallDir)
		tests.AssertDirExists(t, expected)
	})
}

func TestStateConfig_GetContextPath(t *testing.T) {
	setupEnvironment(t, func(t *testing.T, wd string) {
		contextPath := mockStateConfig.GetContextPath()
		expected := filepath.Join(wd, HomeDir, InstallLocation, Name)
		assert.Equal(t, expected, contextPath)
	})
}

func Test_validateStateConfigFileName(t *testing.T) {
	log.SetLevel(log.DebugLevel)
	t.Run("fail: name is dotfiles.yaml", func(t *testing.T) {
		var buf bytes.Buffer
		valid := validateStateConfigFileName(stateConfigFile, &buf)
		assert.False(t, valid)
		assert.Equal(t, "", buf.String())
	})
	t.Run("fail: name does not have yaml extension", func(t *testing.T) {
		var buf bytes.Buffer
		valid := validateStateConfigFileName(Name, &buf)
		assert.False(t, valid)
		assert.Equal(t, "", buf.String())
	})
	t.Run("success", func(t *testing.T) {
		var buf bytes.Buffer
		valid := validateStateConfigFileName(mockStateConfig.FileName(), &buf)
		assert.True(t, valid)
		assert.Regexp(t, regexp.MustCompile("msg=\"found potential state config\""), buf.String())
	})
}

func setupSaveStateConfig(t *testing.T, f func(t *testing.T, wd string)) {
	tests.RunInTmp(t, func(t *testing.T, wd string) {
		log.SetLevel(log.DebugLevel)

		homeDir := filepath.Join(wd, HomeDir)
		err := os.MkdirAll(homeDir, os.FileMode(0755))
		tests.AssertNoError(t, err)

		err = os.Setenv("HOME", homeDir)
		tests.AssertNoError(t, err)

		var buf bytes.Buffer
		err = saveStateConfig(&mockStateConfig, &buf)
		tests.AssertNoError(t, err)
		assert.Empty(t, buf.String())

		f(t, wd)
	})
}

func TestSaveStateConfig(t *testing.T) {
	setupSaveStateConfig(t, func(t *testing.T, wd string) {
	})
}

func Test_removeStateConfigIfNotInstalled(t *testing.T) {
	t.Run("fail: install location does not exist", func(t *testing.T) {
		setupSaveStateConfig(t, func(t *testing.T, wd string) {
			userDir, err := getUserDirectory()
			tests.AssertNoError(t, err)

			var buf bytes.Buffer
			removed, err := removeStateConfigIfNotInstalled(&mockStateConfig, &buf)
			tests.AssertNoError(t, err)
			assert.True(t, removed)
			assert.Regexp(t, regexp.MustCompile("msg=\"state config has no candidate installed; trying to delete it\""), buf.String())
			assert.Regexp(t, regexp.MustCompile("msg=\"successfully deleted state config without installed candidate\""), buf.String())
			tests.AssertFileAbsent(t, filepath.Join(userDir, mockStateConfig.FileName()))
		})
	})

	t.Run("success: install location does exist", func(t *testing.T) {
		setupSaveStateConfig(t, func(t *testing.T, wd string) {
			var buf bytes.Buffer

			err := os.MkdirAll(filepath.Join(wd, HomeDir, InstallLocation, Name), os.FileMode(0755))
			tests.AssertNoError(t, err)

			removed, err := removeStateConfigIfNotInstalled(&mockStateConfig, &buf)
			tests.AssertNoError(t, err)
			assert.False(t, removed)

			assert.NotRegexp(t, regexp.MustCompile("msg=\"state config has no candidate installed; trying to delete it\""), buf.String())
			assert.NotRegexp(t, regexp.MustCompile("msg=\"successfully deleted state config without installed candidate\""), buf.String())
			assert.Equal(t, "", buf.String())
		})
	})
}

func Test_validStateConfig(t *testing.T) {
	t.Run("fail: source not set", func(t *testing.T) {
		stateConfig := &StateConfig{}

		var buf bytes.Buffer
		valid := validStateConfig(stateConfig, &buf)
		assert.False(t, valid)
		assert.Regexp(t, regexp.MustCompile("msg=\"state config validation failed\" empty=source"), buf.String())
	})
	t.Run("fail: install location not set", func(t *testing.T) {
		stateConfig := &StateConfig{
			Source: "foobar",
		}

		var buf bytes.Buffer
		valid := validStateConfig(stateConfig, &buf)
		assert.False(t, valid)
		assert.Regexp(t, regexp.MustCompile("msg=\"state config validation failed\" empty=\"install location\""), buf.String())
	})
	t.Run("success", func(t *testing.T) {
		var buf bytes.Buffer
		valid := validStateConfig(&mockStateConfig, &buf)
		assert.True(t, valid)
		assert.Equal(t, "", buf.String())
	})
}

func Test_loadStateConfig(t *testing.T) {
	t.Run("success", func(t *testing.T) {
		setupSaveStateConfig(t, func(t *testing.T, wd string) {
			userDir, err := getUserDirectory()
			tests.AssertNoError(t, err)

			var buf bytes.Buffer
			stateConfig, err := loadStateConfig(userDir, mockStateConfig.FileName(), &buf)
			tests.AssertNoError(t, err)
			assert.NotNil(t, stateConfig)
			assert.Regexp(t, regexp.MustCompile("msg=\"trying to unmarshal state config\""), buf.String())
			assert.Regexp(t, regexp.MustCompile("msg=\"successfully unmarshalled state config\""), buf.String())
		})
	})
	t.Run("fail: validation failed", func(t *testing.T) {
		setupEnvironment(t, func(t *testing.T, wd string) {
			err := createUserDirectory()
			tests.AssertNoError(t, err)

			userDir, err := getUserDirectory()
			tests.AssertNoError(t, err)

			config := fmt.Sprintf(`version: %s
context: ebf87c9b333833349a703d78e66e4226
contextName: /Users/satanik/Development/pidrakin/dotfiles
contextAbsPath: /Users/satanik/.local/share/dotfiles/ebf87c9b333833349a703d78e66e4226
target: ""
linked: null`, version)
			configFileName := "ebf87c9b333833349a703d78e66e4226.yaml"
			configPath := filepath.Join(userDir, configFileName)
			err = os.WriteFile(configPath, []byte(config), os.FileMode(0644))
			tests.AssertNoError(t, err)

			var buf bytes.Buffer
			stateConfig, err := loadStateConfig(userDir, configFileName, &buf)
			assert.Error(t, err, "state config validation failed")
			assert.Nil(t, stateConfig)
			assert.Regexp(t, regexp.MustCompile("msg=\"trying to unmarshal state config\""), buf.String())
			assert.Regexp(t, regexp.MustCompile("msg=\"state config validation failed\" empty=source"), buf.String())
			assert.NotRegexp(t, regexp.MustCompile("msg=\"successfully unmarshalled state config\""), buf.String())
		})
	})
}

func Test_handleStateConfigLink(t *testing.T) {
	t.Run("success", func(t *testing.T) {
		setupSaveStateConfig(t, func(t *testing.T, wd string) {
			userDir, err := getUserDirectory()
			tests.AssertNoError(t, err)

			configs := make(map[string]*StateConfig)
			configs[Name] = &mockStateConfig
			stateConfig, err := handleStateConfigLink(userDir, stateConfigFile, configs)
			tests.AssertNoError(t, err)
			assert.NotNil(t, stateConfig)
		})
	})
	t.Run("fail: state config not available", func(t *testing.T) {
		setupSaveStateConfig(t, func(t *testing.T, wd string) {
			userDir, err := getUserDirectory()
			tests.AssertNoError(t, err)

			configs := make(map[string]*StateConfig)
			stateConfig, err := handleStateConfigLink(userDir, stateConfigFile, configs)
			tests.AssertNoError(t, err)
			assert.Nil(t, stateConfig)
			tests.AssertFileAbsent(t, filepath.Join(userDir, stateConfigFile))
		})
	})
}

func Test_loadStateConfigs(t *testing.T) {
	setupSaveStateConfig(t, func(t *testing.T, wd string) {
		var buf bytes.Buffer

		err := os.MkdirAll(filepath.Join(wd, HomeDir, InstallLocation, Name), os.FileMode(0755))
		tests.AssertNoError(t, err)

		stateConfig, previousStateConfig, configs, err := loadStateConfigs(&buf)
		tests.AssertNoError(t, err)
		assert.NotNil(t, stateConfig)
		assert.Nil(t, previousStateConfig)
		assert.Equal(t, 1, len(configs))
		assert.Equal(t, stateConfig, configs[Name])
	})
}

func Test_sourceToFilename(t *testing.T) {
	filename := sourceToName(Source)
	assert.Equal(t, Name, filename)
}

func Test_UpdateStateConfig(t *testing.T) {
	t.Run("set first state config", func(t *testing.T) {
		setupSaveStateConfig(t, func(t *testing.T, wd string) {
			configs := make(map[string]*StateConfig)
			cfgMgr := &Manager{
				StateConfigs: configs,
			}

			var buf bytes.Buffer
			stateConfig, switched, err := UpdateStateConfig(&buf, cfgMgr, Source, InstallLocation)
			tests.AssertNoError(t, err)
			assert.False(t, switched)
			assert.NotNil(t, stateConfig)
			assert.Equal(t, Name, stateConfig.Name)
			assert.Equal(t, 1, len(configs))
			assert.Contains(t, configs, Name)
		})
	})
	t.Run("set same state config", func(t *testing.T) {
		setupSaveStateConfig(t, func(t *testing.T, wd string) {
			configs := make(map[string]*StateConfig)
			configs[Name] = &mockStateConfig
			cfgMgr := &Manager{
				StateConfig:  &mockStateConfig,
				StateConfigs: configs,
			}

			var buf bytes.Buffer
			stateConfig, switched, err := UpdateStateConfig(&buf, cfgMgr, Source, InstallLocation)
			tests.AssertNoError(t, err)
			assert.False(t, switched)
			assert.NotNil(t, stateConfig)
			assert.Same(t, &mockStateConfig, stateConfig)
			assert.Equal(t, Name, stateConfig.Name)
			assert.Equal(t, 1, len(configs))
			assert.Contains(t, configs, Name)
		})
	})
	t.Run("set new state config", func(t *testing.T) {
		setupSaveStateConfig(t, func(t *testing.T, wd string) {
			configs := make(map[string]*StateConfig)
			configs[Name] = &mockStateConfig
			cfgMgr := &Manager{
				StateConfig:  &mockStateConfig,
				StateConfigs: configs,
			}

			source := "foobar"
			filename := sourceToName(source)
			other := &StateConfig{
				Version:         version,
				Name:            filename,
				Source:          source,
				InstallLocation: InstallLocation,
			}
			var buf bytes.Buffer
			err := saveStateConfig(other, &buf)
			tests.AssertNoError(t, err)

			buf.Reset()
			stateConfig, switched, err := UpdateStateConfig(&buf, cfgMgr, source, InstallLocation)
			tests.AssertNoError(t, err)
			assert.True(t, switched)
			assert.NotNil(t, stateConfig)
			assert.NotSame(t, &mockStateConfig, stateConfig)
			assert.Equal(t, source, stateConfig.Source)
			assert.Equal(t, filename, stateConfig.Name)
			assert.Equal(t, 2, len(configs))
			assert.Contains(t, configs, filename)
		})
	})
}
