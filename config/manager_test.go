package config

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/pidrakin/go/tests"
	"gopkg.in/yaml.v3"
	"os"
	"path/filepath"
	"testing"
)

func TestNewManager(t *testing.T) {
	tests.RunInTmp(t, func(t *testing.T, wd string) {
		err := os.Setenv("HOME", wd)
		tests.AssertNoError(t, err)

		cfgMgr, err := NewManager()
		tests.AssertNoError(t, err)
		assert.NotNil(t, cfgMgr)
		assert.Equalf(t, "", cfgMgr.configPath, "expected configPath to be empty")
		assert.NotNil(t, cfgMgr.Config)
		assert.Nil(t, cfgMgr.CollectionsConfig)
		assert.Nil(t, cfgMgr.StateConfig)

		cfgMgr, err = NewManager()
		tests.AssertNoError(t, err)
	})
}

func TestNewManagerWithStateConfig(t *testing.T) {
	setupSaveStateConfig(t, func(t *testing.T, wd string) {
		cfg := NewConfig()
		yamlData, err := yaml.Marshal(cfg)
		tests.AssertNoError(t, err)

		parent := filepath.Join(mockStateConfig.GetContextPath(), cfg.Dir())
		err = os.MkdirAll(parent, os.FileMode(0755))
		tests.AssertNoError(t, err)
		cfgPath := filepath.Join(parent, cfg.Name())

		err = os.WriteFile(cfgPath, yamlData, os.FileMode(0644))
		tests.AssertNoError(t, err)

		cfgMgr, err := NewManager()
		tests.AssertNoError(t, err)
		assert.NotNil(t, cfgMgr)
		assert.Equalf(t, "", cfgMgr.configPath, "expected configPath to be empty")
		assert.NotNil(t, cfgMgr.Config)
		assert.Nil(t, cfgMgr.CollectionsConfig)
		assert.NotNil(t, cfgMgr.StateConfig)

		cfgMgr, err = NewManager()
		tests.AssertNoError(t, err)
	})
}
