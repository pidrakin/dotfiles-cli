package config

import (
	"gopkg.in/yaml.v3"
	"os"
	"path/filepath"
)

type Manager struct {
	configPath          string
	Config              *Config
	CollectionsConfig   *CollectionsConfig
	StateConfig         *StateConfig
	PreviousStateConfig *StateConfig
	StateConfigs        map[string]*StateConfig
}

func (manager *Manager) ConfigPath() string {
	return manager.configPath
}

func NewManager() (*Manager, error) {
	manager := &Manager{}
	var err error
	if manager.StateConfig, manager.PreviousStateConfig, manager.StateConfigs, err = LoadStateConfigs(); err != nil {
		return manager, err
	}
	if manager.StateConfig == nil {
		manager.Config = NewConfig()
		return manager, nil
	}
	if manager.Config, err = ReadConfig(manager.StateConfig.GetContextPath()); err != nil {
		return manager, err
	}

	err = manager.getCollectionsConfig()
	return manager, err
}

func (manager *Manager) getCollectionsConfig() error {
	configPath := filepath.Join(manager.StateConfig.GetContextPath(), manager.Config.CollectionsConfig.File)
	if _, err := os.Stat(configPath); err == nil {
		manager.configPath = configPath
		return manager.parseCollectionsConfig()
	}

	return nil
}

func (manager *Manager) parseCollectionsConfig() (err error) {
	if _, err = os.Stat(manager.configPath); os.IsNotExist(err) {
		return nil
	}
	yamlFile, err := os.ReadFile(manager.configPath)
	if err != nil {
		return err
	}
	manager.CollectionsConfig = &CollectionsConfig{}
	err = yaml.Unmarshal(yamlFile, &manager.CollectionsConfig)
	return err
}
