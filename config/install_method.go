package config

import "strings"

//go:generate stringer -type=InstallMethod
type InstallMethod int

func (im InstallMethod) MarshalText() ([]byte, error) {
	return []byte(strings.ToLower(im.String())), nil
}

func (im *InstallMethod) UnmarshalText(text []byte) error {
	*im = InstallMethodFromText(string(text))
	return nil
}

func InstallMethodFromText(text string) InstallMethod {
	switch strings.ToLower(text) {
	default:
		return Global
	case "global":
		return Global
	case "local":
		return Local
	}
}

const (
	Global InstallMethod = iota
	Local
)
