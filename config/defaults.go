package config

var (
	localInstallDir         = ".local/share/dotfiles"
	globalInstallDir        = "/usr/share/dotfiles"
	ignoreFile              = ".dotfilesignore"
	hooksDir                = "hooks"
	collectionsConfigFile   = ".dotfilescollections"
	stateConfigFile         = "dotfiles.yaml"
	previousStateConfigFile = "prevous-dotfiles.yaml"
	configDir               = ".dotfiles"
	configName              = "config"
	gitDir                  = ".git"
)

func GetLocalInstallDir() string {
	return localInstallDir
}
