# TODO

- [ ] feat: [link] when switching context either unlink all or backup all from old context (default: backup, -f force unlink, -i ask away)
- [ ] examine: Print usage only for relevant errors
- [ ] feat: Show [compatibility](https://github.com/go-git/go-git/blob/master/COMPATIBILITY.md) from upstream package
- [ ] feat: [ssh-config] outsourcing of ssh_config parser & proxyjump feature to a go library
- [ ] feat: [rollout ssh] if IdentitiesOnly yes then make new keyring and pass identity to allow agent forwarding
- [ ] feat: remove concrete URLs e.g. https://gitlab.com/satanik/dotfiles.git
- [ ] feat: refactor tests to avoid duplicate code (e.g. temp dir preparation)
- [ ] fix: [rollout] fail properly when rollout with ssh url and no key available
- [ ] feat: [rollout] add rollout hooks
- [ ] feat: [rollout] add --purge to unlink and uninstall dotfiles on remote
- [ ] feat: [remove] to unlink+uninstall dotfiles (--purge: also removes .local/share/dotfiles)