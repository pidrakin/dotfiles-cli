$ curl -fsSL https://dotfiles.pidrak.in | sudo sh -
[DEBUG] OS is linux
[DEBUG] found apt: installing via package manager apt
[DEBUG] installed dotfiles

$ dotfiles install https://gitlab.com/pidrakin/dotfiles-example.git
running pre-link hooks...
hooks took 0 seconds

running post-link hooks...
hooks took 4 seconds

[ ✔ ] successfully linked 🎉

2 added file links
2 modified file links
2 hooks succeeded

$ dotfiles status --output wide
HOST   CLI    INSTALLED  LINKED  LINK_STATUS  REPO                                              GIT_STATUS
local  1.7.11  true       true    up-to-date   https://gitlab.com/pidrakin/dotfiles-example.git  up-to-date